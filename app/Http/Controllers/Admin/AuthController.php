<?php

namespace App\Http\Controllers\Admin;

use App\Project\Auth\Admin as AuthAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class AuthController extends Controller
{
	/**
	 * Page login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin(Request $request)
	{
		// session()->reflash();

		return view('admin.login');
	}

	/**
	 * Page login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$args = $request->all();

		// Validate login
		$validate = AuthAdmin::validateLogin($args);

		if ($validate->any())
			return redirect()->back()->withErrors($validate);

		// Login user
		if (Auth::attempt([
				'username' => $args['username'],
				'password' => $args['password'],
				'is_active' => 1
			]) === false)
		{
			return redirect()->back()->withErrors(messageBag('login_failed', 'These credentials do not match our records.'));
		}

		if (session('request_path'))
			return redirect()->intended(session('request_path'));
		else
			return redirect()->route('admin.home');
	}

	/**
	 * Page login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout(Request $request)
	{
		Auth::logout();

		return redirect()->route('admin.login');
	}
}
