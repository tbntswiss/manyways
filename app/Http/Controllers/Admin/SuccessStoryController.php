<?php

namespace App\Http\Controllers\Admin;

use App\Project\SuccessStory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuccessStoryController extends Controller
{
	/**
	 * Page success stories
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getSuccessStories(Request $request)
	{
		$success_stories = SuccessStory::all();

		return view('admin.success-stories', ['success_stories' => $success_stories]);
	}

	/**
	 * Page create success story
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getCreateSuccessStory(Request $request)
	{
		return view('admin.success-story.create');
	}

	/**
	 * Ajax create success story
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postCreateSuccessStory(Request $request)
	{
		$args = $request->all();

		// Validate form
		$validate = SuccessStory::validateInsert($args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Insert success_story
		$success_story_id = SuccessStory::insert($args);

		// Get success_story
		$success_story = SuccessStory::one($success_story_id);

		return response()->success(['success_story' => get_data($success_story)]);
	}

	/**
	 * Page update success story
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getUpdateSuccessStory(Request $request, $success_story_id)
	{
		$success_story = SuccessStory::one($success_story_id);

		return view('admin.success-story.update', ['success_story' => $success_story]);
	}

	/**
	 * Ajax update success story
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postUpdateSuccessStory(Request $request, $success_story_id)
	{
		$args = $request->all();

		// Validate form
		$validate = SuccessStory::validateUpdate($success_story_id, $args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get success_story
		$success_story = SuccessStory::one($success_story_id);

		// Update success_story
		$success_story->update($args);

		return response()->success(['success_story' => get_data($success_story)]);
	}

	/**
	 * Page delete success story
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getDeleteSuccessStory(Request $request, $success_story_id)
	{
		// Check if success_story exists
		if (SuccessStory::exists($success_story_id) === false)
			return response()->error(messageBag('not_exists', 'The success_story does not exists in our records.'));

		// Get success_story
		$success_story = SuccessStory::one($success_story_id);

		// Delete success_story
		$success_story->delete();

		return response()->success(['success_story' => get_data($success_story)]);
	}
}
