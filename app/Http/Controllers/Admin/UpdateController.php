<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;

use DB;
use Schema;

class UpdateController extends Controller
{
	/**
	 * Update
	 *
	 * @param \Illuminate\Http\Request
	 * @return void
	 */
	public function updateSite(Request $request)
	{
		// $this->addStepsTitlesColumn();
		$this->addSuccessStoriesIsProductColumn();
	}

	/**
	 * Success Stories
	 * Add column: is_product
	 *
	 */
	public function addSuccessStoriesIsProductColumn()
	{
		if (Schema::hasColumn('success_stories', 'is_product') === true)
			return;

		Schema::table('success_stories', function (Blueprint $table)
		{
			$table->unsignedTinyInteger('is_product')->after('client');
		});
	}

	/**
	 * Requests
	 * Add column: is_deleted
	 *
	 */
	public function addStepsTitlesColumn()
	{
		if (Schema::hasColumn('success_stories_langs', 'step_1_title') === true)
			return;

		Schema::table('success_stories_langs', function (Blueprint $table)
		{
			$table->string('step_1_title', 255)->after('step_1');
			$table->string('step_2_title', 255)->after('step_2');
			$table->string('step_3_title', 255)->after('step_3');
			$table->string('step_4_title', 255)->after('step_4');
			$table->string('step_5_title', 255)->after('step_5');
		});

		DB::table('success_stories_langs')
			->where('lang_id', 1)
			->update([
				'step_1_title' => 'Listening',
				'step_2_title' => 'Development of a personalized design',
				'step_3_title' => 'Choice of articles, objects,  textiles, etc',
				'step_4_title' => 'Choice of fabrication location',
				'step_5_title' => 'Delivery',
			]);

		DB::table('success_stories_langs')
			->where('lang_id', 2)
			->update([
				'step_1_title' => 'Ecoute',
				'step_2_title' => 'Développement d’un  design personnalisé',
				'step_3_title' => 'Choix des articles, objets,  textiles, etc',
				'step_4_title' => 'Choix du lieux de  production',
				'step_5_title' => 'Livraison',
			]);

		DB::table('success_stories_langs')
			->where('lang_id', 3)
			->update([
				'step_1_title' => 'Erster Schritt',
				'step_2_title' => 'Entwicklung eines persönlichen Designs',
				'step_3_title' => 'Auswahl der Gegenstände, Produkte, Textilien usw.',
				'step_4_title' => 'Wahl der Produktionsstätte',
				'step_5_title' => 'Lieferung',
			]);
	}
}
