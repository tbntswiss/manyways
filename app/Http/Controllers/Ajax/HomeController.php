<?php

namespace App\Http\Controllers\Ajax;

use App\Project\Lang;
use App\Project\Utils\Image as UtilsImage;
use App\Project\Utils\File as UtilsFile;

use App;
use File;
use Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	/**
	 * Set cookie warning as read
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postCookieAccepted(Request $request)
	{
		$args = $request->all();
		return response()->success()->cookie('cookies_accepted', (int) $args['is_hidden'] ?? 1, 60 * 24 * 7);
	}

	/**
	 * Get app config
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getConfig(Request $request)
	{
		// Get langs
		$langs = Lang::allByCodes();
		$lang = Lang::getCurrent();

		// Get translations
		$resource_path = resource_path().'/lang/'.App::getLocale();
		$trans = [];

		$lang_files = File::files($resource_path);
		$lang_dirs = File::directories($resource_path);

		foreach ($lang_files as &$file) {
			$filename = pathinfo($file)['filename'];
			$trans[$filename] = trans($filename);
		}

		foreach ($lang_dirs as &$dir_path) {
			$dirname = basename($dir_path);
			$trans[$dirname] = [];

			$lang_files = File::files($dir_path);

			foreach ($lang_files as &$file) {
				$filename = pathinfo($file)['filename'];
				$trans[$dirname][$filename] = trans($dirname.'/'.$filename);
			}
		}

		// Format config
		$config = [
			'lang' => $lang,
			'langs' => $langs,
			'trans' => $trans,
			'csrfToken' => csrf_token(),
			'app' => [
				'name' => appName(),
				'url' => appUrl(),
				'base' => baseUrl(),
			],
			'dir' => [
				'images' => UtilsImage::getDir(),
				'thumbs' => UtilsImage::getDir().'thumbs/',
				'files' => UtilsFile::getDir(),
			],
			// 'debug' => [
			// 	'root' => $request->root(),
			// 	'url' => $request->url(),
			// 	'fullUrl' => $request->fullUrl(),
			// 	'path' => $request->path(),
			// 	'segments' => $request->segments(),
			// 	'header' => $request->header(),
			// 	'server' => $request->server(),
			// ],
		];

		return response()->success($config);
	}

	/**
	 * Get csrf token
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getCSRFToken(Request $request)
	{
		return response()->success(['token' => csrf_token()]);
	}
}
