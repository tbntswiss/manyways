<?php

namespace App\Http\Controllers\App;

use App\Project\Lang;
use App\Project\Contact;
use App\Project\SuccessStory;

use App\Mail\Contact as MailContact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App;
use Mail;
use Config;

class HomeController extends Controller
{
	/**
	 * Get home page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getHome(Request $request)
	{
		$lang_id = Lang::getCurrentId();
		$success_stories = SuccessStory::all($lang_id);

		return view('public.home', ['success_stories' => $success_stories]);
	}

	/**
	 * Get success story page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getSuccessStory(Request $request, $success_story_page, $success_story_slug)
	{
		$success_story_slug = explode('/', $success_story_slug);
		$success_story_slug = array_pop($success_story_slug);

		$lang_id = Lang::getCurrentId();
		$success_story_id = SuccessStory::getIdFromSlug($success_story_slug, $lang_id);

		if (SuccessStory::exists($success_story_id) === false)
			return view('errors.404');

		$success_story = SuccessStory::one($success_story_id, $lang_id);
		$next_id = SuccessStory::firstNextId($success_story, $lang_id);

		$next_success_story = SuccessStory::one($next_id, $lang_id);

		return view('public.success-story', ['success_story' => $success_story, 'next_success_story' => $next_success_story]);
	}

	/**
	 * Get product page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getProducts(Request $request)
	{
		return view('public.products');
	}

	/**
	 * Get contact page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getContact(Request $request)
	{
		return view('public.contact');
	}

	/**
	 * Check captcha
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postCaptcha(Request $request)
	{
		$args = $request->all();

		// Validate captcha
		$validate = Contact::validateCaptcha($args);

		if ($validate->any())
			return response()->error(['errors' => $validate->all()]);

		// Make request
		$args['response'] = $args['captcha_response'];
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = ['query' => array_merge($args, ['secret' => env('CAPTCHA_SECRET', ''), 'remoteip' => $_SERVER['REMOTE_ADDR']])];

		$client = new \GuzzleHttp\Client(['defaults' => ['exceptions' => false]]);
		$response = $client->get($url, $data);

		$response = json_decode($response->getBody());

		// Flash session
		if ($response->success === true)
			session()->flash('captcha_verify', ['captcha' => $args['response'], 'response' => $response]);

		return response()->success(['response' => $response]);
	}

	/**
	 * Post contact form
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postContact(Request $request)
	{
        App::setLocale('fr');

		$args = $request->all();

		// Validate form
		$validate = Contact::validateForm($args);

		if ($validate->any())
			return response()->error($validate->messages());

		// // Validate captcha
		// $captcha_session = session('captcha_verify');

		// if ($captcha_session === null || $captcha_session['response']->challenge_ts !== $args['captcha_verify'])
		// 	return response()->error(['post' => $args, 'errors' => ['The captcha is not valid.']]);

		// Send email
		Mail::to(Config::get('project.email'))->send(new MailContact(['data' => $args]));
		// Mail::to($args['email'])->send(new MailContact(['data' => $args]));

		return response()->success();
	}

	/**
	 * Get Madrid story page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getConfirmation(Request $request)
	{
		return view('public.confirmation');
	}
}
