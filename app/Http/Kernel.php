<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\ViewMiddleware::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'base' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,

            \App\Http\Middleware\VerifyCsrfToken::class,
            // \App\Http\Middleware\DomainMiddleware::class,
        ],

        'admin' => [
            \App\Http\Middleware\GuardAdminMiddleware::class,
            \App\Http\Middleware\LangAdminMiddleware::class,
            \App\Http\Middleware\AuthAdminMiddleware::class,
            \App\Http\Middleware\ViewAdminMiddleware::class,
        ],

        'app' => [
            \App\Http\Middleware\LangAjaxMiddleware::class,
            \App\Http\Middleware\LangWebMiddleware::class,
            // \App\Http\Middleware\AuthWebMiddleware::class,
            // \App\Http\Middleware\ViewWebMiddleware::class,
        ],

        'ajax' => [
            \App\Http\Middleware\LangAjaxMiddleware::class,
            // \App\Http\Middleware\AuthAjaxMiddleware::class,
            // \App\Http\Middleware\ViewWebMiddleware::class,
        ],

        'cron' => [
            // \App\Http\Middleware\AuthCronMiddleware::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
}
