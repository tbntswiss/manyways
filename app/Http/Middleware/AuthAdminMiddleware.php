<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AuthAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request_path = $request->path();
        $request_paths = [
            'admin/login',
        ];

        // Redirect login page if unauthenticated
        if (Auth::check() === false && in_array($request_path, $request_paths) === false) {
            return redirect()->route('admin.login')->with('request_path', $request_path);
        }
        else if (Auth::check() === true && in_array($request_path, $request_paths) === true) {
            return redirect()->route('admin.home');
        }

        // Get user
        $request->user = Auth::user();

        return $next($request);
    }
}
