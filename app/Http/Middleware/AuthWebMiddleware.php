<?php

namespace App\Http\Middleware;

use App\Project\User;

use Auth;
use Closure;

class AuthWebMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request_path = $request->path();
        $redirect_paths = [
            langUrl('login'),
            langUrl('forgotten_password'),
            langUrl('recovery_password'),
        ];

        // Redirect admin
        if ($request->domain->is_admin === true && $request->domain->is_festival === false)
            return redirect()->route('admin.login');

        // Redirect login page if unauthenticated
        if (Auth::check() === false && in_array($request_path, $redirect_paths) === false) {
            return redirect()->to(langUrl('login'))->with('request_path', $request_path);
        }
        else if (Auth::check() === true && in_array($request_path, $redirect_paths) === true) {
            return redirect()->to(langUrl('home'));
        }

        // Get user
        $request->user = Auth::user();

        if ($request->user !== null)
            $request->user = User::one(Auth::user()->id);

        return $next($request);
    }
}
