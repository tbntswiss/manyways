<?php

namespace App\Http\Middleware;

use App\Project\Festival;

use Config;
use Closure;

class DomainMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get domain name
        $server_name = $request->server->get('SERVER_NAME');

        // Get festival
        $festivals_domains = Festival::listDomains();
        $festival_id = array_search($server_name, $festivals_domains);
        $festival = null;

        if (Festival::exists($festival_id) === true)
            $festival = Festival::one($festival_id);

        // Check if admin or festival
        $is_admin = $server_name === Config::get('project.domain');
        $is_festival = $festival_id !== false;

        if ($is_admin === false && $is_festival === false)
            return redirect()->to('404');

        $request->festival = $festival;
        $request->domain = array_to_object([
            'server_name' => $server_name,
            'is_admin' => $is_admin,
            'is_festival' => $is_festival,
        ]);

        return $next($request);
    }
}
