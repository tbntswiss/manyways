<?php

namespace App\Http\Middleware;

use App\Project\Lang;

use App;
use Closure;

class LangAjaxMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang_code = $request->all()['lang_code'] ?? '';

        if (Lang::codeExists($lang_code) === true)
            App::setLocale($lang_code);

        return $next($request);
    }
}
