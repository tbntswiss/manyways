<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Config;
use Session;

class LangWebMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * First init done in
         * App\Providers\RouteServiceProvider::switchLang()
         */

        // Get default local
        $local = Config::get('app.locale_default');
        $local_request_empty = Config::get('app.locale_prefix_empty');
        $locals = array_merge(Config::get('app.locale_alts'), [$local]);

        // Get browser language
        $lang_nav = null;

        if ($request->segment(1) === null && Session::get('language') === null && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang_nav = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);

            if (strpos($lang_nav, '_') !== false) {
                $lang_nav = explode('_', $lang_nav, 2);
                $lang_nav = $lang_nav[0];
            }

            if (in_array($lang_nav, $locals) === true)
                $local = $lang_nav;

            Session::set('language', $local);
            Session::save();

            return redirect('/'.($local === $local_request_empty ? '' : $local));
        }

        Session::set('language', App::getLocale());

        return $next($request);
    }
}
