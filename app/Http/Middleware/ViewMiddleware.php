<?php

namespace App\Http\Middleware;

use App\Project\Lang;

use View;
use Closure;

class ViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        View::share('app_lang', Lang::getCurrent());
        View::share('app_langs', Lang::all());

        View::share('app_name', appName());
        View::share('app_url', appUrl());
        View::share('app_base', baseUrl());

        View::share('dir_images', '\App\Project\Utils\Image::getDir');
        View::share('dir_thumbs', '\App\Project\Utils\Image::getThumbsDir');

        return $next($request);
    }
}
