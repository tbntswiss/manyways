<?php

namespace App\Listeners;

use App\Project\Image\SuccessStory;

class FileImageEventSubscriber
{
	/**
	 * Handle image file delete events
	 *
	 * @param object $image Image
	 * @return void
	 */
	public function onDelete($image)
	{
		SuccessStory::delete($image->id);
	}

	/**
	 * Register the listeners for the subscriber
	 *
	 * @param Illuminate\Events\Dispatcher $events
	 * @return void
	 */
	public function subscribe($events)
	{
		$events->listen('file.image.delete', __CLASS__.'@onDelete');
	}
}