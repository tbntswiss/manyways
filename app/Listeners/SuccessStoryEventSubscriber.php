<?php

namespace App\Listeners;

use App\Project\SuccessStory\Lang as SuccessStoryLang;
use App\Project\SuccessStory\Image as SuccessStoryImage;

class SuccessStoryEventSubscriber
{
	/**
	 * Handle success story delete events
	 *
	 * @param object $success_story Success story
	 * @return void
	 */
	public function onDelete($success_story)
	{
		SuccessStoryLang::delete($success_story->id);
		SuccessStoryImage::delete($success_story->id);
	}

	/**
	 * Register the listeners for the subscriber
	 *
	 * @param Illuminate\Events\Dispatcher $events
	 * @return void
	 */
	public function subscribe($events)
	{
		$events->listen('success_story.delete', __CLASS__.'@onDelete');
	}
}
