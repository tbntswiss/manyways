<?php

/**
 * This class implements ArrayAccess copied from
 * http://www.php.net/manual/en/class.arrayaccess.php
 */

namespace App;

class Project
{
	/**
	 * Data
	 *
	 * @var object
	 */
	private $data = [];

	/**
	 * Constructor
	 *
	 * @param mixed $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		$this->setData($data, true);
	}

	/**
	 * Get data attribute
	 *
	 * @param string $key Attribute
	 * @return mixed
	 */
	public function &__get($key)
	{
		return $this->data[$key];
	}

	/**
	 * Set data attribute
	 *
	 * @param string $key Attribute
	 * @param mixed $value Attribute value
	 * @return void
	 */
	public function __set($key, $value)
	{
		$this->data[$key] = $value;
	}

	/**
	 * Isset data attribute
	 *
	 * @param string $key Attribute
	 * @return boolean
	 */
	public function __isset($key)
	{
		return isset($this->data[$key]);
	}

	/**
	 * Unset data attribute
	 *
	 * @param string $key Attribute
	 * @return void
	 */
	public function __unset($key)
	{
		unset($this->data[$key]);
	}

	/**
	 * Serialize data
	 *
	 * @return void
	 */
	public function __sleep()
	{
		return ['data'];
	}

	/**
	 * Serialize data
	 *
	 * @return void
	 */
	public function __clone()
	{
		$this->data = clone $this->data;
	}

	/**
	 * Serialize data
	 *
	 * @return void
	 */
	public function __toString()
	{
		return json_encode($this->toArray());
	}

	/**
	 * Assigns a value to the specified offset
	 *
	 * @param string The offset to assign the value to
	 * @param mixed  The value to set
	 * @abstracting ArrayAccess
	 */
	public function offsetSet($offset,$value)
	{
		if (is_null($offset))
			$this->data[] = $value;
		else
			$this->data[$offset] = $value;
	}

	/**
	 * Whether or not an offset exists
	 *
	 * @param string An offset to check for
	 * @return boolean
	 * @abstracting ArrayAccess
	 */
	public function offsetExists($offset)
	{
		return isset($this->data[$offset]);
	}

	/**
	 * Unsets an offset
	 *
	 * @param string The offset to unset
	 * @abstracting ArrayAccess
	 */
	public function offsetUnset($offset)
	{
		if ($this->offsetExists($offset)) unset($this->data[$offset]);
	}

	/**
	 * Returns the value at specified offset
	 *
	 * @param string The offset to retrieve
	 * @return mixed
	 * @abstracting ArrayAccess
	 */
	public function offsetGet($offset)
	{
		return $this->offsetExists($offset) ? $this->data[$offset] : null;
	}

	/**
	 * Get data array
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->data;
	}

	/**
	 * Get data object
	 *
	 * @return object
	 */
	public function getData()
	{
		return (object) $this->data;
	}

	/**
	 * Set data
	 *
	 * @param boolean $replace Replace current data
	 * @return void
	 */
	public function setData($data, $replace = false)
	{
		$data = is_collection($data) ? $data->toArray() : is_object($data) ? (array) $data : $data;
		$this->data = $replace === true ? $data : array_merge($this->data, $data);
	}

	/**
	 * Get data attribute
	 *
	 * @param string $key Attribute
	 * @return mixed
	 */
	public function get($key)
	{
		return $this->__get($key);
	}

	/**
	 * Set data attribute
	 *
	 * @param string $key Attribute
	 * @param mixed $value Attribute value
	 * @return void
	 */
	public function set($key, $value)
	{
		$this->__set($key, $value);
	}

	/**
	 * Isset data attribute
	 *
	 * @param string $key Attribute
	 * @return boolean
	 */
	public function has($key)
	{
		return $this->__isset($key);
	}

	/**
	 * Unset data attribute
	 *
	 * @param string $key Attribute
	 * @return void
	 */
	public function forget($key)
	{
		$this->__unset($key);
	}

	/**
	 * Initialize object
	 *
	 * @param string $class_name Child class name
	 * @param mixed $data Object data
	 * @param integer $lang_id Lang id
	 * @return self
	 */
	public static function initialize($class_name, $data, $lang_id = 0)
	{
		if (is_collection($data) === true) {
			foreach ($data as $key => $item)
				$data[$key] = new $class_name($item, $lang_id);
		}
		else {
			$data = new $class_name($data, $lang_id);
		}

		return $data;
	}
}
