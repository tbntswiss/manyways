<?php

namespace App\Project;

use DB;
use Validator;
use Hash;

class Admin
{
	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make($data, [
			'username' => 'required|string|unique:users_admin,username',
			'password' => 'required|string',
		])->errors();

		return $errors;
	}



	/****************************************************************
	 * Methods
	 ***************************************************************/




	/****************************************************************
	 * Database
	 ***************************************************************/

	/**
	 * Check if admin exists
	 *
	 * @param integer $user_id User id
	 * @return boolean
	 */
	public static function exists($user_id)
	{
		return DB::table('users_admin')
			->where('id', $user_id)
			->exists();
	}

	/**
	 * Insert admin
	 *
	 * @param array $user User data
	 * @return integer
	 */
	public static function insert($user)
	{
		$now = date('Y-m-d H:i:s');

		$user_id = DB::table('users_admin')
			->insertGetId([
				'username' => $user['username'],
				'password' => Hash::make($user['password']),
				'type' => 1,
				'is_active' => 1,
				'created_at' => $now,
			]);

		return $user_id;
	}

	/**
	 * Update admin password
	 *
	 * @param integer $user_id User id
	 * @param string $new_password New password
	 * @return void
	 */
	public static function updatePassword($user_id, $new_password)
	{
		$now = date('Y-m-d H:i:s');

		DB::table('users_admin')
			->where('id', $user_id)
			->update([
				'password' => Hash::make($new_password),
				'updated_at' => $now,
			]);
	}
}
