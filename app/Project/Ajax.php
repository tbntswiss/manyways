<?php

namespace App\Project;

use Validator;

class Ajax
{
	/****************************************************************
	 * Validators
	 ***************************************************************/

	/**
	 * Validate lang
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateLang($data)
	{
		return Validator::make($data, [
			'lang_id' => 'required|numeric|exists:langs,id',
		])->errors();
	}
}
