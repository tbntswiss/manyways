<?php

namespace App\Project\Auth;

use Validator;

class Admin
{
	/**
	 * Validate admin login
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateLogin($data)
	{
		$errors = Validator::make($data, [
			'username' => 'required|string',
			'password' => 'required|string',
		])->errors();

		return $errors;
	}
}
