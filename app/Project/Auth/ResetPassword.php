<?php

namespace App\Project\Auth;

use Illuminate\Contracts\Encryption\DecryptException;

use DB;
use Config;
use Validator;

class ResetPassword
{
	/****************************************************************
	 * Encryption
	 ***************************************************************/

	/**
	 * Encrypt reset token
	 *
	 * @param string $reset_token Reset token
	 * @return string
	 */
	public static function encrypt($reset_token)
	{
		return encrypt($reset_token);
	}

	/**
	 * Decrypt reset token
	 *
	 * @param string $reset_token Reset token
	 * @return string
	 */
	public static function decrypt($reset_token)
	{
		try {
			return decrypt($reset_token);
		}
		catch (DecryptException $e) {
			return '';
		}
	}




	/****************************************************************
	 * Database
	 ***************************************************************/

	/**
	 * Check reset password entry
	 *
	 * @param integer $festival_id Festival id
	 * @param string $reset_token Reset token
	 * @return integer
	 */
	public static function userId($festival_id, $reset_token)
	{
		return DB::table('reset_passwords')
			->where('festival_id', $festival_id)
			->where('token', $reset_token)
			->first()->user_id ?? 0;
	}

	/**
	 * Check reset password entry
	 *
	 * @param integer $festival_id Festival id
	 * @param string $reset_token Reset token
	 * @return boolean
	 */
	public static function check($festival_id, $reset_token)
	{
		$reset_password_duration = Config::get('project.reset_password_duration');
		$now = date('Y-m-d H:i:s', strtotime('-'.$reset_password_duration.' hours'));

		// Check token
		return DB::table('reset_passwords')
			->where('festival_id', $festival_id)
			->where('token', $reset_token)
			->where('created_at', '>', $now)
			->exists();
	}

	/**
	 * Add reset password entry
	 *
	 * @param integer $festival_id Festival id
	 * @param integer $user_id User id
	 * @param string $reset_token Reset token
	 * @return integer
	 */
	public static function add($festival_id, $user_id, $reset_token)
	{
		$now = date('Y-m-d H:i:s');

		// Empty old entry
		self::delete($reset_token);

		// Add new entry
		$reset_password_id = DB::table('reset_passwords')
			->insertGetId([
				'festival_id' => $festival_id,
				'user_id' => $user_id,
				'token' => $reset_token,
				'created_at' => $now,
			]);

		return $reset_password_id;
	}

	/**
	 * Delete reset password entry
	 *
	 * @param string $reset_token Reset token
	 * @return void
	 */
	public static function delete($reset_token)
	{
		DB::table('reset_passwords')
			->where('token', $reset_token)
			->delete();
	}
}
