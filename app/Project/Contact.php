<?php

namespace App\Project;

use Validator;

class Contact
{
	/**
	 * Validate captcha
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateCaptcha(array $data)
	{
		return Validator::make($data, [
			'captcha_response' => 'required|string',
		])->errors();
	}

	/**
	 * Validate form
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateForm($data)
	{
		$errors = Validator::make($data, [
			'surname' => 'required|string',
			'firstname' => 'required|string',
			'email' => 'required|email',
			'phone' => 'string',
			'message' => 'required|string',
		])->errors();

		return $errors;
	}
}
