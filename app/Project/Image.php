<?php

namespace App\Project;

use App\Project\Utils\Image as UtilsImage;

use Illuminate\Support\MessageBag;

use DB;
use Validator;

class Image
{
	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if image exists
	 *
	 * @param string $image_id Image id
	 * @return boolean
	 */
	public static function exists($image_id)
	{
		return DB::table('files_images')
			->where('id', $image_id)
			->exists();
	}

	/**
	 * Get image
	 *
	 * @param integer $image_id Image id
	 * @return object
	 */
	public static function one($image_id)
	{
		return DB::table('files_images')
			->where('id', $image_id)
			->first();
	}

	/**
	 * Get all images
	 *
	 * @return array
	 */
	public static function all()
	{
		return DB::table('files_images')
			->get();
	}

	/**
	 * Upload image file
	 *
	 * @param integer $image_id Image id to replace
	 * @param array $image Image to upload
	 * @param array $options Upload options
	 * @return integer
	 */
	public static function upload($image_id, $image, $options = [])
	{
		// Remove image if exists
		$image_exists = self::exists($image_id);

		if ($image_exists === true)
			UtilsImage::delete(self::one($image_id)->filename);

		// Upload image
		$image_name = UtilsImage::upload($image, $options);

		// Update database
		if ($image_exists === true)
			self::update($image_id, $image_name);
		else
			$image_id = self::insert($image_name);

		return $image_id;
	}

	/**
	 * Insert image file
	 *
	 * @param string $image_name Image name
	 * @return integer
	 */
	public static function insert($image_name)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'filename' => $image_name,
			'created_at' => $now,
		];

		// Insert database
		$image_id = DB::table('files_images')
			->insertGetId($insert_values);

		return $image_id;
	}

	/**
	 * Update image file
	 *
	 * @param integer $image_id Image id
	 * @param string $image_name Image name
	 * @return void
	 */
	public static function update($image_id, $image_name)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'filename' => $image_name,
			'updated_at' => $now,
		];

		// Update database
		DB::table('files_images')
			->where('id', $image_id)
			->update($update_values);
	}

	/**
	 * Delete images files
	 *
	 * @param integer $images_ids Images ids
	 * @return void
	 */
	public static function delete($images_ids)
	{
		if (is_array($images_ids) === false)
			$images_ids = [$images_ids];

		// Delete images
		foreach ($images_ids as &$image_id) {
			if (self::exists($image_id) === false)
				continue;

			// Get image
			$image = self::one($image_id);

			// Delete file
			UtilsImage::delete($image->filename);

			// Delete database
			DB::table('files_images')->where('id', $image_id)->delete();

			// Fire event
			event('file.image.delete', $image);
		}
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate uploads
	 *
	 * @param array $image Image to validate
	 * @param boolean $is_required File is required
	 * @param string $image_name File error name
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateUpload($image, $is_required = false, $image_name = 'image')
	{
		$image = gettype($image) !== 'array' ? [$image_name => $image] : $image;
		$max_size = UtilsImage::getMaxWidth();

		$errors = Validator::make($image, [
			$image_name => ($is_required ? 'required|' : '').'file|dimensions:max_width='.$max_size.',max_height='.$max_size,
		])->errors();

		return $errors;
	}

	/**
	 * Check if uploaded
	 *
	 * @param array $image Image to validate
	 * @return boolean
	 */
	public static function isUploaded($image)
	{
		return self::validateUpload($image, true)->any() === false;
	}
}
