<?php

namespace App\Project\Image;

use App\Project\SuccessStory as AppSuccessStory;

use DB;
use Validator;

class SuccessStory
{
	/**
	 * Delete success story images
	 *
	 * @param integer $image_id Image id
	 * @return void
	 */
	public static function delete($image_id)
	{
		DB::table('success_stories')
			->where('background_id', $image_id)
			->update(['background_id' => 0]);

		DB::table('success_stories')
			->where('image_1_id', $image_id)
			->update(['image_1_id' => 0]);

		DB::table('success_stories')
			->where('image_2_id', $image_id)
			->update(['image_2_id' => 0]);

		DB::table('success_stories_images')
			->where('image_id', $image_id)
			->delete();
	}
}
