<?php

namespace App\Project;

use App;
use Config;

class Lang
{
	/**
	 * Get lang id from code
	 *
	 * @param string $code Lang code
	 * @return integer
	 */
	public static function codeExists($code)
	{
		return isset(self::allByCodes()->{$code});
	}

	/**
	 * Get lang id from code
	 *
	 * @param string $code Lang code
	 * @return integer
	 */
	public static function getIdFromCode($code)
	{
		return self::allByCodes()->{$code}->id ?? 0;
	}

	/**
	 * Get current lang id
	 *
	 * @return integer
	 */
	public static function getCurrentId()
	{
		return self::getCurrent()->id ?? 0;
	}

	/**
	 * Get current lang
	 *
	 * @return object
	 */
	public static function getCurrent()
	{
		return self::allByCodes()->{App::getLocale()} ?? null;
	}

	/**
	 * Get lang
	 *
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	static public function one($lang_id)
	{
		return self::allByIds()->{$lang_id} ?? null;
	}

	/**
	 * Get all langs
	 *
	 * @return array
	 */
	static public function all()
	{
		return array_to_object(self::config());
	}

	/**
	 * Get all langs ids
	 *
	 * @return object
	 */
	public static function allIds()
	{
		return array_keys(collect(self::config())->keyBy('id')->toArray());
	}

	/**
	 * Get all langs by ids
	 *
	 * @return object
	 */
	public static function allByIds()
	{
		return array_to_object(collect(self::config())->keyBy('id')->toArray());
	}

	/**
	 * Get all langs by codes
	 *
	 * @return object
	 */
	public static function allByCodes()
	{
		return array_to_object(collect(self::config())->keyBy('code')->toArray());
	}

	/**
	 * Get all langs from config
	 *
	 * @return array
	 */
	static private function config()
	{
		$langs = Config::get('langs');
		$langs = array_map(
			function($lang, $code, $i) { $lang['id'] = $i + 1; $lang['code'] = $code; return $lang; },
			$langs, array_keys($langs), array_keys(array_values($langs))
		);

		return $langs;
	}
}
