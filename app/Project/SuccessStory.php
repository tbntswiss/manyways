<?php

namespace App\Project;

use App\Project;
use App\Project\Image;
use App\Project\SuccessStory\Lang as SuccessStoryLang;
use App\Project\SuccessStory\Image as SuccessStoryImage;

use DB;
use Validator;

class SuccessStory extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @param integer $lang_id Lang id
	 * @return void
	 */
	public function __construct($data, $lang_id = 0)
	{
		parent::__construct($data);

		$this->lang_id = $lang_id;
		$this->format();
	}

	/**
	 * Format success story
	 *
	 * @return void
	 */
	public function format()
	{
		$this->visible_at_utc = to_utc_date($this->visible_at);
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->background = Image::one($this->background_id);
		$this->image_1 = Image::one($this->image_1_id);
		$this->image_2 = Image::one($this->image_2_id);

		$this->langs = SuccessStoryLang::get($this->id, $this->lang_id);
		$this->gallery = SuccessStoryImage::get($this->id);
	}

	/**
	 * Update success story
	 *
	 * @param array $success_story Success story data
	 * @return void
	 */
	public function update($success_story)
	{
		$this->_update($this->id, $success_story, $this->getData());
		$this->setData($this->one($this->id)->getData());
	}

	/**
	 * Delete success story
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this->id);

		event('success_story.delete', [$this]);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if success story exists
	 *
	 * @param integer $success_story_id Success story id
	 * @return boolean
	 */
	public static function exists($success_story_id)
	{
		return DB::table('success_stories')
			->where('id', $success_story_id)
			->exists();
	}

	/**
	 * Get success story id from slug
	 *
	 * @param string $slug Success story slug
	 * @param integer $lang_id Lang id
	 * @return integer
	 */
	public static function getIdFromSlug($slug, $lang_id)
	{
		$success_story = DB::table('success_stories_langs')
			->where('lang_id', $lang_id)
			->where('url', $slug)
			->first();

		return $success_story->success_story_id ?? 0;
	}

	/**
	 * Get next success story id
	 *
	 * @param integer $success_story_visible_at Success story id
	 * @param integer $lang_id Lang id
	 * @return array
	 */
	public static function firstNextId($success_story, $lang_id = 0)
	{
		$now = date('Y-m-d H:i:s');
		$prev_id = -1;
		$first_id = -1;

		// Get next success story id
		$success_story_test = DB::table('success_stories AS ss')
			->where('ss.id', '!=', $success_story->id);

		if ($lang_id != 0) {
			$success_story_test = $success_story_test
				->join('success_stories_langs AS ssl', 'ssl.success_story_id', '=', 'ss.id')
				->where('ssl.lang_id', $lang_id)
				->where('ss.is_active', 1);
		}

		$success_story_next = clone $success_story_test;
		$success_story_next = $success_story_next
			->where('ss.visible_at', '<=', $success_story->visible_at)
			->orderBy('ss.visible_at', 'DESC')
			->first();

		if ($success_story_next !== null)
			return $success_story_next->id;

		$test = $success_story_test
			->where('ss.visible_at', '<=', $now)
			->orderBy('ss.visible_at', 'DESC')
			->first();

		return $test->id;
	}

	/**
	 * Get last success story url
	 *
	 * @param integer $lang_id Lang id
	 * @return string
	 */
	public static function lastUrl($lang_id)
	{
		$now = date('Y-m-d H:i:s');

		// Get last success story
		$success_story = DB::table('success_stories')
			->where('is_active', 1)
			->where('visible_at', '<=', $now)
			->orderBy('visible_at', 'DESC')
			->first();

		if ($success_story === null)
			return '';

		$success_story = self::initialize(__CLASS__, $success_story, $lang_id);

		return $success_story->langs->url;
	}

	/**
	 * Get success story
	 *
	 * @param integer $success_story_id Success story id
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function one($success_story_id, $lang_id = 0)
	{
		$success_story = DB::table('success_stories')
			->where('id', $success_story_id)
			->first();

		if ($success_story === null)
			return null;

		return self::initialize(__CLASS__, $success_story, $lang_id);
	}

	/**
	 * Get all success stories
	 *
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function all($lang_id = 0)
	{
		$now = date('Y-m-d H:i:s');

		// Get all success stories
		$success_stories = DB::table('success_stories');

		if ($lang_id !== 0) {
			$success_stories = $success_stories
				->where('is_active', 1);
		}

		$success_stories = $success_stories
			->where('visible_at', '<=', $now)
			->orderBy('visible_at', 'DESC')
			->get();

		if ($success_stories === null)
			return null;

		return self::initialize(__CLASS__, $success_stories, $lang_id);
	}

	/**
	 * Insert success story
	 *
	 * @param array $success_story Success story data
	 * @return integer
	 */
	public static function insert($success_story)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'client' => $success_story['client'],
			'is_active' => $success_story['is_active'] ?? 0,
			'is_product' => $success_story['is_product'] ?? 0,
			'visible_at' => $success_story['visible_at'],
			'created_at' => $now,
		];

		// Insert background image
		if (Image::isUploaded($success_story['background'] ?? '') === true)
			$insert_values['background_id'] = Image::upload(0, $success_story['background']);

		// Insert image 1 image
		if (Image::isUploaded($success_story['image_1'] ?? '') === true)
			$insert_values['image_1_id'] = Image::upload(0, $success_story['image_1']);

		// Insert image 2 image
		if (Image::isUploaded($success_story['image_2'] ?? '') === true)
			$insert_values['image_2_id'] = Image::upload(0, $success_story['image_2']);

		// Insert success_story
		$success_story_id = DB::table('success_stories')
			->insertGetId($insert_values);

		// Insert langs
		SuccessStoryLang::update($success_story_id, $success_story['langs'] ?? []);

		// Insert gallery images
		SuccessStoryImage::update($success_story_id, $success_story['gallery'] ?? []);

		return $success_story_id;
	}

	/**
	 * Update success story
	 *
	 * @param integer $success_story_id Success story id
	 * @param array $success_story Success story data
	 * @param object $old_success_story Old success story data
	 * @return void
	 */
	private static function _update($success_story_id, $success_story, $old_success_story)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$update_values = [
			'client' => $success_story['client'],
			'is_active' => $success_story['is_active'] ?? 0,
			'is_product' => $success_story['is_product'] ?? 0,
			'visible_at' => $success_story['visible_at'],
			'updated_at' => $now,
		];

		// Update background image
		if (Image::isUploaded($success_story['background'] ?? null) === true)
			$update_values['background_id'] = Image::upload($old_success_story->background_id ?? 0, $success_story['background']);

		// Update image 1 image
		if (Image::isUploaded($success_story['image_1'] ?? null) === true)
			$update_values['image_1_id'] = Image::upload($old_success_story->image_1_id ?? 0, $success_story['image_1']);

		// Update image 2 image
		if (Image::isUploaded($success_story['image_2'] ?? null) === true)
			$update_values['image_2_id'] = Image::upload($old_success_story->image_2_id ?? 0, $success_story['image_2']);

		// Update success_story
		DB::table('success_stories')
			->where('id', $success_story_id)
			->update($update_values);

		// Update langs
		SuccessStoryLang::update($success_story_id, $success_story['langs'] ?? []);

		// Update gallery images
		SuccessStoryImage::update($success_story_id, $success_story['gallery'] ?? []);

		// Delete images
		Image::delete($success_story['images_delete'] ?? []);
	}

	/**
	 * Delete success story
	 *
	 * @param integer $success_story_id Success story id
	 * @return void
	 */
	private static function _delete($success_story_id)
	{
		DB::table('success_stories')
			->where('id', $success_story_id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make($data, [
			'client' => 'required|string',
			'is_active' => 'boolean',
			'is_product' => 'boolean',
			'visible_at' => 'required|date',
		])->errors();

		$errors->merge(SuccessStoryLang::validateLangs($data['langs'] ?? [], $data['is_product'] ?? 0));

		$errors->merge(Image::validateUpload($data['background'] ?? '', true, 'background'));
		$errors->merge(Image::validateUpload($data['image_1'] ?? '', false, 'image_1'));
		$errors->merge(Image::validateUpload($data['image_2'] ?? '', false, 'image_2'));

		foreach ($data['gallery'] ?? [] as $key => $gallery)
			$errors->merge(Image::validateUpload(['gallery' => [$key => $gallery]], true, 'gallery.'.$key));

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param integer $success_story_id Success story id
	 * @param array $data Array to validate
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateUpdate($success_story_id, $data)
	{
		$errors = Validator::make($data, [
			'client' => 'required|string',
			'is_active' => 'boolean',
			'is_product' => 'boolean',
			'visible_at' => 'required|date',
			'images_delete' => 'array',
		])->errors();

		$errors->merge(SuccessStoryLang::validateLangs($data['langs'] ?? [], $data['is_product'] ?? 0, $success_story_id));

		$errors->merge(Image::validateUpload($data['background'] ?? '', false, 'background'));
		$errors->merge(Image::validateUpload($data['image_1'] ?? '', false, 'image_1'));
		$errors->merge(Image::validateUpload($data['image_2'] ?? '', false, 'image_2'));

		foreach ($data['gallery'] ?? [] as $key => $gallery)
			$errors->merge(Image::validateUpload(['gallery' => [$key => $gallery]], true, 'gallery.'.$key));

		return $errors;
	}
}
