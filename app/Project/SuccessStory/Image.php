<?php

namespace App\Project\SuccessStory;

use App\Project\Image as AppImage;

use Illuminate\Support\MessageBag;

use DB;
use Validator;

class Image
{
	/**
	 * Get success stories images
	 *
	 * @param integer $success_story_id Success story id
	 * @return object
	 */
	public static function get($success_story_id)
	{
		$images_ids = DB::table('success_stories_images')
			->where('success_story_id', $success_story_id)
			->pluck('image_id')
			->toArray();

		return array_map(function($image_id) { return AppImage::one($image_id); }, $images_ids);
	}

	/**
	 * Update success story images
	 *
	 * @param integer $success_story_id Success story id
	 * @param array $data Images to insert
	 * @param array $data_delete Images to delete
	 * @return void
	 */
	public static function update($success_story_id, $data)
	{
		// Upload images
		$images_ids = array_map(function($image) { return AppImage::upload(0, $image); }, $data);

		// Format images
		$insert_values = [];

		foreach ($images_ids as &$image_id) {
			$insert_values[] = [
				'success_story_id' => $success_story_id,
				'image_id' => $image_id,
			];
		}

		// Insert images
		DB::table('success_stories_images')->insert($insert_values);
	}

	/**
	 * Delete success story images
	 *
	 * @param integer $success_story_id Success story id
	 * @return void
	 */
	public static function delete($success_story_id)
	{
		DB::table('success_stories_images')
			->where('success_story_id', $success_story_id)
			->delete();
	}
}
