<?php

namespace App\Project\SuccessStory;

use App\Project\Lang as AppLang;

use DB;
use Validator;

class Lang
{
	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Get success stories langs
	 *
	 * @param integer $success_story_id Success story id
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function get($success_story_id, $lang_id = 0)
	{
		$langs = AppLang::allByIds();

		// Get langs
		$success_stories_langs = DB::table('success_stories_langs')
			->where('success_story_id', $success_story_id);

		if ($lang_id !== 0) {
			$success_stories_langs = $success_stories_langs
				->where('lang_id', $lang_id);
		}

		$success_stories_langs = $success_stories_langs
			->get();

		// Format langs
		$success_stories = [];

		foreach ($success_stories_langs as &$success_story_lang)
			$success_stories[$langs->{$success_story_lang->lang_id}->code] = $success_story_lang;

		if ($lang_id !== 0)
			$success_stories = $success_stories[$langs->{$lang_id}->code];

		return $success_stories;
	}

	/**
	 * Update success story langs
	 *
	 * @param integer $success_story_id Success story id
	 * @param array $data Langs to update
	 * @return void
	 */
	public static function update($success_story_id, $data)
	{
		$langs = AppLang::all();

		// Format langs
		$insert_values = [];

		foreach ($langs as &$lang) {
			$insert_values[] = [
				'success_story_id' => $success_story_id,
				'lang_id' => $lang->id,
				'url' => str_slug($data[$lang->code]['url']),
				'title' => $data[$lang->code]['title'],
				'description' => $data[$lang->code]['description'],
				'step_1_title' => $data[$lang->code]['step_1_title'] ?? '',
				'step_1' => $data[$lang->code]['step_1'] ?? '',
				'step_2_title' => $data[$lang->code]['step_2_title'] ?? '',
				'step_2' => $data[$lang->code]['step_2'] ?? '',
				'step_3_title' => $data[$lang->code]['step_3_title'] ?? '',
				'step_3' => $data[$lang->code]['step_3'] ?? '',
				'step_4_title' => $data[$lang->code]['step_4_title'] ?? '',
				'step_4' => $data[$lang->code]['step_4'] ?? '',
				'step_5_title' => $data[$lang->code]['step_5_title'] ?? '',
				'step_5' => $data[$lang->code]['step_5'] ?? '',
			];
		}

		// Delete langs
		self::delete($success_story_id);

		// Insert langs
		DB::table('success_stories_langs')->insert($insert_values);
	}

	/**
	 * Delete success story langs
	 *
	 * @param integer $success_story_id Success story id
	 * @return void
	 */
	public static function delete($success_story_id)
	{
		DB::table('success_stories_langs')
			->where('success_story_id', $success_story_id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate langs
	 *
	 * @param array $data Array to validate
	 * @param integer $success_story_id Success story id
	 * @return \Illuminate\Support\MessageBag
	 */
	public static function validateLangs($data, $is_product = 0, $success_story_id = 0)
	{
		$langs = AppLang::all();
		$errors = messageBag();
		$lang_exists = false;

		$data['is_product'] = $is_product;

		info([$data]);

		foreach ($langs as &$lang) {
			$lang_exists = true;

			$errors = Validator::make($data, [
				$lang->code.'.url' => 'required|string|success_story_url:'.$success_story_id.','.$lang->id,
				$lang->code.'.title' => 'required|string',
				$lang->code.'.description' => 'required|string',
				$lang->code.'.step_1_title' => 'required|string',
				$lang->code.'.step_1' => 'required|string',
				$lang->code.'.step_2_title' => 'required_unless:is_product,1|string',
				$lang->code.'.step_2' => 'required_unless:is_product,1|string',
				$lang->code.'.step_3_title' => 'required_unless:is_product,1|string',
				$lang->code.'.step_3' => 'required_unless:is_product,1|string',
				$lang->code.'.step_4_title' => 'required_unless:is_product,1|string',
				$lang->code.'.step_4' => 'required_unless:is_product,1|string',
				$lang->code.'.step_5_title' => 'required_unless:is_product,1|string',
				$lang->code.'.step_5' => 'required_unless:is_product,1|string',
			])->errors()->merge($errors);
		}

		return $errors;
	}
}
