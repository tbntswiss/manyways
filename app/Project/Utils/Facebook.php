<?php

namespace App\Project\Utils;

class Facebook
{
	/**
	 * Trigger facebook share
	 *
	 * @return void
	 */
	static public function triggerShare($url)
	{
		$url = 'https://graph.facebook.com/';
		$data = ['query' => ['scrape' => true, 'id' => urlencode($url)]];

		$client = new \GuzzleHttp\Client(['defaults' => ['exceptions' => false]]);
		$response = $client->get($url, $data);
	}
}
