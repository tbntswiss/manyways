<?php

namespace App\Project\Utils;

use Image as FacadesImage;

class Image
{
	/**
	 * Thumbs max width
	 *
	 * @var integer
	 */
	private static $thumb_max_width = 800;

	/**
	 * Image max width
	 *
	 * @var integer
	 */
	private static $image_max_width = 2400;

	/**
	 * Images upload dir
	 *
	 * @var string
	 */
	private static $image_uploads_dir = 'uploads/images';

	/**
	 * Get image max width
	 *
	 * @return integer
	 */
	public static function getMaxWidth()
	{
		return self::$image_max_width;
	}

	/**
	 * Get thumb max width
	 *
	 * @return integer
	 */
	public static function getThumbsMaxWidth()
	{
		return self::$thumb_max_width;
	}

	/**
	 * Format uploads dir images
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatDir($dir = '')
	{
		return slash_end(slash_end(self::$image_uploads_dir).trim($dir, '/'));
	}

	/**
	 * Format uploads dir thumbs
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatThumbsDir($dir = '')
	{
		return slash_end(self::formatDir($dir).'thumbs');
	}

	/**
	 * Format absolute uploads dir images
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatDirPath($dir = '')
	{
		return slash_end(base_path()).self::formatDir($dir);
	}

	/**
	 * Format absolute uploads dir thumbs
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatThumbsDirPath($dir = '')
	{
		return slash_end(base_path()).self::formatThumbsDir($dir);
	}

	/**
	 * Create dir if not exists
	 *
	 * @param string $dir Image dir
	 * @return void
	 */
	private static function initFolder($dir)
	{
		if (is_dir($dir) === false && is_file($dir) === false)
			mkdir($dir, 0755, true);
	}

	/**
	 * Get filepath parts
	 *
	 * @param string $filepath Image filepath
	 * @return string|false
	 */
	private static function getFileParts($filepath)
	{
		$file_parts = explode('/', $filepath);

		$basename = array_pop($file_parts);
		$basename_parts = explode('.', $basename);

		$extension = array_pop($basename_parts);
		$filename = implode('.', $basename_parts);
		$dir = implode('/', $file_parts);

		return [
			'dir' => $dir,
			'extension' => $extension,
			'filename' => $filename,
			'basename' => $basename,
		];
	}

	/**
	 * Get image filename
	 *
	 * @param file $image Image file
	 * @param string $filename Image name
	 * @return string
	 */
	private static function formatName($image, $filename = '', $append = '', $ext = '')
	{
		$image_ext = $ext ?: $image->getClientOriginalExtension();

		return str_slug(trim($filename) ?: rtrim($image->getClientOriginalName(), '.'.$image->getClientOriginalExtension())).$append.'.'.$image_ext;
	}

	/**
	 * Format image dir
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getDir($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDir($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format thumb dir
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getThumbsDir($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatThumbsDir($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format image path
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getDirPath($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDirPath($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format thumb path
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getThumbsDirPath($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatThumbsDirPath($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Upload image file
	 *
	 * @param file $image Image file
	 * @param array $options Image options
	 * @return string
	 */
	public static function upload($image, $options = [])
	{
		$options = array_merge([
			'name' => '',
			'path' => '',
		], $options);

		// Format image
		$basename = self::formatName($image, $options['name'], '', 'jpg');
		$image_dir_path = self::formatDirPath($options['path']);
		$thumbs_dir_path = self::formatThumbsDirPath($options['path']);

		// Init directories
		self::initFolder($image_dir_path);
		self::initFolder($thumbs_dir_path);

		// Check image name
		if (is_file($image_dir_path.$basename) === true)
			$basename = self::formatName($image, $options['name'], '-'.microtime(true), 'jpg');

		// Upload image
		$image->move($image_dir_path, $basename);

		FacadesImage::make($image_dir_path.$basename)
			->encode('jpg', 80)
			->save($image_dir_path.$basename);

		// Upload thumb
		FacadesImage::make($image_dir_path.$basename)
			->widen(self::$thumb_max_width)
			->encode('jpg', 80)
			->save($thumbs_dir_path.$basename);

		return trim(slash_end($options['path']).$basename, '/');
	}

	/**
	 * Delete image file
	 *
	 * @param string $filepath Image filepath
	 * @return void
	 */
	public static function delete($filepath)
	{
		// Delete image
		$image_path = self::getDirPath($filepath);
		if (is_file($image_path) === true) unlink($image_path);

		// Delete thumb
		$thumb_path = self::getThumbsDirPath($filepath);
		if (is_file($thumb_path) === true) unlink($thumb_path);
	}
}
