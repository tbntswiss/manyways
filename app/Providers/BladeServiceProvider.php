<?php

namespace App\Providers;

use Blade;

use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot()
	{
		Blade::directive('define', function($expression)
		{
			$expression = rtrim($expression, ';');

			return "<?php $expression; ?>";
		});

		Blade::directive('script', function($expression)
		{
			$expression = $this->parseExpression($expression);

			return
				"<script type='text/javascript'>".
					"var {{ $expression[1] }} = JSON.parse('{!! stringify(get_data($expression[0])) !!}');".
				"</script>";
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function parseExpression($expression)
	{
		return array_map('trim', explode(',', str_replace(['(', ')'], '', $expression)));
	}
}