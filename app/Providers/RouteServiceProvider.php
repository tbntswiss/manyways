<?php

namespace App\Providers;

use App;
use Lang;
use Config;
use Request;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapRoutesAdmin();
        $this->mapRoutesApi();
        $this->mapRoutesAjax();
        $this->mapRoutesCron();

        $this->switchLang();

        $this->mapRoutesApp();

        //
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public static function switchLang()
    {
        // Get local
        $local = Config::get('app.locale_default');
        $local_request_empty = Config::get('app.locale_prefix_empty');
        $local_request = Request::segment(1);
        $locals = array_merge(Config::get('app.locale_alts'), [$local]);

        // Switch local
        if (in_array($local_request, $locals) === true)
            $local = $local_request;
        else
            $local = $local_request_empty;

        // Set local
        App::setLocale($local);

        // Set route names
        foreach(Lang::get('urls') as $k => $v)
            Route::pattern($k, $v);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRoutesApp()
    {
        Route::group([
            'middleware' => ['base', 'app'],
            'namespace' => $this->namespace.'\App',
        ], function ($router) {
            require base_path('routes/app.php');
        });
    }

    /**
     * Define the "ajax" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapRoutesAjax()
    {
        Route::group([
            'middleware' => ['base', 'ajax'],
            'namespace' => $this->namespace.'\Ajax',
            'prefix' => 'ajax',
        ], function ($router) {
            require base_path('routes/ajax.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapRoutesApi()
    {
        Route::group([
            'middleware' => ['base', 'api'],
            'namespace' => $this->namespace.'\Api',
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRoutesAdmin()
    {
        Route::group([
            'middleware' => ['base', 'admin'],
            'namespace' => $this->namespace.'\Admin',
            'prefix' => 'admin',
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapRoutesCron()
    {
        Route::group([
            'middleware' => ['base', 'cron'],
            'namespace' => $this->namespace.'\Cron',
            'prefix' => 'cron',
        ], function ($router) {
            require base_path('routes/cron.php');
        });
    }
}
