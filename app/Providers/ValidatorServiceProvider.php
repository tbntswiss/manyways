<?php

namespace App\Providers;

use App\Project\SuccessStory;

use ForceUTF8\Encoding;
use Illuminate\Support\ServiceProvider;

use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot()
	{
		Validator::extend('before_equal', function($attribute, $value, $parameters, $validator)
		{
			return strtotime($validator->getData()[$parameters[0]]) <= strtotime($value);
		});

		Validator::extend('after_equal', function($attribute, $value, $parameters, $validator)
		{
			return strtotime($validator->getData()[$parameters[0]]) >= strtotime($value);
		});

		Validator::extend('is_utf8_malformed', function($attribute, $value, $parameters, $validator)
		{
			return $value === '' || $value === Encoding::fixUTF8($value);
		});

		Validator::extend('success_story_url', function($attribute, $value, $parameters, $validator)
		{
			list($success_story_id, $lang_id) = $parameters;
			$success_story_id_exists = SuccessStory::getIdFromSlug(str_slug($value), $lang_id);

			return $success_story_id == $success_story_id_exists || $success_story_id_exists == 0;
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
