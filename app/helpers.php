<?php

use App\Project\Lang;
use App\Project\Utils\DateFormat;

use ForceUTF8\Encoding;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\MessageBag;

/*
|--------------------------------------------------------------------------
| String
|--------------------------------------------------------------------------
*/

function generate_hash()
{
	return sha1(str_random(7).microtime().str_random(22));
}

function str_bool($str)
{
	return $str === true || $str === 'true' || $str === 1 || $str === '1' ? true : false;
}

function trim_end($str, $trim_char)
{
	return rtrim($str, $trim_char).$trim_char;
}

function trim_before($str, $trim_char)
{
	return $trim_char.ltrim($str, $trim_char);
}

function trim_all($str, $trim_char)
{
	return $trim_char.trim($str, $trim_char).$trim_char;
}

function slash_end($str)
{
	return trim_end($str, '/');
}

function slash_before($str)
{
	return trim_before($str, '/');
}

function slash_all($str)
{
	return trim_all($str, '/');
}

function str_url($str)
{
	if (strpos($str, 'https') === 0 || strpos($str, 'http') === 0)
		return $str;

	return 'http://'.ltrim($str, '://');
}



/*
|--------------------------------------------------------------------------
| Arrays
|--------------------------------------------------------------------------
*/

function array_to_object($d)
{
	if (is_array($d) === false && is_object($d) === false)
		return $d;

	return json_decode(json_encode($d));
}

function array_prefix($d, $prefix)
{
	return array_combine(array_map(function($k) use ($prefix) { return $prefix.$k; }, array_keys($d)), $d);
}

function array_reduce_empty($d)
{
	return count(array_filter(array_flatten($d))) === 0;
}

function sort_by($d, $k)
{
	return array_values(array_sort($d, function ($v) use ($k) {
		if (is_array($v) && isset($v[$k])) return $v[$k];
		else if (is_object($v) && isset($v->{$k})) return $v->{$k};
	}));
}

function sort_by_nat(&$d, $k)
{
	usort($d, function($a, $b) use (&$k) {
		if (is_array($a) && isset($a[$k])) return strnatcmp($a[$k], $b[$k]);
		else if (is_object($a) && isset($a->{$k})) return strnatcmp($a->{$k}, $b->{$k});
	});
}



/*
|--------------------------------------------------------------------------
| Objects
|--------------------------------------------------------------------------
*/

function object_to_array($d)
{
	if (is_array($d) === false && is_object($d) === false)
		return $d;

	if (is_object($d) === true)
        $d = get_object_vars($d);

    return is_array($d) === true ? array_map(__FUNCTION__, $d) : $d;
}

function object_merge()
{
	$args = func_get_args();
	$obj = [];

	foreach ($args as &$arg) {
		try {
			$obj = array_replace_recursive($obj, object_to_array($arg));
		}
		catch (ErrorException $e) {
			continue;
		}
	}

	return array_to_object($obj);
}



/*
|--------------------------------------------------------------------------
| Project
|--------------------------------------------------------------------------
*/

function is_project($d)
{
	return is_object($d) === true && get_parent_class($d) === 'App\Project';
}

function get_data($d)
{
	if (is_project($d) === true) {
		$d = $d->getData(); foreach ($d as $key => $value) $d->{$key} = get_data($value);
		return $d;
	}
	else if (is_collection($d) === true) {
		return array_map(__FUNCTION__, $d->toArray());
	}
	else if (is_array($d) === true) {
		return array_map(__FUNCTION__, $d);
	}
	else if (is_object($d) === true) {
		foreach ($d as $key => $value) $d->{$key} = get_data($value);
		return $d;
	}
	else {
		return $d;
	}
}



/*
|--------------------------------------------------------------------------
| Collections
|--------------------------------------------------------------------------
*/

function is_collection($d)
{
	return is_object($d) === true && get_class($d) === 'Illuminate\Support\Collection';
}



/*
|--------------------------------------------------------------------------
| Json
|--------------------------------------------------------------------------
*/

function stringify($d)
{
	$escapers =     ["\\",   "/",   "\"",   "\n",  "\r",  "\t",  "\x08", "\x0c", "\u0022"];
	$replacements = ["\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f",  "\\b",  "\\\\\""];

	return str_replace($escapers, $replacements, json_encode($d, JSON_HEX_QUOT | JSON_HEX_APOS));
}



/*
|--------------------------------------------------------------------------
| Dates
|--------------------------------------------------------------------------
*/

function to_utc_date($date)
{
	return str_replace(' ', 'T', $date).'Z';
}

function format_date($date, $format = 'm F, H:i')
{
	return with(new \DateTime($date))->format($format);
}

function format_date_strf($date, $format = 'm F, H:i')
{
	setlocale(LC_TIME, Lang::getCurrent()->locale.'.UTF-8');
	return strftime(DateFormat::date_format_to_strftime_format($format), format_date($date, 'U'));
}



/*
|--------------------------------------------------------------------------
| App
|--------------------------------------------------------------------------
*/

function is_cli()
{
	return PHP_SAPI === 'cli';
}

function appName()
{
	return env('APP_NAME', 'APP_NAME');
}

function appUrl()
{
	if (is_cli() === true)
		return 'http://localhost';

	return url('');
}

function baseUrl()
{
	return trim(explode('?', str_replace(Request::path(), '/', Request::server('REQUEST_URI')))[0], '/').'/';
}

function langUrl($url)
{
	return \Lang::get('urls.'.$url);
}



/*
|--------------------------------------------------------------------------
| Print
|--------------------------------------------------------------------------
*/

function ddd()
{
	if (PHP_SAPI !== 'cli') {
		echo(
			'<style>'.
				'.sf-dump { font-size: 10px !important; }'.
				'.sf-dump .sf-dump-compact { display: inline !important; font-size: inherit!important; font-family: inherit!important; color: inherit!important; white-space: inherit!important; padding: inherit!important; background: inherit!important; }'.
			'</style>'
		);
	}

	array_map(function ($x) { (new Dumper)->dump($x); }, func_get_args());
}



/*
|--------------------------------------------------------------------------
| Views
|--------------------------------------------------------------------------
*/

function get_view($view, $data)
{
	$view = (string) view($view, $data);
	$view = str_replace(["\n", "\t"], '', $view);

	return $view;
}



/*
|--------------------------------------------------------------------------
| Sessions
|--------------------------------------------------------------------------
*/

function init_post($d, $session = 'post')
{
	return object_merge($d, session($session) === null ? [] : session($session));
}

function clean_post($d)
{
	if (is_array($d) === true) return array_map(__FUNCTION__, $d);
	else return is_uploaded_file($d) ? null : Encoding::fixUTF8($d);
}



/*
|--------------------------------------------------------------------------
| Message bag
|--------------------------------------------------------------------------
*/

function messageBag($key = '', $message = '')
{
	$errors = new MessageBag;
	return $key === '' ? $errors : $errors->add($key, $message);
}



/*
|--------------------------------------------------------------------------
| PDF
|--------------------------------------------------------------------------
*/

function make_pdf($view, $data, $output)
{
	$mpdf = new \mPDF();

	$mpdf->simpleTables = true;
	$mpdf->packTableData = true;
	$mpdf->CSSselectMedia = 'screen';
	$mpdf->ignore_invalid_utf8 = true;

	$mpdf->setFooter('{PAGENO} / {nb}');
	$mpdf->WriteHTML(get_view($view, $data));
	$mpdf->Output($output, 'F');

	return true;
}
