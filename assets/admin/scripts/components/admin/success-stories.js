'use strict';

let el = {};

let _successStories = {};

const init = () =>
{
	_successStories = window.SUCCESS_STORIES || {};

	initDOM();
	initListeners();
	initSuccessStories();
};

const initDOM = () =>
{
	el = {};
	el.search = $('[data-search]');
};

const initListeners = () =>
{
	el.search.on('keyup', function() { search($(this)); });
};

const initSuccessStories = () =>
{
	for (let i = 0, k = _.keys(_successStories), c = k.length; i < c; i++) {
		_successStories[k[i]] = _.assign({
			el: $('[data-success-story-id="' + _successStories[k[i]].id + '"]'),
			search: _successStories[k[i]].client.toLowerCase(),
		}, _successStories[k[i]]);
	}
};

const search = ($search) =>
{
	const search = $search.val().toLowerCase();

	_.map(_successStories, (successStory, successStoryId) => {
		if (successStory.search.indexOf(search) === -1) successStory.el.addClass('uk-hidden');
		else successStory.el.removeClass('uk-hidden');
	});
};

export default {
	init,
};
