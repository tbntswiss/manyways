'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initListeners();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form]');
	el.submit = $('[data-submit]');
};

const initListeners = () =>
{
	el.submit.on('click', function() { createSuccessStory(); });
};

const createSuccessStory = () =>
{
	let formData = new FormData(el.form.get(0));

	// Block form
	el.form.attr('data-disabled', '');
	el.submit.find('[uk-spinner]').setHidden(false);

	// Format errors names
	let errorsNames = {};

	for (let i = 0, ci = Laravel.langs.length; i < ci; i++) {
		for (let j = 0, data = ['url', 'title', 'description', 'step_1_title', 'step_1', 'step_2_title', 'step_2', 'step_3_title', 'step_3', 'step_4_title', 'step_4', 'step_5_title', 'step_5'], cj = data.length; j < cj; j++)
			errorsNames[Laravel.langs[i].code + '.' + data[j]] = 'langs[' + Laravel.langs[i].code + '][' + data[j] + ']';
	}

	_Form.clearErrors();

	// Send ajax call
	_Ajax.post('admin/success-story/create', formData, {
		success: (data) => {
			UIkit.notification({ message: 'The success story was successfully created.', status: 'success' });

			_Pjax.href('admin/success-stories');
		},
		error: (error) => {
			_Form.addErrors(error, {
				alert: true,
				names: errorsNames,
				debug: true,
			});

			// Unblock form
			el.form.removeAttr('data-disabled', '');
			el.submit.find('[uk-spinner]').setHidden(true);
		},
		debug: true,
	});
};

export default {
	init,
};
