'use strict';

let el = {};

let _galleryIncrement = 0;

const init = () =>
{
	initDOM();
	initListeners();

	addImageGallery();
};

const initDOM = () =>
{
	el = {};
	el.gallery_template = $('[data-gallery-template]');
};

const initListeners = () =>
{
	$(document)
		.on('change', '[data-gallery] [data-file!="__GALLERY__"] input', function() { if (this.value !== '') addImageGallery($(this)); })
		.on('click', '[data-gallery] [data-file-remove]', function() { removeImageGallery($(this)); });
};

const addImageGallery = ($input) =>
{
	const $gallery = el.gallery_template.clone();
	const galleryImageId = newGalleryImageId();

	el.gallery_template.parent().append($gallery);

	$gallery.setHidden(false);

	setTimeout(() => {
		$gallery.removeAttr('data-gallery-template');
		$gallery.attr('data-gallery-item', '');

		$gallery.find('[data-file]').attr('data-file', galleryImageId);
		$gallery.find('[data-file-target]').attr('data-file-target', galleryImageId);
	}, 100);

	UIkit.update();
};

const removeImageGallery = ($input) =>
{
	$input.parents('[data-gallery-item]').remove();
};

const newGalleryImageId = () =>
{
	return 'gallery-' + (_galleryIncrement += 1);
};

export default {
	init,
};
