'use strict';

let el = {};

let _successStory = {};

const init = () =>
{
	_successStory = window.SUCCESS_STORY || {};

	initDOM();
	initListeners();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form]');

	el.submit = $('[data-submit]');
	el.delete = $('[data-delete]');
};

const initListeners = () =>
{
	el.submit.on('click', function() { updateSuccessStory(); });
	el.delete.on('click', function() { deleteSuccessStory(); });
};

const updateSuccessStory = () =>
{
	let formData = new FormData(el.form.get(0));

	// Block form
	el.form.attr('data-disabled', '');
	el.submit.find('[uk-spinner]').setHidden(false);

	// Format errors names
	let errorsNames = {};

	for (let i = 0, ci = Laravel.langs.length; i < ci; i++) {
		for (let j = 0, data = ['url', 'title', 'description', 'step_1_title', 'step_1', 'step_2_title', 'step_2', 'step_3_title', 'step_3', 'step_4_title', 'step_4', 'step_5_title', 'step_5'], cj = data.length; j < cj; j++)
			errorsNames[Laravel.langs[i].code + '.' + data[j]] = 'langs[' + Laravel.langs[i].code + '][' + data[j] + ']';
	}

	_Form.clearErrors();

	// Send ajax call
	_Ajax.post('admin/success-story/update/' + _successStory.id, formData, {
		success: (data) => {
			UIkit.notification({ message: 'The success story was successfully updated.', status: 'success' });

			_Pjax.refresh();
		},
		error: (error) => {
			_Form.addErrors(error, {
				alert: true,
				debug: true,
				names: errorsNames,
			});

			// Unblock form
			el.form.removeAttr('data-disabled', '');
			el.submit.find('[uk-spinner]').setHidden(true);
		},
		debug: true,
	});
};

const deleteSuccessStory = () =>
{
	UIkit.modal.confirm('Are you sure you want to delete <strong>' + _successStory.client + '</strong>').then(() => {
		_Form.clearErrors();

		// Block form
		el.form.attr('data-disabled', '');
		el.submit.find('[uk-spinner]').setHidden(false);

		// Send ajax call
		_Ajax.get('admin/success-story/delete/' + _successStory.id, {
			success: (data) => {
				_Pjax.href('admin/success-stories');
			},
			error: (error) => {
				_Form.addErrors(error, {
					alert: true,
					debug: true,
				});

				// Unblock form
				el.form.removeAttr('data-disabled', '');
				el.submit.find('[uk-spinner]').setHidden(true);
			},
			debug: true,
		});
	});
};

export default {
	init,
};
