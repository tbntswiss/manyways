'use strict';

const init = () =>
{
	initListeners();
};

const initListeners = function()
{
	$(document)
		.on('change', '[data-file] input', function() { loadFile($(this)); })
		.on('click', '[data-file-remove]', function() { deleteFile($(this)); });
};

const loadFile = function($input)
{
	const input = $input.get(0);

	if (input.files.length === 0 || input.files[0] === undefined)
		return;

	let reader = new FileReader();

	reader.onerror = (e) => console.warn('Image load error', e);
	reader.onload = (e) => displayFile($input, e.target.result);
	reader.readAsDataURL(input.files[0]);
};

const displayFile = function($input, imageBase64)
{
	const $parent = $input.parents('[data-file]');
	const $target = $('[data-file-target="' + $parent.attr('data-file') + '"]');

	if ($target.find('img').length !== 0)
		$target.find('img').remove();

	$target.setHidden(false);
	$target.prepend('<img src="' + imageBase64 + '" />');

	$parent.setHidden(true);

	UIkit.update();
};

const deleteFile = function($input)
{
	const $target = $input.parents('[data-file-target]');
	const $parent = $('[data-file="' + $target.attr('data-file-target') + '"]');

	const targetName = $target.attr('data-file-target');

	$target.setHidden(true);
	$target.find('img').remove();
	$target.parents('form').append('<input type="hidden" name="images_delete[]" value="' + targetName + '" />');

	$parent.setHidden(false);

	_Utils.emptyInputFile($parent.find('input').get(0));
};

export default {
	init,
};
