'use strict';

const init = () =>
{
	initEvents();
	initMenu();
};

const initEvents = () =>
{
	$(document).on('pjax:callback', function() { initMenu(); });
};

const initMenu = () =>
{
	$('[data-nav-link]').removeClass('is-active');
	$('[data-nav-link~="' + _App.getCurrentMenu() + '"]').addClass('is-active');
};

export default {
	init,
};
