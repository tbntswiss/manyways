'use strict';

const init = () =>
{
	initEvents();
	animEnd();
};

const initEvents = () =>
{
	$(document).on('pjax:send', function() { animEnd(); });
	$(document).on('pjax:callback', function() { animStart(); });
};

const animEnd = () =>
{
	// On page landing
};

const animStart = () =>
{
	// On page leaving
};

export default {
	init,
};
