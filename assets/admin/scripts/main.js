'use strict';

import _Ajax from './modules/ajax';
import _App from './modules/app';
import _Form from './modules/form';
import _Modal from './modules/modal';
import _ModalSelect from './modules/modal-select';
import _OffCanvas from './modules/off-canvas';
import _Pjax from './modules/pjax';
import _SelectAll from './modules/select-all';
import _Trans from './modules/trans';
import _Utils from './modules/utils';

import AdminSuccessStories from './components/admin/success-stories';
import AdminSuccessStory from './components/admin/success-story';
import AdminSuccessStoryGallery from './components/admin/success-stories/gallery';
import AdminSuccessStoryCreate from './components/admin/success-stories/create';
import AdminSuccessStoryUpdate from './components/admin/success-stories/update';

import Menu from './components/menu';
import Images from './components/images';
import Pages from './components/pages';

_Utils.access(_Ajax, '_Ajax');
_Utils.access(_App, '_App');
_Utils.access(_Form, '_Form');
_Utils.access(_Modal, '_Modal');
_Utils.access(_ModalSelect, '_ModalSelect');
_Utils.access(_OffCanvas, '_OffCanvas');
_Utils.access(_Pjax, '_Pjax');
_Utils.access(_SelectAll, '_SelectAll');
_Utils.access(_Trans, '_Trans');
_Utils.access(_Utils, '_Utils');

$(document).ready(function()
{
	Menu.init();
	Pages.init();

	_App.registerJs([
		{ page: 'admin.success_stories', js: [AdminSuccessStories] },
		{ page: 'admin.success_story.create', js: [Images, AdminSuccessStoryGallery, AdminSuccessStoryCreate] },
		{ page: 'admin.success_story.update', js: [Images, AdminSuccessStoryGallery, AdminSuccessStoryUpdate] },
	]);
});
