'use strict';

const get = (url, options) =>
{
	return execute(url, {}, _.assign(options, { type: 'GET' }));
};

const post = (url, data, options) =>
{
	return execute(url, data, _.assign(options, { type: 'POST' }));
};

const execute = (url, data, options) =>
{
	// Init options
	let ajaxOptions = _.assign({
		url: url,
		data: data,
		type: 'GET',
		timeout: 60 * 1000,
		cache: false,
		complete: () => {},
	}, options);

	// Set success callback
	ajaxOptions.success = (response) => {
		if (options.debug === true) console.log('Ajax success:', url, response);
		if (options.success) options.success(response);
	};

	// Set error callback
	ajaxOptions.error = (response) => {
		if (options.debug === true) console.warn('Ajax error:', url, response);
		if (options.error) options.error(response);
	};

	// Set data options
	if (_Utils.isObjectType(data, 'FormData') === true) {
		ajaxOptions.processData = false;
		ajaxOptions.contentType = false;

		if (ajaxOptions.type === 'POST')
			ajaxOptions.data.append('_token', Laravel.token);

		ajaxOptions.data.append('lang_code', Laravel.lang.code);
	}
	else {
		if (ajaxOptions.type === 'POST')
			ajaxOptions.data._token = Laravel.token;

		ajaxOptions.data.lang_code = Laravel.lang.code;
	}

	// Debug
	if (options.debug === true)
		console.debug('Ajax start:', url, ajaxOptions);

	// Execute request
	return $.ajax(ajaxOptions);
};

const isResponseSuccess = (response) =>
{
	const errorCode = Number(_Utils.isObject(response) ? response.status: 0);
	return errorCode >= 200 && errorCode < 300;
};

const isResponseError = (response) =>
{
	return isResponseSuccess(response) === false;
};

export default
{
	get,
	post,
	isResponseSuccess,
	isResponseError,
};
