'use strict';

let _config = [];

const callback = function()
{
	callJs();
};

const getCurrentMenu = function()
{
	return Laravel.menu;
};

const getLangId = function()
{
	return Laravel.lang.id;
};

const getLangCode = function()
{
	return Laravel.lang.code;
};

const isJs = function(page)
{
	return Laravel.js.indexOf(page) > -1;
};

const registerJs = function(config)
{
	_config = _.concat([], _config, config);

	callJs();
};

const callJs = function(config)
{
	_.map(_config, (config) => {
		if (isJs(config.page) === false)
			return;

		if (Array.isArray(config.js) === false)
			config.js = [config.js];

		_.map(config.js, (js) => {
			if (js.init !== undefined) js.init();
			else console.warn('Undefined method "init":', _Utils.getObjectType(js), js);
		});
	});
};

export default
{
	getCurrentMenu,
	getLangId,
	getLangCode,
	isJs,
	registerJs,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
