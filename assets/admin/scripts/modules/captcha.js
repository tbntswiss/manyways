'use strict';

import _App from '../modules/app';

var el = {};
var sitekey = '6LctDwkUAAAAACF6gOD49TbUOPlRq3Q66bFdfhc6';
var recaptcha = null;

var callback = function()
{
	initCaptcha();
};

var initCaptcha = function()
{
	if (recaptcha !== null)
		resetCaptcha();

	recaptcha = null;

	if ($('#g-recaptcha').length > 0) {
		$('#g-recaptcha').empty();

		recaptcha = grecaptcha.render('g-recaptcha', { sitekey: sitekey, lang: _App.getLangId() });
	}
};

var resetCaptcha = function()
{
	grecaptcha.reset(recaptcha);
};

var verifyCaptcha = function(verifyCallback)
{
	verifyCallback = verifyCallback || function(){};

	if (grecaptcha === null) {
		verifyCallback({ success: false });
		return false;
	}

	// Get values
	var values = {};
		values.lang_id = _App.getLangId();
		values.captcha_response = grecaptcha.getResponse(recaptcha);

	// Send form
	$.ajax({
		url: 'ajax/captcha',
		data: values,
		type: 'POST',
		timeout: 60 * 1000,
		cache: false,
		success: function(data)
		{
			verifyCallback(data.response);
		},
		error: function(error)
		{
			console.warn('An error occured during captcha send.');
			verifyCallback({ success: false, errors: error.responseJSON && error.responseJSON.errors ? error.responseJSON.errors : [] });
		},
		complete: function()
		{
			resetCaptcha();
		},
	});
};

export default
{
	init: initCaptcha,
	verify: verifyCaptcha,
	reset: resetCaptcha,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
