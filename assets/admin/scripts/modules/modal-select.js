'use strict';

const _ModalSelect = function(config)
{
	let el = {};

	let _mixitup = null;
	let _config = _.assign({ items: {}, mixitup: '' }, config);

	const init = () =>
	{
		if (_config.parent === undefined || _config.parent === '')
			return initError('ModalSelect missing "parent" parameter:');

		if (_config.$active_group === undefined || _Utils.isJquery(_config.$active_group) === false || _config.$active_group.length === 0)
			return initError('ModalSelect missing "active_group" parameter:');

		if (_config.$inactive_group === undefined || _Utils.isJquery(_config.$inactive_group) === false || _config.$inactive_group.length === 0)
			return initError('ModalSelect missing "inactive_group" parameter:');

		initDOM();
		initEvents();

		return true;
	};

	const initError = () =>
	{
		return console.warn(message, _config) || false;
	};

	const initDOM = () =>
	{
		el = {};

		el.active_group = _config.$active_group;
		el.inactive_group = _config.$inactive_group;

		el.search = _config.$search;
		el.search_none = el.inactive_group.find('[data-select-search-none]');
	};

	const initEvents = () =>
	{
		el.active_group.on('change', 'input', function() { toggleItem($(this)); });
		el.inactive_group.on('change', 'input', function() { toggleItem($(this)); });

		if (el.search !== undefined)
			el.search.on('keyup', function() { search(); });
	};

	const getActive = () =>
	{
		return _.filter(_config.items, (item) => { if (Boolean(item._selected) === true) return item; });
	};

	const setActive = (itemsIds) =>
	{
		_.map(_config.items, (item) => {
			if (itemsIds.indexOf(item.id) !== -1 && Boolean(item._selected) === false) {
				moveToActiveGroup(getItem(item.id));
			}
			else if (itemsIds.indexOf(item.id) === -1 && Boolean(item._selected) === true) {
				moveToInactiveGroup(getItem(item.id));
			}
		});

		shouldShowSearchNone();
	};

	const toggleItem = ($select) =>
	{
		const $selectBox = $select.parents('[' + _config.parent + ']');

		// Change group
		if ($select.is(':checked') === true)
			moveToActiveGroup($selectBox);
		else
			moveToInactiveGroup($selectBox);

		// Update search result
		search();
	};

	const moveToActiveGroup = ($item) =>
	{
		const itemId = Number($item.attr(_config.parent));

		$item.detach().appendTo(el.active_group);
		$item.find('input').prop('checked', true);

		el.active_group.parent().setHidden(false);
		el.inactive_group.parent().setHidden(el.inactive_group.find('[' + _config.parent + ']').length === 0);

		if (_config.items[itemId] !== undefined)
			_config.items[itemId]._selected = true;

		// Update order
		updateGroup(el.active_group);
		UIkit.update();
	};

	const moveToInactiveGroup = ($item) =>
	{
		const itemId = Number($item.attr(_config.parent));

		$item.detach().appendTo(el.inactive_group);
		$item.find('input').prop('checked', false);

		el.inactive_group.parent().setHidden(false);
		el.active_group.parent().setHidden(el.active_group.find('[' + _config.parent + ']').length === 0);

		if (_config.items[itemId] !== undefined)
			_config.items[itemId]._selected = false;

		// Update order
		updateGroup(el.inactive_group);
		UIkit.update();
	};

	const updateGroup = (group) =>
	{
		if (_config.mixitup === undefined)
			return;

		if (_mixitup !== null)
			_mixitup.destroy();

		_mixitup = mixitup(group.get(0), { load: { sort: _config.mixitup } });
	};

	const updateGroups = () =>
	{
		updateGroup(el.active_group);
		updateGroup(el.inactive_group);

		el.active_group.parent().setHidden(el.active_group.find('[' + _config.parent + ']').length === 0);
		el.inactive_group.parent().setHidden(el.inactive_group.find('[' + _config.parent + ']').length === 0);
	};

	const search = () =>
	{
		if (el.search === undefined)
			return;

		const search = _.trim(el.search.val().toLowerCase());

		_.map(_config.items, (item, itemId) => item._el.setHidden(item._search.indexOf(search) === -1 && item._selected === false));

		shouldShowSearchNone();
	};

	const emptySearch = () =>
	{
		el.search.val('');

		search();
	};

	const shouldShowSearchNone = () =>
	{
		if (el.search === undefined)
			return;

		if (el.search_none.length !== 0)
			el.search_none.setHidden(el.inactive_group.find('[' + _config.parent + ']').filter(':visible').length !== 0);

		UIkit.update();
	};

	const getItem = (itemId) =>
	{
		let $item = el.active_group.find('[' + _config.parent + '="' + itemId + '"]');
		if ($item.length !== 0) return $item;

		$item = el.inactive_group.find('[' + _config.parent + '="' + itemId + '"]');
		if ($item.length !== 0) return $item;
	};

	const addItem = (item) =>
	{
		_config.items[item.id] = item;

		el.inactive_group.append(item._el);

		if (Boolean(item._selected) === true)
			moveToActiveGroup(getItem(item.id));
	};

	const removeItem = (itemId) =>
	{
		delete _config.items[itemId];

		getItem(itemId).remove();
	};

	return init() && {
		getActive,
		setActive,
		updateGroups,
		emptySearch,
		addItem,
		removeItem,
	};
};

export default _ModalSelect;
