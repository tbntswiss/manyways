'use strict';

let _lastModal = null;

const getModalId = function(modalId)
{
	if (_Utils.isJquery(modalId) === true)
		modalId = '[data-modal="' + modalId.attr('data-modal') + '"]';

	return modalId;
};

const show = function(modalId)
{
	_lastModal = modalId;
	UIkit.modal(getModalId(modalId))[0].show();

	scrollTo(modalId);
};

const scrollTo = function(modalId, top)
{
	const $modal = $(getModalId(modalId));
	if ($modal.find('.uk-modal-body').length) $modal.find('.uk-modal-body').scrollTop(0);
};

const hide = function(modalId)
{
	UIkit.modal(getModalId(modalId))[0].hide();
};

const hideLast = function()
{
	if (_lastModal !== null) UIkit.modal(getModalId(_lastModal))[0].hide();
};

export default
{
	show,
	hide,
	hideLast,
};
