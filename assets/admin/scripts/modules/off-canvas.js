'use strict';

let _lastCanvas = null;

const getCanvasId = function(canvasId)
{
	if (_Utils.isJquery(canvasId) === true)
		canvasId = '[data-off-canvas="' + canvasId.attr('data-off-canvas') + '"]';

	return canvasId;
};

const show = function(canvasId)
{
	_lastCanvas = canvasId;
	setTimeout(() => UIkit.offcanvas(getCanvasId(canvasId))[0].show());
};

const hide = function(canvasId)
{
	UIkit.offcanvas(getCanvasId(canvasId))[0].hide();
};

const hideLast = function()
{
	if (_lastCanvas !== null) UIkit.offcanvas(getCanvasId(_lastCanvas))[0].hide();
};

export default
{
	show,
	hide,
	hideLast,
};
