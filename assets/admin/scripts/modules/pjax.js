'use strict';

import Pjax from '../../../vendor/pjax/index';

let pjax = null;
let baseUrl = '';
let fullUrl = '';
let referrer = '';
let oldReferrer = '';

const callback = function()
{
	initPjax();
	initEvents();
};

const initPjax = function()
{
	baseUrl = $('head').find('base').attr('href');
	fullUrl = window.location.href;

	pjax = new Pjax({
		elements: '_LetTheEventsDoIt_',
		selectors: ['head title', '#app'],
		// debug: true,
	});

	$(document)
		.on('pjax:send', pjaxSend)
		.on('pjax:complete', pjaxComplete)
		.on('pjax:success', pjaxSuccess)
		.on('pjax:error', pjaxError);
};

const initEvents = function()
{
	$(document)
		.on('click', 'a[pjax]', goTo)
		.on('click', '[data-href]', goTo);
};

const pjaxSend = function()
{

};

const pjaxComplete = function()
{
	$('*').off('click keyup keydown mouseenter mouseleave touch touchstart touchend submit change focus scroll');
	$(document).off('click keyup keydown mouseenter mouseleave touch touchstart touchend submit change focus scroll');
	$(window).off('resize');

	initEvents();

	$(document).trigger('pjax:callback', { referrer: oldReferrer });
	oldReferrer = referrer;
};

const pjaxSuccess = function()
{

};

const pjaxError = function(error)
{
	console.warn('Pjax error:', error);
};

const goTo = function(e)
{
	e.preventDefault();
	href($(this).attr('href') || $(this).attr('data-href'));
};

const href = function(url, force)
{
	referrer = url;
	fullUrl = baseUrl + $.trim(url, '/');

	if (force === true)
		pjax.latestChance(fullUrl);
	else
		pjax.loadUrl(fullUrl, pjax.options);
};

const refresh = function(force)
{
	href(getCurrentRoute(), force);
};

const getFullUrl = function()
{
	return fullUrl;
};

const getCurrentRoute = function()
{
	return fullUrl.replace(baseUrl, '');
};

export default
{
	href,
	refresh,
	getFullUrl,
	getCurrentRoute,
};

$(document).ready(callback);
