'use strict';

let el = {};

const callback = () =>
{
	initListeners();
};

const initListeners = () =>
{
	$(document).on('change', '[data-select-all-item]', toggleItem);
	$(document).on('change', '[data-select-all]', toggleItemsGroup);
};

const toggleItem = function (selectAllId)
{
	const $item = $(this);

	if (typeof selectAllId !== 'string')
		selectAllId = $item.attr('data-select-all-item');

	const $items = $('[data-select-all-item="' + selectAllId + '"]');
	const $itemsChecked = $('[data-select-all-item="' + selectAllId + '"]:checked');
	const $itemsGroup = $('[data-select-all="' + selectAllId + '"]');

	if ($itemsGroup.length > 0)
		$itemsGroup.prop('checked', $items.length === $itemsChecked.length);

	emit($item, selectAllId);
};

const toggleItemsGroup = function ()
{
	const $itemsGroup = $(this);

	const selectAllId = $itemsGroup.attr('data-select-all');
	const isChecked = $itemsGroup.is(':checked');

	const $items = $('[data-select-all-item="' + selectAllId + '"]');

	if ($items.length > 0)
		$items.prop('checked', isChecked);

	emit($itemsGroup, selectAllId);
};

const emit = ($select, selectAllId) =>
{
	$(document).trigger('select-all.' + selectAllId, $select);
};

const refresh = (selectAllId) =>
{
	toggleItem(selectAllId);
};

const activeGroup = (itemsGroupName, checked) =>
{
	const $items = $('[data-select-all-item="' + itemsGroupName + '"]');
	const $itemsGroup = $('[data-select-all="' + itemsGroupName + '"]');

	if ($items.length > 0)
		$items.prop('checked', checked);

	if ($itemsGroup.length > 0)
		$itemsGroup.prop('checked', checked);
};

const isItemChecked = (itemsGroupName) =>
{
	return $('[data-select-all-item="' + itemsGroupName + '"]:checked').length > 0;
};

const isItemsGroupChecked = (itemsGroupName) =>
{
	return $('[data-select-all="' + itemsGroupName + '"]').prop('checked');
};

export default
{
	refresh,
	activeGroup,
	isItemChecked,
	isItemsGroupChecked,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
