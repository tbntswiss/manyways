'use strict';

var el = {};
var shareWindowWidth = 600;

var callback = function()
{
	initDOM();

	if (el.share_facebook.length > 0)
		initListeners();
};

var initDOM = function()
{
	el = {};

	el.share_facebook = $('[data-share="facebook"]');
	el.share_twitter = $('[data-share="twitter"]');
	el.share_whatsapp = $('[data-share="linkedin"]');
};

var initListeners = function()
{
	el.share_facebook.on('click', shareFacebook);
	el.share_twitter.on('click', shareTwitter);
	el.share_whatsapp.on('click', shareLinkedIn);
};

var getBoxPosition = function()
{
	return ($(window).width() - shareWindowWidth) / 2;
};

var getCurrentUrl = function()
{
	return window.location.href;
};

var shareFacebook = function()
{
	requestLink('https://facebook.com/sharer.php?u=');
};

var shareTwitter = function()
{
	requestLink('https://twitter.com/intent/tweet?via=Brands__Up&url=');
};

var shareLinkedIn = function()
{
	requestLink('https://www.linkedin.com/cws/share?url=');
};

var openShareModal = function(socialUrl, shareUrl)
{
	var leftPos = getBoxPosition();
	window.open(socialUrl + shareUrl, '_blank', 'status=0, menubar=0, toolbar=0, resizable=1, scrollbars=1, top=100, left=' + leftPos + ', width=' + shareWindowWidth + ', height=370');
};

var requestLink = function(socialUrl)
{
	openShareModal(socialUrl, getCurrentUrl());
};

export default
{
	facebook: shareFacebook,
	twitter: shareTwitter,
	linkedin: shareLinkedIn,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
