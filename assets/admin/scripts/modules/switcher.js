'use strict';

let switchers = {};

const callback = () =>
{
	initEvents();
	initSwitcher();
};

const initEvents = () =>
{
	$(document).on('show', '.uk-switcher', function(e, tab) { updateActive(tab); });
};

const initSwitcher = () =>
{
	for (let i = 0, k = _.keys(switchers), c = k.length; i < c; i++) {
		if ($('[data-switcher-id="' + k[i] + '"]').length > 0) UIkit.tab('[data-switcher-id="' + k[i] + '"]')[0].show(switchers[k[i]]);
	}
};

const updateActive = (tab) =>
{
	if (tab.$el.eq(0).attr('data-switcher-id') !== undefined)
		switchers[tab.$el.eq(0).attr('data-switcher-id')] = tab.$el.find('.uk-active').index();
};

export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
