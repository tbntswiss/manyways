'use strict';

const get = (key, params) =>
{
	let trans = _Utils.deepValue(window.Laravel.page_trans, key) || key;

	for (let i = 0, k = _.keys(params), c = k.length; i < c; i++)
		trans = trans.replace(':' + k[i], params[k[i]]);

	return trans;
};

const getAll = () =>
{
	return window.Laravel.page_trans;
};

export default {
	get,
	getAll,
};
