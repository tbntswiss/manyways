'use strict';

import _App from '../modules/app';
import _Pjax from '../modules/pjax';
import _Utils from '../modules/utils';
import _Captcha from '../modules/captcha';

var confirm_url = null;
var isSending = false;

const callback = function()
{
	if (_App.isJs('contact') === true){
		formType();
	}
};


var formType = function()
{
	$('.area-container_area').focusout(function(){
		var length = $(this).val().length;
		var label = $(this).parent().find('.area-container_label');
		var require = $(this).parent().find('.area-container_required');
		if(length != 0) {
			label.addClass('filled');
			require.removeClass('require');
			$(this).removeClass('require');
		}
		else {
			label.removeClass('filled');
			require.addClass('require');
			$(this).addClass('require');
		}
	});

	$('.area-container_area').focus(function(){
		var require = $(this).parent().find('.area-container_required');
		require.removeClass('require');
		$(this).removeClass('require');
		//$('body').addClass('no-transition');
	});



	$('.input-container_input').focusout(function(){
		var length = $(this).val().length;
		var label = $(this).parent().find('.input-container_label');
		var require = $(this).parent().find('.input-container_required');
		if(length != 0) {
			label.addClass('filled');
			if($(this).attr('required') == 'required'){
				require.removeClass('require');
				$(this).removeClass('require');
			}
		}
		else {
			label.removeClass('filled');
			if($(this).attr('required') == 'required'){
				require.addClass('require');
				$(this).addClass('require');
			}
		}
	});

	$('.input-container_input').focus(function(){
		if($(this).attr('required') == 'required'){
			var require = $(this).parent().find('.input-container_required');
			require.removeClass('require');
			$(this).removeClass('require');
		}
		//$('body').addClass('no-transition');
	});

	$('.submit').click(function() {
		confirm_url = $(this).attr('data-url');
		checkCaptcha();
	});
};



var checkCaptcha = function()
{
	sendContact();

	// _Captcha.verify(function(response)
	// {
	// 	if (response === undefined || response.success === false) {
	// 		// showLoader(false);
	// 		// showError(true, response && response.errors ? response.errors.join('<br />') : trans.captcha_error);
	// 		console.warn(response);
	// 	}
	// 	else {
	// 		sendContact(response);
	// 	}
	// });
};


var sendContact = function(response)
{
	if (isSending === true) return;
		isSending = true;

	$('.link-cta.submit').addClass('send');

	// Get values
	var values = getValues();
		values.lang_id = _App.getLangId();
		// values.captcha_verify = response.challenge_ts;

	// Send new comment
	$.ajax({
		url: 'ajax/contact',
		data: values,
		type: 'POST',
		timeout: 60 * 1000,
		cache: false,
		success: function(data)
		{
			// Reset values
			resetValues();

			// Redirect
			_Pjax.href(confirm_url);
		},
		error: function(error)
		{
			$('.infos-error-form').addClass('display');
			$('.infos-error-form').empty();
			$.each(error.responseJSON, function(i){
				$('.infos-error-form').append('<p>' + error.responseJSON[i] + '</p>');
			});
			$('.link-cta.submit').removeClass('send');
		},
		complete: function()
		{
			isSending = false;

			// Reset captcha
			// _Captcha.reset();
		},
	});
};


var getValues = function()
{
	return {
		_token: Laravel.token,
		surname: $('[name="surname"]').val(),
		firstname: $('[name="firstname"]').val(),
		email: $('[name="email"]').val(),
		phone: $('[name="tel"]').val(),
		message: $('[name="message"]').val(),
	};
};


var resetValues = function()
{
	return {
		surname: $('[name="surname"]').val(''),
		firstname: $('[name="firstname"]').val(''),
		email: $('[name="email"]').val(''),
		phone: $('[name="tel"]').val(''),
		message: $('[name="message"]').val(''),
	};
};


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
