'use strict';

import _App from '../modules/app';
import _Utils from '../modules/utils';


const callback = function()
{

	if (_App.isJs('home') === true || _App.isJs('contact') === true || _App.isJs('products') === true || _App.isJs('404') === true || _App.isJs('confirm') === true){
		$.each($('.hero--header'), function(){
			init($(this));
		});
	}

};

var init = function(arg)
{

	var base 	= arg;

	var fo 		= base.find('.fo')[0];
	var fs 		= base.find('.fs')[0];
	var bo 		= base.find('.bo')[0];
	var bs 		= base.find('.bs')[0];

	var back 	= base.find('.header--back')[0];
	var front 	= base.find('.header--front')[0];

	var w 		= window.innerWidth;

	if(w > 1440){
		placeImgBig(fo,fs,bo,bs);
	}
	else if(w < 768){
		placeImgSmall(fo,fs,bo,bs);
	}
	else if(w >= 768 && w <= 1440){
		placeImgDefault(fo,fs,bo,bs);
	}

	window.addEventListener('resize', function(){
		var w = window.innerWidth;

		if(w > 1440){
			placeImgBig(fo,fs,bo,bs);
		}
		else if(w < 768){
			placeImgSmall(fo,fs,bo,bs);
		}
		else if(w >= 768 && w <= 1440){
			placeImgDefault(fo,fs,bo,bs);
		}
	});

	var where = true;

	$('#home--slider').on('mousemove', function(e){

		where = false;

		var x 	= e.clientX;
		var y 	= e.clientY;

		var wi 	= '';
		var he 	= '';

		var dx 	= x/400;
		var dy 	= y/400;

		front.style.transform 	= 'translateX(' + (dx*2) + '%) translateY(' + (dy/2) + '%)';
		back.style.transform 	= 'translateX(' + (dx/5) + '%) translateY(' + (dy/20) + '%)';

	});

	base.on('mousemove', function(e){
		if(where == true && $(window).width() > 769){
			var x 	= e.clientX;
			var y 	= e.clientY;

			var wi 	= '';
			var he 	= '';


			var dx 	= x/400;
			var dy 	= y/400;


			front.style.transform 	= 'translateX(' + (dx*2) + '%) translateY(' + (dy/5) + '%)';
			back.style.transform 	= 'translateX(' + (dx/5) + '%) translateY(' + (dy/20) + '%)';
		}
	});

};


var placeImgDefault = function(fo,fs,bo,bs)
{
	var imgs 	= [fo,fs,bo,bs];

	$.each(imgs, function(i){
		var thisImg 		= imgs[i];
		var x 				= thisImg.getAttribute('data-x');
		var y 				= thisImg.getAttribute('data-y');
		var width 			= thisImg.getAttribute('data-width');

		thisImg.style.width = width + 'px';

		if(thisImg.className === 'fo'){
			thisImg.style.left 		= x + 'vw';
			thisImg.style.top 		= y + 'vh';
		}
		else if(thisImg.className === 'fs'){
			thisImg.style.left 		= x + 'vw';
			thisImg.style.bottom 	= y + 'vh';
		}
		else if(thisImg.className === 'bo'){
			thisImg.style.right 	= x + 'vw';
			thisImg.style.top 		= y + 'vh';
		}
		else if(thisImg.className === 'bs'){
			thisImg.style.right 	= x + 'vw';
			thisImg.style.bottom 	= y + 'vh';
		}
	});

	return;
};


var placeImgSmall = function(fo,fs,bo,bs)
{
	var imgs 	= [fo,fs,bo,bs];

	$.each(imgs, function(i){
		var thisImg 		= imgs[i];
		var x 				= thisImg.getAttribute('data-x');
		var y 				= thisImg.getAttribute('data-y');
		var width 			= thisImg.getAttribute('data-width');
		var widthSmall		= width / 1440 * 180;

		thisImg.style.width = widthSmall + 'vw';

		if(thisImg.className === 'fo'){
			thisImg.style.left 		= '55vw';
			thisImg.style.bottom 	= '-20vh';
		}
		else if(thisImg.className === 'fs'){
			thisImg.style.left 		= '45vw';
			thisImg.style.bottom 	= '-30vh';
		}
		else if(thisImg.className === 'bo'){
			thisImg.style.right 	= '55vw';
			thisImg.style.top 		= '0vh';
		}
		else if(thisImg.className === 'bs'){
			thisImg.style.right 	= '55vw';
			thisImg.style.top 		= '15vh';
		}
	});

	return;
};


var placeImgBig = function(fo,fs,bo,bs)
{
	var imgs 	= [fo,fs,bo,bs];

	$.each(imgs, function(i){
		var thisImg 		= imgs[i];
		var x 				= thisImg.getAttribute('data-x');
		var y 				= thisImg.getAttribute('data-y');
		var width 			= thisImg.getAttribute('data-width');
		var widthBig		= width / 1440 * 100;

		thisImg.style.width = widthBig + 'vw';

		if(thisImg.className === 'fo'){
			thisImg.style.left 		= x + 'vw';
			thisImg.style.top 		= y + 'vh';
		}
		else if(thisImg.className === 'fs'){
			thisImg.style.left 		= x + 'vw';
			thisImg.style.bottom 	= y + 'vh';
		}
		else if(thisImg.className === 'bo'){
			thisImg.style.right 	= x + 'vw';
			thisImg.style.top 		= y + 'vh';
		}
		else if(thisImg.className === 'bs'){
			thisImg.style.right 	= x + 'vw';
			thisImg.style.bottom 	= y + 'vh';
		}
	});

	return;
};



export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
