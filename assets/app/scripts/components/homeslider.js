'use strict';

import _App from '../modules/app';
import _Utils from '../modules/utils';


const callback = function()
{
	if (_App.isJs('home') === false){
		return;
	}
	else if (_App.isJs('home') === true){
		init();
	}
};


var init = function()
{

	var w 		= document.querySelector('.s-1').offsetWidth;
	var bw 		= 120;

	var rtime;
	var timeout = false;
	var delta 	= 200;

	$(window).on('resize', function(){
		rtime = new Date();
		if (timeout === false) {
			timeout = true;
			setTimeout(resizeend, delta);
		}

		if ($(window).width() <= 769) {
			main.disable();
		}
		else if ($(window).width() > 769) {
			main.enable();
		}

		if(document.querySelector('.s-1') != null){
			w = document.querySelector('.s-1').offsetWidth;
			return w;
		}
	});


	//IF RESIZE, WAIT END OF RESIZE TO UPDATE POSITION.
	function resizeend() {
		if (new Date() - rtime < delta) {
			setTimeout(resizeend, delta);
		}
		else {
			timeout = false;

			//CHECK SNAP
			main.x = current * w * (-1);
			main.scrollProxy.left(main.x + w);
			main.update();

		}
	}




	var isMoving 	= false;
	var current 	= 1;
	var active 		= 1;

	var click 	= false;

	var select;

	selectActive();

	var main = Draggable.create("#home--slider", {
		type:"scrollLeft",
		lockAxis: true,
		edgeResistance:0.85,
		throwProps:true,
		minDuration: .4,
		maxDuration: .8,
		onPress: addGrab,
		onDragStart: killTweens,
		onDrag: updateMain,
		onThrowUpdate: updateMain,
		onDragEnd: check,
		onRelease: removeGrab,
		snap:{
			x: function(endValue){
				var stick = Math.round((endValue*(-1)) / w) * w;
				current = Math.round((endValue*(-1)) / w) + 1;
				if(current < 1){
					current = 1;
				}
				if(current >= 10){
					stick = Math.round(((endValue/current*10)*(-1)) / w) * w;
					current = 10;
				}
				return stick;
			}
		}
	})[0];


	if ($(window).width() <= 769) {
		main.disable();
		Draggable.get("#home--slider").disable();
	}


	function updateMain(){

		document.querySelector('.bottom-slider').style.left = (main.x/w*bw) + 'px';

		if(active != current){
			selectActive();
		}

		//move active title depending on drag position
		var titleActive 	= select.find('h1')[0];
		var decalActive 	= main.x + ((current - 1)*w);

		titleActive.style.transform = 'translateX(' + (decalActive / 10) + '%)';

		main.update();
	}


	function killTweens() {
		TweenLite.killTweensOf(main);
	}


	function check(){

		main.update();


		if(current <= 1){
			current = 1;
			$('.home-controls .prev').addClass('unselect');
			$('.bottom-infos').removeClass('hide');
			$('.bottom-slider-bounds').removeClass('show');
			$('.menu--toggle').removeClass('white-skin');
		}
		else{
			$('.home-controls .prev').removeClass('unselect');
			$('.bottom-infos').addClass('hide');
			$('.bottom-slider-bounds').addClass('show');
			$('.menu--toggle').addClass('white-skin');
		}


		if(current <= 2){
			$('.bottom-img-infos').removeClass('hide');
		}
		else{
			$('.bottom-img-infos').addClass('hide');
		}


		if(current >= 10){
			current = 10;
			$('.home-controls .next').addClass('unselect');
		}
		else{
			$('.home-controls .next').removeClass('unselect');
		}

		main.update();
	}

	function addGrab(){
		$('#home--slider').addClass('grabbing');
	}

	function removeGrab(){
		$('#home--slider').removeClass('grabbing');
	}

	//prev/next
	$('.home-controls .prev').on('click', function(e){
		prev();
	});


	$('.home-controls .next').on('click', function(e){
		next();
	});


	//SAME WITH ARROW KEY
	$(window).on('keydown', function(e) {
		if(e.keyCode == 37 && $(window).scrollTop() < 200 && $('#home--slider').length != 0){
			prev();
			return false;
		}
		else if(e.keyCode == 39 && $(window).scrollTop() < 200 && $('#home--slider').length != 0){
			next();
			return false;
		}
	});


	function next(){
		killTweens();
		main.update();

		var int;
		var from, to;

		if(isMoving == false){

			if(current < 10){
				current++;

				from = main.x;
				to = main.x - w - 1;

				int = setInterval(function(){
					if(from == to || from <= to + 1){
						main.scrollProxy.left(to + 1);
						updateMain();
						check();
						clearInterval(int);
						isMoving = false;
					}
					else {
						from = from - ((from - to)/10);
						main.scrollProxy.left(from);
						updateMain();
						isMoving = true;
					}
				}, 16);
			}
		}
		else{
			return false;
		}
	}


	function prev() {
		killTweens();
		main.update();

		var int;
		var from, to;

		if(isMoving == false){

			if(current > 1){
				current--;

				from = main.x;
				to = main.x + w + 1;

				int = setInterval(function(){

					if(from == to || from >= to - 1){
						main.scrollProxy.left(to - 1);
						check();
						updateMain();
						clearInterval(int);
						isMoving = false;
					}
					else {
						from = from + ((to - from)/10);
						main.scrollProxy.left(from);
						updateMain();
						isMoving = true;
					}
				}, 16);
			}
		}
		else{
			return false;
		}
	}



	function selectActive(){
		select = $('.s-' + (current - 1));

		$('li.active-slider').css('color', 'rgb(200,200,200)');
		$('.active-slider').removeClass('active-slider');

		select.addClass('active-slider');

		active = current;

		var color = select.data('bg');
		var label = select.data('bg');
		if(color == undefined){
			color = '#ffffff';
		}

		if(label == undefined || label == '#EFF4FA'){
			label = '#33b5e5';
		}


		$('.slider-bg').css('background-color', color);
		$('li.active-slider').css('color', label);

	}


	$('.bottom-slider li').on('click', function(e){

		e.preventDefault();

		if(click == false){

			var clicked 	= $(this).data('slider');
			var toMove 		= current - clicked;

			var int;
			var from, to;

			from = main.x;

			click = true;

			if(toMove > 0){

				to 		= main.x + (toMove*w) + 1;
				current = clicked;

				to = main.x + (toMove*w) + 1;

				int = setInterval(function(){

					if(from == to || from >= to - 1){
						main.scrollProxy.left(to - 1);
						check();
						updateMain();
						clearInterval(int);
						isMoving = false;
						click = false;
					}
					else {
						from = from + ((to - from)/10);
						main.scrollProxy.left(from);
						updateMain();
						isMoving = true;
					}
				}, 16);
			}
			else if(toMove < 0){

				to 		= main.x + (toMove*w) + 1;
				current = clicked;

				int = setInterval(function(){
					if(from == to || from <= to + 1){
						main.scrollProxy.left(to + 1);
						updateMain();
						check();
						clearInterval(int);
						isMoving = false;
						click = false;
					}
					else {
						from = from - ((from - to)/10);
						main.scrollProxy.left(from);
						updateMain();
						isMoving = true;
					}
				}, 16);
			}
			return false;
		}
	});

}


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
