'use strict';

import _App from '../modules/app';
import _Utils from '../modules/utils';


const callback = function()
{
	if (_App.isJs('contact') === false){
		return;
	}
	else if (_App.isJs('contact') === true){
		initMap();
	}
};


var initMap = function()
{
	var mapOptions = {
		center: new google.maps.LatLng(46.489560,6.397894),
		zoom: 15,
		zoomControl: false,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
		},
		disableDoubleClickZoom: true,
		mapTypeControl: false,
		scaleControl: false,
		scrollwheel: false,
		panControl: false,
		streetViewControl: false,
		draggable : false,
		overviewMapControl: false,
		overviewMapControlOptions: {
			opened: false,
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles:
			[ { "featureType": "all", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [ { "color": "#444444" } ] }, { "featureType": "landscape", "elementType": "all", "stylers": [ { "color": "#f2f2f2" } ] }, { "featureType": "poi", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": 45 } ] }, { "featureType": "road.highway", "elementType": "all", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "color": "#46bcec" }, { "visibility": "on" } ] } ],
	}


	var mapElement = document.getElementById('map');
	var map = new google.maps.Map(mapElement, mapOptions);

	var icon = {
		url: 'public/images/icons/blue/marker.svg',
		scaledSize: new google.maps.Size(15, 22)
	}

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(46.489560,6.397894),
		icon: icon,
		map: map
	});

};


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
