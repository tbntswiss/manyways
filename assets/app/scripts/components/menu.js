'use strict';

import _App from '../modules/app';

const callback = function()
{
	setMenuActive();
	gotop();
	scroll();
	mobile();
};

var setMenuActive = function()
{
	$('[data-nav-link]').removeClass('is-active');
	$('[data-nav-link~="' + _App.getCurrentMenu() + '"]').addClass('is-active');
};


var gotop = function()
{
	$(".gotop").click(function() {
		$("html, body").animate({ scrollTop: 0 }, 1000);
		return false;
	});
};


var scroll = function()
{
	var scroll;
	var gotop = document.querySelector('.gotop');

	window.addEventListener('scroll', function(){
		scroll = document.body.scrollTop;

		if(scroll > window.innerHeight*0.7){
			gotop.classList.add('show');
		}
		else{
			gotop.classList.remove('show');
		}

	});
}


var mobile = function()
{
	$('.menu--toggle').click(function(){
		$('.navbar').toggleClass('mobile--open');
		$('html').toggleClass('noscroll');
		$('body').toggleClass('noscroll');
		$('#app').toggleClass('noscroll');
	});

	$('.navbar--item').click(function(){
		setTimeout(function() {
			$('.navbar').removeClass('mobile--open');
			$('html').removeClass('noscroll');
			$('body').removeClass('noscroll');
			$('#app').removeClass('noscroll');
		}, 200);
	});


	document.body.addEventListener('touchmove', function(e){
		if(document.querySelector('.mobile--open') != undefined || document.querySelector('.mobile--open') != null){
			e.preventDefault();
		}
	});

}




export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
