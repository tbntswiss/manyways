'use strict';

import _App from '../modules/app';

const callback = function()
{
	if (_App.isJs('case') === false){
		return;
	}
	else if (_App.isJs('case') === true){
		watch();
	}
};

var toDisplay, prev, next;

var watch = function()
{


	//open
	$('.modal-preview').on('click', function(e){

		toDisplay = $(this).find('img').attr('data-full');
		openModal(toDisplay);

		prevnext($(this));

		if ($(window).width() > 769) {
			followCursor();
		}


		$(window).on('resize', function () {
			if ($(window).width() <= 769) {
				stickCursor();
			}
			else if ($(window).width() > 769) {
				followCursor();
			}
		});
	});


	//close
	$('.modal').on('click', function(){
		closeModal();
	});

	//prev/next
	$('.modal .controls .prev').on('click', function(){
		previousImg(prev);
		return false;
	});

	$('.modal .controls .next').on('click', function(){
		nextImg(next);
		return false;
	});

	//prev/next with arrow keys
	$(window).on('keydown', function(e) {
		if(e.keyCode == 37 && $('.modal.open').length != 0){
			previousImg(prev);
			return false;
		}
		else if(e.keyCode == 39 && $('.modal.open').length != 0){
			nextImg(next);
			return false;
		}
	});
}

var openModal= function(url){
	var modal = $('.modal');
	modal.addClass('open');
	$('html, body').css('overflow', 'hidden');

	setTimeout(function() {
		var img = $('<img id="dynamic">');
		img.attr('src', url);
		img.appendTo(modal);
	}, 500);

	setTimeout(function() {
		$('#dynamic').addClass('show');
	}, 550);
}


var prevnext = function(el){

	//define prev and next
	prev = el.prev();
	if(el.index() == 0){
		prev = $('.modal-preview').last();
	}

	next = el.next();
	if(el.index() == $('.story--products .block-default .block').length - 1){
		next = $('.modal-preview').first();
	}
}

var closeModal= function(url){
	var modal = $('.modal');
	var img = $('#dynamic');

	img.removeClass('show');

	setTimeout(function() {
		modal.removeClass('open');
		$('html, body').css('overflow', '');
	}, 300);

	setTimeout(function() {
		img.remove();
	}, 1000);
}

var previousImg = function(el){

	var img = $('#dynamic');

	img.removeClass('show');

	setTimeout(function() {
		img.attr('src', el.find('img').attr('data-full'));
	}, 300);

	setTimeout(function() {
		img.addClass('show');
	}, 400);

	var current;

	setTimeout(function() {
		$('.story--products .block-default .block').each(function(i){
			if($(this).find('img').attr('data-full') == img.attr('src')){
				current = $(this);
				prevnext(current);
			}
		});
	}, 500);
}

var nextImg = function(el){

	var img 	= $('#dynamic');

	img.removeClass('show');

	setTimeout(function() {
		img.attr('src', el.find('img').attr('data-full'));
	}, 300);

	setTimeout(function() {
		img.addClass('show');
	}, 400);

	var current;

	setTimeout(function() {
		$('.story--products .block-default .block').each(function(i){
			if($(this).find('img').attr('data-full') == img.attr('src')){
				current = $(this);
				prevnext(current);
			}
		});
	}, 500);
}


var followCursor = function(){
	var cursor = document.querySelector('.closeModal');
	window.addEventListener('mousemove', function(e){
		var x = e.clientX;
		var y = e.clientY;

		cursor.style.top 	= (y - 16) + 'px';
		cursor.style.left = (x - 16) + 'px';
	});
}


var stickCursor = function(){
	var cursor = document.querySelector('.closeModal');

	cursor.style.top 	= '24px';
	cursor.style.left 	= '24px';
}


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
