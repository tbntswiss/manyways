'use strict';

import _App from '../modules/app';
import _Utils from '../modules/utils';


const callback = function()
{
	newsletterType();
};


var newsletterType = function()
{
	$('.footer--column__content form #mce-EMAIL').focusout(function(){
		$('.footer--column__content form label').removeClass('hide');
	});

	$('.footer--column__content form #mce-EMAIL').focus(function(){
		$('.footer--column__content form label').addClass('hide');
	});
};


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
