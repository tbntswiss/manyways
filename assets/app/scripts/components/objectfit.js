'use strict';

import _App from '../modules/app';

const callback = function()
{
	init();
};


var init = function()
{
	var isIE = /*@cc_on!@*/false || !!document.documentMode;

	if(isIE == true){
		if($('.objectfit').length != 0){

			//Do the trick here !
			$('.objectfit').each(function () {
				var $container = $(this).parent(),
					imgUrl = $container.find('img').prop('src');

				if (imgUrl) {
					$container
					.css('backgroundImage', 'url(' + imgUrl + ')')
					.addClass('compat-object-fit');
				}
			});

		}
	}
}

export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
