'use strict';

var body = document.querySelector('body');

var animEnd = function()
{
	if (body.className.indexOf('no-transition') !== -1)
		return;

	body.classList.remove('out-transition');
	body.classList.add('in-transition');
};

var animStart = function()
{
	// detect anchor for succes stories
	if (window.location.href.indexOf('#success-stories') !== -1)
		$('html, body').animate({ scrollTop: $('#success-stories').offset().top }, 1);

	if (body.className.indexOf('no-transition') !== -1)
		return;

	setTimeout(() => {
		body.classList.remove('in-transition');
		body.classList.add('out-transition');
	}, 600);
};

var disableAnim = function()
{
	body.classList.remove('out-transition');
	body.classList.remove('in-transition');
	body.classList.add('no-transition');

	setTimeout(function() {
		body.classList.remove('no-transition');
	}, 550);
};

export default {};

$(document).ready(animStart);

$(document).on('pjax:send', animEnd);
$(document).on('pjax:callback', animStart);

window.onpopstate = function(event)
{
	disableAnim();
};
