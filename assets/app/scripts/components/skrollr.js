'use strict';

import _App from '../modules/app';

var s = null;

const callback = function()
{
	if(s != null){
		s.refresh();
	}

	init();
};

var init = function()
{

	if ($(window).width() > 769) {
		setblock();
	}

	$(function (){

		// disable skrollr if the window is resized below 768px wide
		$(window).on('resize', function () {
			if ($(window).width() <= 769) {
				//s = skrollr.init().destroy(); // skrollr.init() returns the singleton created above
				if(s != null){
					s.destroy();
				}

			}
			else if ($(window).width() > 769) {
				setblock();
			}
		});

	});

};


var setblock = function() {

	var def 		= false;
	var inverted 	= false;



	if($('.skrollr-plugin-default').length != 0){

		$.each($('.skrollr-plugin-default'), function(index){

			var childs 	= $(this).children();
			var start 	= 900;
			var end 	= -600;

			$.each(childs, function(i){

				if(i % 2 != 0){

					var h_data = $(this).outerHeight();

					var data_start 	= 'data-' + start.toString() + '-top';
					var start_tr 	= 'transform: translate(-1px, -40px);';

					var data_end 	= 'data-' + end.toString() + '-top';
					var end_tr 		= 'transform: translate(-1px, 40px);';


					$(this).attr(data_start, start_tr);
					$(this).attr(data_end, end_tr);
					$(this).attr('data-smooth-scrolling', 'off');


					//once the datas are pushed in the DOM, update the start / end vars.
					start 	= start + h_data + 1;
					end 	= end + h_data + 1;
				}
			});
		});
		def = true;
		checkInit(def, inverted);
	}
	else{
		def = true;
		checkInit(def, inverted);
	}



	if($('.skrollr-plugin-inverted').length != 0){

		$.each($('.skrollr-plugin-inverted'), function(index){

			var childs 	= $(this).children();
			var start 	= 900;
			var end 	= -600;

			$.each(childs, function(i){

				if(i % 2 != 0){

					var h_data = $(this).outerHeight();

					var data_start 	= 'data-' + start.toString() + '-top';
					var start_tr 	= 'transform: translate(-0px, -120px);';

					var data_end 	= 'data-' + end.toString() + '-top';
					var end_tr 		= 'transform: translate(-0px, -40px);';


					$(this).attr(data_start, start_tr);
					$(this).attr(data_end, end_tr);
					$(this).attr('data-smooth-scrolling', 'off');


					//once the datas are pushed in the DOM, update the start / end vars.
					start 	= start + h_data + 1;
					end 	= end + h_data + 1;
				}
			});

			if($('.skrollr-plugin').length == index+1){
				s = skrollr.init();
			}
		});
		inverted = true;
		checkInit(def, inverted);
	}
	else{
		inverted = true;
		checkInit(def, inverted);
	}
}


var checkInit = function(def, inv){

	if(def == true && inv == true){
		s = skrollr.init();
	}
}



export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
