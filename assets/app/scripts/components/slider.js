'use strict';

import _App from '../modules/app';
import _Utils from '../modules/utils';


const callback = function()
{
	if (_App.isJs('case') === false){
		return;
	}
	else if (_App.isJs('case') === true){
		init();
	}
};


var init = function()
{

	var w = document.querySelector('.step-1').offsetWidth;

	$(window).on('resize', function(){

		if ($(window).width() <= 769) {
			dragleft.disable();
			dragright.disable();
		}
		else if ($(window).width() > 769) {
			dragleft.enable();
			dragright.enable();
		}

		if(document.querySelector('.step-1') != null){
			w = document.querySelector('.step-1').offsetWidth;
			return w;
		}
	});


	var isMoving = false;

	var current = 1;

	var dragleft = Draggable.create(".block-slider.left", {
		type:"scrollLeft",
		edgeResistance:0.85,
		throwProps:true,
		minDuration: .2,
		maxDuration: .6,
		onPress: addGrab,
		onDragStart: killTweens,
		onDrag: updateRight,
		onThrowUpdate: updateRight,
		onDragEnd: check,
		onRelease: removeGrab,
		snap:{
			x: function(endValue){
				var stick = Math.round((endValue*(-1)) / w) * w;
				current = Math.round((endValue*(-1)) / w) + 1;
				return stick;
			}
		}
	})[0];

	var dragright = Draggable.create(".block-slider.right", {
		type:"scrollLeft",
		edgeResistance:0.85,
		throwProps:true,
		minDuration: .2,
		maxDuration: .6,
		onPress: addGrab,
		onDragStart: killTweens,
		onDrag: updateLeft,
		onThrowUpdate: updateLeft,
		onDragEnd: check,
		onRelease: removeGrab,
		snap:{
			x: function(endValue){
				var stick = Math.round((endValue*(-1)) / w) * w;
				current = Math.round((endValue*(-1)) / w) + 1;
				return stick;
			}
		}
	})[0];


	if ($(window).width() <= 769) {
		dragleft.disable();
		dragright.disable();
	}


	function addGrab(){
		$('.block-slider').addClass('grabbing');
	}

	function removeGrab(){
		$('.block-slider').removeClass('grabbing');
	}

	function killTweens() {
		TweenLite.killTweensOf([dragleft, dragright.scrollProxy]);
		TweenLite.killTweensOf([dragright, dragleft.scrollProxy]);
	}

	function updateRight() {
		dragright.scrollProxy.left(dragleft.x);
	}

	function updateLeft() {
		dragleft.scrollProxy.left(dragright.x);
	}


	function check(){

		dragright.update();
		dragleft.update();

		updateBottom();

		if(current <= 1){
			$('.story--process .controls .prev').addClass('unselect');
		}
		else{
			$('.story--process .controls .prev').removeClass('unselect');
		}


		if(current >= 5){
			$('.story--process .controls .next').addClass('unselect');
		}
		else{
			$('.story--process .controls .next').removeClass('unselect');
		}
	}


	function updateBottom(){

		var update = current*20;
		if(update < 20){
			update = 20;
		}
		else if(update > 100){
			update = 100;
		}
		$('.bar .current').css('width', update+'%');
		$('.numbers .current').html(update/20);
	}


	//prev/next
	$('.story--process .controls .prev').on('click', function(){
		killTweens();
		dragright.update();
		dragleft.update();

		var int;
		var from, to;

		if(isMoving == false){

			if(current > 1){
				current--;

				updateBottom();

				from = dragright.x;
				to = dragright.x + w + 1;

				int = setInterval(function(){

					if(from == to || from >= to - 1){
						dragright.scrollProxy.left(to - 1);
						dragleft.scrollProxy.left(to - 1);
						check();
						clearInterval(int);
						isMoving = false;
					}
					else {
						from = from + ((to - from)/10);
						dragright.scrollProxy.left(from);
						dragleft.scrollProxy.left(from);
						isMoving = true;
					}

				}, 16);
			}

		}
		else{
			return false;
		}
	});

	$('.story--process .controls .next').on('click', function(){
		killTweens();
		dragright.update();
		dragleft.update();

		var int;
		var from, to;

		if(isMoving == false){

			if(current < 5){
				current++;

				updateBottom();

				from = dragright.x;
				to = dragright.x - w - 1;

				int = setInterval(function(){
					if(from == to || from <= to + 1){
						dragright.scrollProxy.left(to + 1);
						dragleft.scrollProxy.left(to + 1);
						check();
						clearInterval(int);
						isMoving = false;
					}
					else {
						from = from - ((from - to)/10);
						dragright.scrollProxy.left(from);
						dragleft.scrollProxy.left(from);
						isMoving = true;
					}

				}, 16);
			}

		}
		else{
			return false;
		}
	});




}


export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
