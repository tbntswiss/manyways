'use strict';






$(document).ready(function($)
{


	console.log('%c Made with love by TBNT - Lausanne, Switzerland', 'background: #202542; color: #EF604C;');
	console.log('%c Visit website http://tbnt.digital/', 'background: #202542; color: #EF604C;');

	var wow = new WOW({
		boxClass: 		'wow',
		animateClass: 	'animated',
		offset: 		0,
		mobile: 		false,
		live: 			true
	});
	wow.init();


	//detect home or not
	if(window.location.href.indexOf('home') !== -1){
		$('li.s-1').trigger('click');
	}

	if(window.location.href.indexOf('en') !== 1 && window.location.href.indexOf('fr') !== 1 && window.location.href.indexOf('de') !== 1){
		$('li.s-1').trigger('click');
	}


	//pre-loader
	setTimeout(function() {
		$('.loader').addClass('loaded');
	}, 4500);

	setTimeout(function() {
		$('html').removeClass('noscroll');
	}, 4800);

	setTimeout(function() {
		$('.loader').addClass('hidden');
	}, 5500);

});

export default {};
