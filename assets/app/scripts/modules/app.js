'use strict';

var getCurrentMenu = function()
{
	return Laravel.menu;
};

var getLangId = function()
{
	return Laravel.lang.id;
};

var getLangCode = function()
{
	return Laravel.lang.code;
};

var isJs = function(page)
{
	return Laravel.js.indexOf(page) > -1;
};

export default
{
	getCurrentMenu,
	getLangId,
	getLangCode,
	isJs,
};
