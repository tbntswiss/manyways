'use strict';

import _App from './app';

var el = {};

var callback = function()
{
	init();
};

var init = function()
{
	initDOM();
	initEvents();
};

var initDOM = function()
{
	el = {};
	el.cookies_box = $('[data-cookies-box]');
};

var initEvents = function()
{
	$(document)
		.on('click', '[data-cookies-box] [data-cookies-button]', function() { hideCookies(); });
};

var hideCookies = function()
{
	el.cookies_box.addClass('is-hidden');

	// Get values
	var values = {};
		values.lang_id = _App.getLangId();
		values.is_hidden = 1;

	// Toggle tuto
	$.ajax({
		url: 'ajax/cookies-accepted',
		data: values,
		type: 'POST',
		timeout: 60 * 1000,
		cache: false,
		error: function(error)
		{
			console.error('An error occured while hiding cookies.');
		},
	});
};

export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
