'use strict';

import _Utils from './utils';

let fields = [];

const clearErrors = () =>
{
	$('.uk-card-error-message').remove();

	for (let i = 0, c = fields.length; i < c; i++) {
		fields[i].$card.removeClass('uk-card-form-danger');

		if (fields[i].options.has_margin_bottom === false)
			fields[i].$card.removeClass('uk-margin-medium-bottom');

		if (fields[i].options.has_position_relative === false)
			fields[i].$card.removeClass('uk-position-relative');
	}

	fields = [];
};

const addErrors = (error, format = {}) =>
{
	clearErrors();

	_.map(error.responseJSON || {}, (messages, name) => {
		name = format[name] || name;

		let fieldName = name;
		let fieldOffset = 0;

		if (name.indexOf('.') !== -1) {
			let names = name.split('.');
				names[0] = format[names[0]] || names[0];

			fieldName = names.shift();

			for (let i = 0, c = names.length; i < c; i++) {
				const tempName = names[i];

				if (isNaN(tempName) === true) {
					fieldName += '[' + tempName + ']';
				}
				else {
					fieldName += '[]';
					fieldOffset += Number(tempName);
				}
			}
		}

		const $card = $('[name="' + fieldName + '"]').eq(fieldOffset).parents('.uk-card-form');
		const options = {
			has_margin_bottom: $card.hasClass('uk-margin-medium-bottom'),
			has_position_relative: $card.hasClass('uk-position-relative'),
		};

		$card.addClass('uk-card-form-danger uk-position-relative uk-margin-medium-bottom')
			.append('<p class="uk-card-error-message uk-position-bottom uk-text-danger">' + messages.join('<br>') + '</p>');

		fields.push({ $card, options });
	});

	UIkit.notification({ message: 'The form contains some errors.', status: 'danger' });
};

export default
{
	clearErrors,
	addErrors,
};
