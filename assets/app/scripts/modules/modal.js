'use strict';

import _Utils from './utils';

var show = function(modalId)
{
	if (_Utils.isJquery(modalId) === true)
		modalId = '[data-modal="' + modalId.attr('data-modal') + '"]';

	UIkit.modal(modalId)[0].toggle();
};

export default
{
	show,
};
