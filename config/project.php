<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Domain
    |--------------------------------------------------------------------------
    |
    | This value is the main domain of the project.
    |
    | Admin section can only be accessible if the current domain is matching
    | this main domain.
    |
    */

    'domain' => 'localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Email
    |--------------------------------------------------------------------------
    |
    | This value is the main email of the project.
    |
    */

    'email' => 'info@many-ways.ch',

    /*
    |--------------------------------------------------------------------------
    | Reset Password Duration
    |--------------------------------------------------------------------------
    |
    | This value is the time of the validity for the password recovery.
    |
    */

    'reset_password_duration' => 12,

];
