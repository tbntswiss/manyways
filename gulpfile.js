var elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');

// var BrowserSync = require('laravel-elixir-browsersync2');
// var browser = browser === undefined ? 'google chrome' : browser;

elixir.config.js.folder = '../../';
elixir.config.js.outputFolder = '../';

elixir.config.css.folder = '../../';
elixir.config.css.outputFolder = '../';

elixir.config.css.less.folder = '../../';
elixir.config.css.sass.folder = '../../';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)
{
	/*
	 |--------------------------------------------------------------------------
	 | App Asset Management
	 |--------------------------------------------------------------------------
	 |
	 */

	// Styles
	mix.sass(
		'assets/app/styles/app.sass',
		'assets/build/app/styles/vendor.css'
	);

	mix.styles(
		[
			//'assets/vendor/uikit/css/uikit.min.css',
			'assets/build/app/styles/vendor.css',
		],
		'public/app/styles/vendor.css'
	);

	// Scripts
	mix.webpack(
		[
			'assets/app/scripts/components/**/*.js',
			'assets/app/scripts/modules/**/*.js',
			'assets/app/scripts/main.js',
		],
		'assets/build/app/scripts/custom.js'
	);

	mix.scripts(
		[
			'node_modules/jquery/dist/jquery.js',
			'node_modules/lodash/lodash.min.js',
			'node_modules/gsap/src/uncompressed/TweenMax.js',
			'node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
			//'assets/vendor/uikit/js/uikit.min.js',
			'assets/app/scripts/functions.js',
			'assets/build/app/scripts/custom.js',
			'assets/vendor/skrollr.min.js',
			'assets/vendor/wow.min.js',
			//'assets/vendor/jquery.nPageLoader.js',
			'assets/vendor/gsap/Draggable.js',
			'assets/vendor/gsap/ThrowPropsPlugin.js',
			'assets/build/app/scripts/custom.js'

		],
		'public/app/scripts/vendor.js'
	);



	/*
	 |--------------------------------------------------------------------------
	 | Admin Asset Management
	 |--------------------------------------------------------------------------
	 |
	 */

	// Styles
	mix.sass(
		'assets/admin/styles/app.sass',
		'assets/build/admin/styles/custom.css'
	);

	mix.styles(
		[
			'assets/vendor/uikit/css/uikit.min.css',
			'assets/build/admin/styles/custom.css',
		],
		'public/admin/styles/vendor.css'
	);

	// Scripts
	mix.webpack(
		[
			'assets/admin/scripts/components/**/*.js',
			'assets/admin/scripts/modules/**/*.js',
			'assets/admin/scripts/main.js',
		],
		'assets/build/admin/scripts/custom.js'
	);

	mix.scripts(
		[
			'node_modules/jquery/dist/jquery.js',
			'node_modules/lodash/lodash.min.js',
			'node_modules/gsap/src/uncompressed/TweenMax.js',
			'node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
			'assets/vendor/uikit/js/uikit.min.js',
			'assets/admin/scripts/functions.js',
			'assets/build/admin/scripts/custom.js'
		],
		'public/admin/scripts/vendor.js'
	);

});

// elixir(function(mix) {
// 	BrowserSync.init();
// 	mix.BrowserSync(
// 	{
// 		proxy: 'localhost:8888/manyways',
// 		notify: true
// 	});
// });
