<?php

return [

	/*
	|--------------------------------------------------------------------------
	| APP DEFAULTS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Textilien und personalisierte Artikel | '.appName(),
	'meta_desc' 			=> 'Unser Team hat seit über 20 Jahren Erfahrung in der Kreation und Produktion von Textilien und personalisierten Produkten. Wir erstreben ein professionelles Auftreten Ihrer Firma.',

];
