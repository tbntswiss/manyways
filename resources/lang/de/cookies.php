<?php

return [

	/*
	|--------------------------------------------------------------------------
	| COOKIES
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'info' 					=> 'Afin de vous proposer le meilleur service possible, ce site utilise des cookies. En continuant de naviguer sur le site, vous déclarez accepter leur utilisation.',
	'button_agree' 			=> 'J\'accepte',

];
