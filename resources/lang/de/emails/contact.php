<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'subject' 				=> appName().' | Contact',

	'title' 				=> 'You just received a new email via many-ways.ch.',
	'copyright' 			=> 'Many Ways '.date('Y'),

];
