<?php

return [

	/*
	|--------------------------------------------------------------------------
	| NAVIGATION
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'home' 					=> 'Home',
	'stories' 				=> 'Erfolge & Produkte',
	'products' 				=> 'Neuigkeiten',
	'contact' 				=> 'Kontaktieren Sie uns',

];
