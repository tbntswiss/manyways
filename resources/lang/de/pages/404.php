<?php

return [

	'meta_title' 			=> '404 | Many Ways SA',
	'meta_desc' 			=> "Sie haben verloren... Diese Seite hat ein Problem oder nicht mehr vorhanden ist.",

	'data_hero' 			=> 'true',

	/*HERO INFOS*/
	'hero_infos' 			=> [

		'fo_url' 		=> 'public/images/header/404/obj-front.png',
		'fo_width'		=> '200',
		'fo_x'			=> '10',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/404/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '10',
		'fs_y'			=> '10',

		'bo_url' 		=> 'public/images/header/404/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/404/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	/*CONFIRMATION PAGE*/
	'confirm_title' 		=> 'Whoops !',
	'confirm_txt' 			=> "Sie haben verloren... <br> Diese Seite hat ein Problem oder nicht mehr vorhanden ist.",
	'confirm_story' 		=> 'Letzte Erfolgsgeschichte',

];
