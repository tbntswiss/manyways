<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Kontakt | Many Ways SA',
	'meta_desc' 			=> 'Hier finden Sie alle nötigen Informationen um mit uns Kontakt aufzunehmen. Zögern Sie nicht und teilen Sie ihr Projekt mit uns!',


	/*Slider*/
	'title' 				=> 'Kontaktieren Sie uns',
	'data_hero' 			=> 'true',


	/*HERO INFOS*/
	'hero_infos' 			=> [

		'fo_url' 		=> 'public/images/header/contact/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '15',
		'fo_y'			=> '65',

		'fs_url' 		=> 'public/images/header/contact/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/contact/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '20',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/contact/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],

	/*Infos*/
	'contact_about' 		=> 'Eine Frage? Ein Projekt das Sie mit uns teilen möchten? Kontaktieren Sie uns indem Sie das nachstehende Formular ausfüllen.',

	/*Form*/
	'surname'				=> [
		'reference' 	=> 'surname',
		'txt'			=> 'Name',
		'on_error' 		=> 'Bitte füllen Sie dieses Feld.',
		'required' 		=> 'required',
	],

	'firstname'				=> [
		'reference' => 'firstname',
		'txt'		=> 'Vorname',
		'on_error' 	=> 'Bitte füllen Sie dieses Feld.',
		'required' 	=> 'required',
	],

	'email'					=> [
		'reference' => 'email',
		'txt'		=> 'E-mail',
		'on_error' 	=> 'Bitte füllen Sie dieses Feld.',
		'required' 	=> 'required',
	],

	'tel'					=> [
		'reference' => 'tel',
		'txt'		=> 'Telefonnummer',
		'on_error' 	=> 'Bitte füllen Sie dieses Feld.',
		'required' 	=> '',
	],

	'message'				=> [
		'reference' => 'message',
		'txt'		=> 'Nachricht',
		'on_error' 	=> 'Bitte füllen Sie dieses Feld.',
		'required' 	=> 'required',
	],


	'submit' 				=> 'Senden',



	/*Contact infos*/
	'agency_name' 			=> 'Many Ways SA',
	'phone' 				=> '+41 21 320 35 35',
	'mail' 					=> 'info@many-ways.ch',
	'street' 				=> 'Avenue Longemalle 21B<br />1020 Renens (Suisse)',


	/*CONFIRMATION PAGE*/
	'c_meta_title' 			=> 'Danke | Many Ways SA',
	'c_meta_desc' 			=> "Merci ! Nous prendrons contact avec vous dans les plus brefs délais.",


	'confirm_title' 		=> 'Danke !',
	'confirm_txt' 			=> 'Vielen Dank! Wir werden Sie so schnell wie möglich kontaktieren.',
	'confirm_story' 		=> 'Letzte Erfolgsgeschichte',


	'data_hero_confirm' 	=> 'true',


	/*HERO INFOS*/
	'hero_infos_confirm' 	=> [

		'fo_url' 		=> 'public/images/header/form_confirm/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '0',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/form_confirm/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/form_confirm/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/form_confirm/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


];
