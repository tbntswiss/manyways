<?php

return [

	/*
	|--------------------------------------------------------------------------
	| LOGIN
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Login | Paleo',
	'meta_desc' 			=> 'Login | Paleo | Description',

	'title' 				=> 'Welcome back!',

	'placeh_email' 			=> 'Enter email',
	'placeh_password' 		=> 'Enter password',

	'btn_send' 				=> 'Log in',
	'btn_forgotten' 		=> 'Forgotten password ?',

];
