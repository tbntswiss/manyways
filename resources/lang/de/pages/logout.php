<?php

return [

	/*
	|--------------------------------------------------------------------------
	| LOGOUT
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Logout | Paleo',
	'meta_desc' 			=> 'Logout | Paleo | Description',

	'logout_success' 		=> 'You are now logged out',
	'logout_error' 			=> 'An error has occured, you\'re still logged in u__u',

];
