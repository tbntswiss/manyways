<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Erfolgsgeschichte - Madrid Open | Many Ways SA',
	'meta_desc' 			=> 'Das Mutua Madrid Open, Masters 1000,  ist ein ATP/WTA Tennisturnier.',


	/*HEADER*/
	'img' 					=> 'public/images/cases/madrid/other/header.png',


	'compo_1' 				=> 'public/images/cases/madrid/other/compo1.png',
	'compo_2' 				=> 'public/images/cases/madrid/other/compo2.png',


	/*INFOS*/
	'client_label' 			=> 'Kunde',
	'client' 				=> 'Mutua Madrid Open',
	'descr_label' 			=> 'Beschreibung',
	'descr' 				=> 'Das Mutua Madrid Open, Masters 1000,  ist ein ATP/WTA Tennisturnier.',
	'date_label' 			=> 'Datum',
	'date' 					=> 'Juin 2016',


	/*Process*/
	'1_title' 				=> 'Erster Schritt',
	'1_txt' 				=> 'Brainstorming mit dem Kunden rundum das Tennis-Universum. Des Kunden Logo und deren Farben erweisen sich für das Tennisturnier als jung und modern und so wünscht sich der Kunde, dass das Logo respektiert wird.',
	'2_title' 				=> 'Entwicklung eines persönlichen Designs',
	'2_txt' 				=> 'Originelle T-Shirts und Mützen im „Image“ der Marke. Um den Anforderungen des Kunden gerecht zu werden und so nahe wie möglich an den bereits etablierten Grafiken zu bleiben, fokussieren wir uns eher auf Farbvariationen als auf neue Designs.',
	'3_title' 				=> 'Auswahl der Gegenstände, Produkte, Textilien usw.',
	'3_txt' 				=> 'Auswahl der Textilien basierend auf Sport, Jugendliche und Kinder. Auswahl der Materialien in Baumwolle und Dry-Fit.',
	'4_title' 				=> 'Wahl der Produktionsstätte',
	'4_txt' 				=> 'Zipper-Hoodies made in China. Druck der Textilien in Spanien, dies für eine grössere Flexibilität bei eventuellen Nachbestellungen während der Veranstaltung.',
	'5_title' 				=> 'Lieferung',
	'5_txt' 				=> 'Lieferung 2 Wochen vor der Veranstaltung zur Einrichtung der Shops.',

	'products_title' 		=> 'Produkte',
	'has_products' 			=> 'true',
	'products' 				=> [
		'1' 		=>'public/images/cases/madrid/articles/1.jpg',
		'2' 		=>'public/images/cases/madrid/articles/2.jpg',
		'3' 		=>'public/images/cases/madrid/articles/3.png',
		'4' 		=>'public/images/cases/madrid/articles/4.png',
		'5' 		=>'public/images/cases/madrid/articles/5.png',
		'6' 		=>'public/images/cases/madrid/articles/6.png',
		'7' 		=>'public/images/cases/madrid/articles/7.png',
		'8' 		=>'public/images/cases/madrid/articles/8.jpg',
	],
	'thumbs' 				=> [
		'1' 		=>'public/images/cases/madrid/thumbs/1.jpg',
		'2' 		=>'public/images/cases/madrid/thumbs/2.jpg',
		'3' 		=>'public/images/cases/madrid/thumbs/3.png',
		'4' 		=>'public/images/cases/madrid/thumbs/4.png',
		'5' 		=>'public/images/cases/madrid/thumbs/5.png',
		'6' 		=>'public/images/cases/madrid/thumbs/6.png',
		'7' 		=>'public/images/cases/madrid/thumbs/7.png',
		'8' 		=>'public/images/cases/madrid/thumbs/8.jpg',
	],
	'no_products' 			=> 'Unsere Produkte werden in Kürze verfügbar.',

	'cta-contact_1' 	=>'Sie haben ein Projekt? Zögern Sie nicht',
	'cta-contact_2' 	=>'kontaktieren Sie uns',

	'cta-contact_3a' 	=>'Sind Sie interessiert ?',
	'cta-contact_3b' 	=>'Kontaktieren Sie uns',
	'cta-contact_3c' 	=>'doch für einen individuellen Kostenvoranschlag.',

	/*Next*/
	'next_title' 		=> 'PKF Center',
	'next_label' 		=> 'Nächste Geschichte',
	'next_img' 			=> 'public/images/cases/pkf/other/header.jpg',
	'next_url' 			=> 'de/pkf-center',

];
