<?php

return [

	/*
	|--------------------------------------------------------------------------
	| URLS
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'home' 					=> 'de',

	'products' 				=> 'de/social-media',
	'success_stories' 		=> 'de/success-stories',

	'contact' 				=> 'de/kontakt',
	'confirm' 				=> 'de/danke',

	'success_stories_id' 	=> '[0-9a-z\-]+',

	// -- A MODIFIER/SUPPRIMER -> @foreach ($app_langs)

	'en'					=> 'en',
	'fr'					=> 'fr',
	'de'					=> 'de',

	// -- A SUPPRIMER

	'madrid' 				=> 'de/madrid-open',
	'pkf' 					=> 'de/pkf-center',
	'nendaz' 				=> 'de/gaby-sport-nendaz',
	'fer' 					=> 'de/commerce-de-fer',
];
