<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Custom
    |
    */

    'blog_url'             => 'The :attribute is already used by an another post and must be unique.',
    'product_url'          => 'The :attribute is already used by an another product and must be unique.',
    'category_url'         => 'The :attribute is already used by an another category and must be unique.',
    'is_utf8_malformed'    => 'The :attribute has invalid characters, please double check.',
    'csrf_error'           => 'Opps! Seems you didn\'t refresh this page for a longtime (or it came from your browser cache). Please refresh this page and try again.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'lang' => [
            'accepted' => 'At least one language should be specified.'
        ],
        'image' => [
            'accepted' => 'The image file is required.',
        ],
        'pdf' => [
            'accepted' => 'The PDF file is required.',
        ],
        'image.*' => [
            'file' => 'The image must be a file.',
            'dimensions' => 'The image must have a max width/height of 2400px.',
        ],
        'pdf.*' => [
            'file' => 'The pdf must be a file.',
        ],
        'keys' => [
            'required' => 'At least one item should be in cart.',
        ],
        'keys.*' => [
            'required' => 'At least one item should be in cart.',
        ],
        'sell_id' => [
            'required_without' => 'An item is required.',
        ],
        'rent_id' => [
            'required_without' => 'An item is required.',
        ],
        'captcha_response' => [
            'required' => 'The captcha is required.',
            'string' => 'The captcha is invalid.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'agents_ids' => 'agents',
        'category_id' => 'category',
        'categories_ids' => 'categories',
        'content' => 'content',
        'description' => 'description',
        'email' => 'email',
        'items.rents' => 'rents',
        'items.rents.*' => 'rent item',
        'items.sells' => 'sells',
        'items.sells.*' => 'sell item',
        'label' => 'label',
        'lang_id' => 'lang',
        'legend_rent' => 'rent legend',
        'legend_sell' => 'sell legend',
        'message' => 'message',
        'meta_description' => 'meta description',
        'name' => 'name',
        'phone' => 'phone',
        'product_id' => 'product',
        'reference' => 'reference',
        'rent_id' => 'rent item',
        'sell_id' => 'sell item',
        'subject' => 'subject',
        'title' => 'title',
        'url' => 'url',

        'fr.content' => 'french content',
        'fr.description' => 'french description',
        'fr.label' => 'french label',
        'fr.legend_rent' => 'french rent legend',
        'fr.legend_sell' => 'french sell legend',
        'fr.meta_description' => 'french meta description',
        'fr.reference' => 'french reference',
        'fr.title' => 'french title',
        'fr.url' => 'french url',
        'fr.link' => 'french link',

        'fr.rent$labels' => 'french rent labels',
        'fr.rent$labels.*' => 'french rent labels',
        'fr.rent$items' => 'french rent items',
        'fr.rent$items.*' => 'french rent items',
        'fr.rent$items.*.*' => 'french rent items',

        'fr.sell$labels' => 'french sell labels',
        'fr.sell$labels.*' => 'french sell labels',
        'fr.sell$items' => 'french sell items',
        'fr.sell$items.*' => 'french sell items',
        'fr.sell$items.*.*' => 'french sell items',

        'de.content' => 'deutsch content',
        'de.description' => 'deutsch description',
        'de.label' => 'deutsch label',
        'de.legend_rent' => 'deutsch rent legend',
        'de.legend_sell' => 'deutsch sell legend',
        'de.meta_description' => 'deutsch meta description',
        'de.reference' => 'deutsch reference',
        'de.title' => 'deutsch title',
        'de.url' => 'deutsch url',
        'de.link' => 'deutsch link',

        'de.rent$labels' => 'deutsch rent labels',
        'de.rent$labels.*' => 'deutsch rent labels',
        'de.rent$items' => 'deutsch rent items',
        'de.rent$items.*' => 'deutsch rent items',
        'de.rent$items.*.*' => 'deutsch rent items',

        'de.sell$labels' => 'deutsch sell labels',
        'de.sell$labels.*' => 'deutsch sell labels',
        'de.sell$items' => 'deutsch sell items',
        'de.sell$items.*' => 'deutsch sell items',
        'de.sell$items.*.*' => 'deutsch sell items',

        'map.agents_ids' => 'area agents',
        'map.agents_ids.*' => 'area agents',
        'map.color' => 'area color',
        'map.pos_agent_id' => 'area pos',
        'map.reference' => 'area reference',
        'map.zones_ids' => 'area zones',
        'map.zones_ids.*' => 'area zones',
    ],

];
