<?php

return [

	/*
	|--------------------------------------------------------------------------
	| APP DEFAULTS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Textiles and personalized items | '.appName(),
	'meta_desc' 			=> 'We specialize in the creation and production of textiles and personalized items that enhance your brand.',

];
