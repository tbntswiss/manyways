<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Contact | Many Ways SA',
	'meta_desc' 			=> 'Here is all of the information you need to contact us. Do not hesitate to let us know about your project!',


	/*Slider*/
	'title' 				=> 'Contact Us',
	'data_hero' 			=> 'true',


	/*HERO INFOS*/
	'hero_infos' 			=> [

		'fo_url' 		=> 'public/images/header/contact/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '5',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/contact/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/contact/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/contact/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],

	/*Infos*/
	'contact_about' 		=> 'Do you have a question? Or a project that you would like us to be part of? Contact us by completing the form below!',

	/*Form*/
	'surname'				=> [
		'reference' 	=> 'surname',
		'txt'			=> 'Surname',
		'on_error' 		=> 'Please fill this field',
		'required' 		=> 'required',
	],

	'firstname'				=> [
		'reference' => 'firstname',
		'txt'		=> 'First Name',
		'on_error' 	=> 'Please fill this field',
		'required' 	=> 'required',
	],

	'email'					=> [
		'reference' => 'email',
		'txt'		=> 'E-mail',
		'on_error' 	=> 'Please fill this field',
		'required' 	=> 'required',
	],

	'tel'					=> [
		'reference' => 'tel',
		'txt'		=> 'Phone Number',
		'on_error' 	=> 'Please fill this field',
		'required' 	=> '',
	],

	'message'				=> [
		'reference' => 'message',
		'txt'		=> 'Message',
		'on_error' 	=> 'Please add your message here.',
		'required' 	=> 'required',
	],


	'submit' 				=> 'Send',



	/*Contact infos*/
	'agency_name' 			=> 'Many Ways SA',
	'phone' 				=> '+41 21 320 35 35',
	'mail' 					=> 'info@many-ways.ch',
	'street' 				=> 'Avenue Longemalle 21B<br />1020 Renens (Suisse)',


	/*CONFIRMATION PAGE*/
	'c_meta_title' 			=> 'Thank you | Many Ways SA',
	'c_meta_desc' 			=> "Thank you for your message! We will contact you as soon as possible.",


	'confirm_title' 		=> 'Thank you !',
	'confirm_txt' 			=> 'Your message has been sent successfully. We will get back to you as soon as possible.',
	'confirm_story' 		=> 'Read our last Success Stories',


	'data_hero_confirm' 	=> 'true',


	/*HERO INFOS*/
	'hero_infos_confirm' 	=> [

		'fo_url' 		=> 'public/images/header/form_confirm/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '5',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/form_confirm/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/form_confirm/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/form_confirm/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


];
