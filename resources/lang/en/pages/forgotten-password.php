<?php

return [

	/*
	|--------------------------------------------------------------------------
	| LOGIN
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Forgotten password | Paleo',
	'meta_desc' 			=> 'Forgotten password | Paleo | Description',

	'title' 				=> 'Forgotten password',

	'placeh_email' 			=> 'Enter email',

	'btn_send' 				=> 'Send recovery link',
	'btn_login' 			=> 'Login',

	'success' 				=> 'Your password recovery link has been sent to <span class="uk-text-bold">:email</span>.',

];
