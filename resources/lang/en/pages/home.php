<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Textiles and personalized items | Many Ways SA',
	'meta_desc' 			=> 'We specialize in the creation and production of textiles and personalized items that enhance your brand.',

	'loader' 				=> 'Loading in progress',

	/*Slider*/
	'slider-tagline' 		=> 'We produce textiles and personalized items that make your brand truly unforgettable.',
	'slider_nav' 			=> 'Click and hold to navigate',

	/*About*/
	'about' 				=> 'For over 20 years we have specialized in the design and production of personalised textiles and objects. Our services will put your brand in the spotlight.',

	/*Process*/
	'process_title'			=> 'Our services are both simple and efficient.',

	'process_step_1' 		=> 'We take the time to listen and advise you on your choices.',
	'process_step_2' 		=> 'We develop a design adapted to your needs and those of your customers.',
	'process_step_3' 		=> 'We work with you to choose the products best suited to your clientele.',
	'process_step_4' 		=> 'We produce your product in the right place according to costs, deadlines and quantities.',
	'process_step_5' 		=> 'And finally we deliver your product that fits your image.',


	/*Stories*/
	'stories_title' 		=> 'Success Stories',

	/* !!! Preview - should be in admin !!! */
	'story_preview' 		=> 'public/images/cases/fer/other/preview.png',
	'story_title' 			=> 'Cases Name',

	'madrid_preview' 		=> 'public/images/cases/madrid/other/preview.png',
	'madrid_title' 			=> 'Madrid Open',

	'pkf_preview' 			=> 'public/images/cases/pkf/other/preview.jpg',
	'pkf_title' 			=> 'PKF Center',

	'nendaz_preview' 		=> 'public/images/cases/nendaz/other/preview.png',
	'nendaz_title' 			=> 'Gaby Sport Nendaz',

	'fer_preview' 			=> 'public/images/cases/fer/other/preview.png',
	'fer_title' 			=> 'Commerce de Fer',



	'story_more' 			=> 'See more',
	'story_more_product' 	=> 'More products',


	/*Clients*/
	'references_title' 		=> 'Our references',
	'references_sub' 		=> 'We are endorsed by renowned customers who trust us for the quality of our products and our suppliers.',


	'references_logo_placeholder' 	=> 'public/images/references/air14.svg',

	'references_1' 			=> 'public/images/logo/clients/air14.svg',
	'references_2' 			=> 'public/images/logo/clients/alinghi.svg',
	'references_3' 			=> 'public/images/logo/clients/clubmed.png',
	'references_4' 			=> 'public/images/logo/clients/epfl.svg',
	'references_5' 			=> 'public/images/logo/clients/fai.svg',
	'references_6' 			=> 'public/images/logo/clients/gruyeres.svg',
	'references_7' 			=> 'public/images/logo/clients/imd.png',
	'references_8' 			=> 'public/images/logo/clients/jumping.svg',
	'references_9' 			=> 'public/images/logo/clients/lss.svg',
	'references_10' 		=> 'public/images/logo/clients/mjf.svg',
	'references_11' 		=> 'public/images/logo/clients/mmo.png',
	'references_12' 		=> 'public/images/logo/clients/pkf.svg',
	'references_13' 		=> 'public/images/logo/clients/porsche.png',
	'references_14' 		=> 'public/images/logo/clients/pv.png',
	'references_15' 		=> 'public/images/logo/clients/rockoz.svg',
	'references_16' 		=> 'public/images/logo/clients/skioo.png',
	'references_17' 		=> 'public/images/logo/clients/uhp.png',
	'references_18' 		=> 'public/images/logo/clients/vignerons.png',
	'references_19' 		=> 'public/images/logo/clients/gnv.svg',
	'references_20' 		=> 'public/images/logo/clients/bat.png',


	/*Contact*/
	'contact_title' 		=> 'Contact us',
	'online' 				=> 'Online form',
	'mail' 					=> 'info@many-ways.ch',
	'phone' 				=> '+41 21 320 35 35',



	/*SLIDER*/
	'microfibre' 			=> [

		'title' 		=> 'Microfibres',
		'bg' 			=> '#EFF4FA',

		'fo_url' 		=> 'public/images/header/microfibre/obj-front.png',
		'fo_width'		=> '250',
		'fo_x'			=> '7',
		'fo_y'			=> '55',

		'fs_url' 		=> 'public/images/header/microfibre/sha-front.png',
		'fs_width'		=> '350',
		'fs_x'			=> '0',
		'fs_y'			=> '-5',

		'bo_url' 		=> 'public/images/header/microfibre/obj-back.png',
		'bo_width'		=> '170',
		'bo_x'			=> '10',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/microfibre/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'polos' 			=> [

		'title' 		=> 'Polo Shirts',
		'bg' 			=> '#007ED9',

		'fo_url' 		=> 'public/images/header/polos/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '10',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/polos/sha-front.png',
		'fs_width'		=> '350',
		'fs_x'			=> '-5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/polos/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/polos/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'tee' 			=> [

		'title' 		=> 'T-Shirts',
		'bg' 			=> '#005CA8',

		'fo_url' 		=> 'public/images/header/tee/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '5',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/tee/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/tee/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/tee/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'bonnets' 			=> [

		'title' 		=> 'Beanies',
		'bg' 			=> '#252322',

		'fo_url' 		=> 'public/images/header/bonnets/obj-front.png',
		'fo_width'		=> '250',
		'fo_x'			=> '8',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/bonnets/sha-front.png',
		'fs_width'		=> '350',
		'fs_x'			=> '0',
		'fs_y'			=> '5',

		'bo_url' 		=> 'public/images/header/bonnets/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/bonnets/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'sweatshirt' 			=> [

		'title' 		=> 'Sweatshirts',
		'bg' 			=> '#FEC85C',

		'fo_url' 		=> 'public/images/header/sweatshirt/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '5',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/sweatshirt/sha-front.png',
		'fs_width'		=> '400',
		'fs_x'			=> '0',
		'fs_y'			=> '8',

		'bo_url' 		=> 'public/images/header/sweatshirt/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/sweatshirt/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'softshells' 			=> [

		'title' 		=> 'Softshells',
		'bg' 			=> '#1B2F43',

		'fo_url' 		=> 'public/images/header/softshell/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '5',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/softshell/sha-front.png',
		'fs_width'		=> '300',
		'fs_x'			=> '0',
		'fs_y'			=> '10',

		'bo_url' 		=> 'public/images/header/softshell/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/softshell/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'parapluies' 			=> [

		'title' 		=> 'Umbrellas',
		'bg' 			=> '#273D5F',

		'fo_url' 		=> 'public/images/header/parapluies/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '5',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/parapluies/sha-front.png',
		'fs_width'		=> '400',
		'fs_x'			=> '0',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/parapluies/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/parapluies/sha-back.png',
		'bs_width'		=> '300',
		'bs_x'			=> '5',
		'bs_y'			=> '25',

	],


	'doudounes' 			=> [

		'title' 		=> 'Padded Jackets',
		'bg' 			=> '#0F1014',

		'fo_url' 		=> 'public/images/header/doudounes/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '10',
		'fo_y'			=> '50',

		'fs_url' 		=> 'public/images/header/doudounes/sha-front.png',
		'fs_width'		=> '400',
		'fs_x'			=> '4',
		'fs_y'			=> '8',

		'bo_url' 		=> 'public/images/header/doudounes/obj-back.png',
		'bo_width'		=> '220',
		'bo_x'			=> '5',
		'bo_y'			=> '45',

		'bs_url' 		=> 'public/images/header/doudounes/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


	'cap' 			=> [

		'title' 		=> 'Caps',
		'bg' 			=> '#E8ECF3',

		'fo_url' 		=> 'public/images/header/cap/obj-front.png',
		'fo_width'		=> '300',
		'fo_x'			=> '0',
		'fo_y'			=> '58',

		'fs_url' 		=> 'public/images/header/cap/sha-front.png',
		'fs_width'		=> '400',
		'fs_x'			=> '-5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/cap/obj-back.png',
		'bo_width'		=> '200',
		'bo_x'			=> '5',
		'bo_y'			=> '50',

		'bs_url' 		=> 'public/images/header/cap/sha-back.png',
		'bs_width'		=> '300',
		'bs_x'			=> '5',
		'bs_y'			=> '15',

	],

];
