<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Social Media | Many Ways SA',
	'meta_desc' 			=> 'Find all of our products on Instagram.',


	'title' 				=> 'Products',
	'data_hero' 			=> 'true',


	/*HERO INFOS*/
	'hero_infos' 			=> [

		'fo_url' 		=> 'public/images/header/product/obj-front.png',
		'fo_width'		=> '250',
		'fo_x'			=> '5',
		'fo_y'			=> '55',

		'fs_url' 		=> 'public/images/header/product/sha-front.png',
		'fs_width'		=> '300',
		'fs_x'			=> '-5',
		'fs_y'			=> '5',

		'bo_url' 		=> 'public/images/header/product/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/product/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],



];
