<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Sucess Story - Madrid Open | Many Ways SA',
	'meta_desc' 			=> 'The Madrid Open tournament is a male ATP tennis tournament, and part of the female WTA circuit.',


	/*HEADER*/
	'img' 					=> 'public/images/cases/madrid/other/header.png',


	'compo_1' 				=> 'public/images/cases/madrid/other/compo1.png',
	'compo_2' 				=> 'public/images/cases/madrid/other/compo2.png',


	/*INFOS*/
	'client_label' 			=> 'Client',
	'client' 				=> 'Madrid Open',
	'descr_label' 			=> 'About',
	'descr' 				=> 'The Madrid Open tournament is a male ATP tennis tournament, and part of the female WTA circuit.',
	'date_label' 			=> 'Date',
	'date' 					=> 'Juin 2016',


	/*Process*/
	'1_title' 				=> 'Listening',
	'1_txt' 				=> 'Brainstorming with the client on the target and the universe of tennis; The customer wanted us to respect their logo and colors, which needed to be young and modern to fit the tennis world.',
	'2_title' 				=> 'Development of a personalized design',
	'2_txt' 				=> "Original t-shirts and caps that reflected the image of the brand. Adherence to the requested colour palette on the new design, in order to respect the customer's request and stay close to their established graphic universe.",
	'3_title' 				=> 'Choice of articles, objects,  textiles, etc',
	'3_txt' 				=> 'A series of textiles focusing on sport and youth.',
	'4_title' 				=> 'Choice of fabrication location',
	'4_txt' 				=> 'Manufactured hoodies and printed the t-shirts in Spain for greater flexibility in terms of the replenishing stocks should the client require additional materials before the event.',
	'5_title' 				=> 'Delivery',
	'5_txt' 				=> 'Delivery 1 month before set up of the event.',


	'products_title' 		=> 'Products',
	'has_products' 			=> 'true',
	'products' 				=> [
		'1' 		=>'public/images/cases/madrid/articles/1.jpg',
		'2' 		=>'public/images/cases/madrid/articles/2.jpg',
		'3' 		=>'public/images/cases/madrid/articles/3.png',
		'4' 		=>'public/images/cases/madrid/articles/4.png',
		'5' 		=>'public/images/cases/madrid/articles/5.png',
		'6' 		=>'public/images/cases/madrid/articles/6.png',
		'7' 		=>'public/images/cases/madrid/articles/7.png',
		'8' 		=>'public/images/cases/madrid/articles/8.jpg',
	],
	'thumbs' 				=> [
		'1' 		=>'public/images/cases/madrid/thumbs/1.jpg',
		'2' 		=>'public/images/cases/madrid/thumbs/2.jpg',
		'3' 		=>'public/images/cases/madrid/thumbs/3.png',
		'4' 		=>'public/images/cases/madrid/thumbs/4.png',
		'5' 		=>'public/images/cases/madrid/thumbs/5.png',
		'6' 		=>'public/images/cases/madrid/thumbs/6.png',
		'7' 		=>'public/images/cases/madrid/thumbs/7.png',
		'8' 		=>'public/images/cases/madrid/thumbs/8.jpg',
	],
	'no_products' 			=> 'Our products are coming soon.',


	'cta-contact_1' 	=>"Got a project ? Don't hesitate to",
	'cta-contact_2' 	=>'contact us',

	'cta-contact_3a' 	=>'Interested ?',
	'cta-contact_3b' 	=>'Contact us',
	'cta-contact_3c' 	=>'for a free quote!',

	/*Next*/
	'next_title' 		=> 'PKF Center',
	'next_label' 		=> 'Next Success Story',
	'next_img' 			=> 'public/images/cases/pkf/other/header.jpg',
	'next_url' 			=> 'en/pkf-center',

];
