<?php

return [

	/*
	|--------------------------------------------------------------------------
	| URLS
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'home' 					=> 'en',

	'products' 				=> 'en/social-media',
	'success_stories' 		=> 'en/success-stories',

	'contact' 				=> 'en/contact',
	'confirm' 				=> 'en/thank-you',

	'success_stories_id' 	=> '[0-9a-z\-]+',

	// -- A MODIFIER/SUPPRIMER -> @foreach ($app_langs)

	'en'					=> 'en',
	'fr'					=> 'fr',
	'de'					=> 'de',

	// -- A SUPPRIMER

	'madrid' 				=> 'en/madrid-open',
	'pkf' 					=> 'en/pkf-center',
	'nendaz' 				=> 'en/gaby-sport-nendaz',
	'fer' 					=> 'en/commerce-de-fer',
];
