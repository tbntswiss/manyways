<?php

return [

	/*
	|--------------------------------------------------------------------------
	| APP DEFAULTS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Textile et objets personnalisés | '.appName(),
	'meta_desc' 			=> 'Nous sommes spécialisés dans la création et la production de textile et objets personnalisés pour mettre votre marque en valeur.',

];
