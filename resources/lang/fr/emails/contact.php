<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'subject' 				=> appName().' | Contact',

	'title' 				=> 'Vous avez reçu un email via many-ways.ch.',
	'copyright' 			=> 'Many Ways '.date('Y'),

];
