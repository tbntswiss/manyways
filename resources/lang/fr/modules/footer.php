<?php

return [

	/*
	|--------------------------------------------------------------------------
	| FOOTER
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'contact_title'			=> 'Contactez-nous',
	'mail' 					=> 'info@many-ways.ch',
	'phone' 				=> '+41 21 320 35 35',

	'eshop_title' 			=> 'Nos e-shops',

	'eshops' 				=> [
		'1' 	=> 'Air 14',
		'2' 	=> 'Geneva Open',
		'3' 	=> 'FAI',
		'4' 	=> 'Police Vaud',
		'5' 	=> 'Boutique Many Ways',
		'6' 	=> '1936',
		'7' 	=> 'Discount',
		'8' 	=> 'ISTA',
		'9' 	=> 'Jeep',
		'10' 	=> "Rock Oz'arènes",
		'11' 	=> "Fêtes des Vignerons 2019",
	],

	'links' 				=> [
		'1' 	=> 'http://shop.many-ways.ch/',
		'2' 	=> 'http://genevaopen.many-ways.ch/',
		'3' 	=> 'http://fai.many-ways.ch/',
		'4' 	=> 'http://vdpolice.many-ways.ch/',
		'5' 	=> 'http://manyways-shop.many-ways.ch/',
		'6' 	=> 'http://1936collection.many-ways.ch/',
		'7' 	=> 'http://discount.many-ways.ch/',
		'8' 	=> 'http://ista.many-ways.ch/',
		'9' 	=> 'http://jeep.many-ways.ch/',
		'10' 	=> 'http://rockozarenes.many-ways.ch/',
		'11' 	=> 'https://www.fdvshop.ch',
	],

	'newsletter_title'		=> 'Newsletter',
	'newsletter_placeholder'=> 'E-mail',
	'newsletter_cta' 		=> "S'inscrire",

	'lang_fr' 				=> 'Français',
	'lang_de' 				=> 'Allemand',
	'lang_en' 				=> 'Anglais',

];
