<?php

return [

	/*
	|--------------------------------------------------------------------------
	| NAVIGATION
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'home' 					=> 'Accueil',
	'stories' 				=> 'Succès & Produits',
	'products' 				=> 'Fil d\'actualité',
	'contact' 				=> 'Contactez-nous'

];
