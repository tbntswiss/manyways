<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Contact | Many Ways SA',
	'meta_desc' 			=> "Vous trouverez ici toutes les informations nécessaires pour prendre contact avec nous. N'hésitez pas à nous faire part de votre projet !",


	/*Slider*/
	'title' 				=> 'Contactez-nous',
	'data_hero' 			=> 'true',


	/*HERO INFOS*/
	'hero_infos' 			=> [

		'fo_url' 		=> 'public/images/header/contact/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '10',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/contact/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/contact/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '15',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/contact/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],

	/*Infos*/
	'contact_about' 		=> 'Une question ? Un projet dont vous voulez nous faire part ? Contactez-nous en remplissant le formulaire ci-dessous.',

	/*Form*/
	'surname'				=> [
		'reference' 	=> 'surname',
		'txt'			=> 'Nom',
		'on_error' 		=> "Veuillez remplir ce champ s'il vous plaît.",
		'required' 		=> 'required',
	],

	'firstname'				=> [
		'reference' => 'firstname',
		'txt'		=> 'Prénom',
		'on_error' 	=> "Veuillez remplir ce champ s'il vous plaît.",
		'required' 	=> 'required',
	],

	'email'					=> [
		'reference' => 'email',
		'txt'		=> 'E-mail',
		'on_error' 	=> "Veuillez remplir ce champ s'il vous plaît.",
		'required' 	=> 'required',
	],

	'tel'					=> [
		'reference' => 'tel',
		'txt'		=> 'Numéro de téléphone',
		'on_error' 	=> "Veuillez remplir ce champ s'il vous plaît.",
		'required' 	=> '',
	],

	'message'				=> [
		'reference' => 'message',
		'txt'		=> 'Message',
		'on_error' 	=> "Veuillez ajouter votre message ici s'il vous plaît.",
		'required' 	=> 'required',
	],


	'submit' 				=> 'Envoyer',



	/*Contact infos*/
	'agency_name' 			=> 'Many Ways SA',
	'phone' 				=> '+41 21 320 35 35',
	'mail' 					=> 'info@many-ways.ch',
	'street' 				=> 'Avenue Longemalle 21B<br />1020 Renens (Suisse)',


	/*CONFIRMATION PAGE*/
	'c_meta_title' 			=> 'Merci | Many Ways SA',
	'c_meta_desc' 			=> "Merci ! Nous prendrons contact avec vous dans les plus brefs délais.",


	'confirm_title' 		=> 'Merci !',
	'confirm_txt' 			=> 'Votre message a bien été envoyé. Nous prendrons contact avec vous dans les plus brefs délais.',
	'confirm_story' 		=> 'Lire notre dernière Success Story',


	'data_hero_confirm' 	=> 'true',


	/*HERO INFOS*/
	'hero_infos_confirm' 	=> [

		'fo_url' 		=> 'public/images/header/form_confirm/obj-front.png',
		'fo_width'		=> '350',
		'fo_x'			=> '0',
		'fo_y'			=> '60',

		'fs_url' 		=> 'public/images/header/form_confirm/sha-front.png',
		'fs_width'		=> '200',
		'fs_x'			=> '5',
		'fs_y'			=> '0',

		'bo_url' 		=> 'public/images/header/form_confirm/obj-back.png',
		'bo_width'		=> '150',
		'bo_x'			=> '10',
		'bo_y'			=> '40',

		'bs_url' 		=> 'public/images/header/form_confirm/sha-back.png',
		'bs_width'		=> '200',
		'bs_x'			=> '10',
		'bs_y'			=> '25',

	],


];
