<?php

return [

	/*
	|--------------------------------------------------------------------------
	| LOGIN
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Recover password | Paleo',
	'meta_desc' 			=> 'Recover password | Paleo | Description',

	'title' 				=> 'Recover password',

	'placeh_password' 		=> 'Enter password',
	'placeh_password_r' 	=> 'Repeat password',

	'btn_send' 				=> 'Update',
	'btn_login' 			=> 'Login',

	'success' 				=> 'Your password has been updated.',
	'error' 				=> 'Your password wasn\'t updated because either the token is expired or the user didn\'t asked for it.',
	'token_invalid' 		=> 'Your password can\'t be updated because either the token is expired or the user didn\'t asked for it.',

];
