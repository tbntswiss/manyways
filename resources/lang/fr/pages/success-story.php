<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOMEPAGE
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Sucess Story - Madrid Open | Many Ways SA',
	'meta_desc' 			=> 'Le tournoi de Madrid est un tournoi de tennis masculin du circuit ATP et féminin du circuit WTA.',

	/*HEADER*/
	'img' 					=> 'public/images/cases/madrid/other/header.png',


	'compo_1' 				=> 'public/images/cases/madrid/other/compo1.png',
	'compo_2' 				=> 'public/images/cases/madrid/other/compo2.png',


	/*INFOS*/
	'client_label' 			=> 'Client',
	'client' 				=> 'Madrid Open',
	'descr_label' 			=> 'A propos',
	'descr' 				=> 'Le tournoi de Madrid est un tournoi de tennis masculin du circuit ATP et féminin du circuit WTA.',
	'date_label' 			=> 'Date',
	'date' 					=> 'Juin 2016',


	/*Process*/
	'1_title' 				=> 'Ecoute',
	'1_txt' 				=> 'Brainstorming avec le client sur la cible et l’univers du tennis ; le client souhaitait que nous respections leur logo et ses couleurs, qui se veulent jeunes et modernes pour le milieu du tennis.',
	'2_title' 				=> 'Développement d’un  design personnalisé ',
	'2_txt' 				=> 'T-shirts originaux et casquettes à l’image de la marque. Jeu sur les déclinaisons de couleurs plus que sur un nouveau design, afin de respecter la demande du client : rester proche de leur univers graphique déjà établi.',
	'3_title' 				=> 'Choix des articles, objets,  textiles, etc',
	'3_txt' 				=> 'Un choix de textile axé sur le sport, les jeunes et les enfants.',
	'4_title' 				=> 'Choix du lieux de  production',
	'4_txt' 				=> 'Fabrication des hoodies en Espagne pour une plus grande flexibilité des réassorts éventuels pendant la manifestation.',
	'5_title' 				=> 'Livraison',
	'5_txt' 				=> 'Livraison 1 mois avant la mise en place de la manifestation.',

	'products_title' 		=> 'Produits',
	'has_products' 			=> 'true',
	'products' 				=> [
		'1' 		=>'public/images/cases/madrid/articles/1.jpg',
		'2' 		=>'public/images/cases/madrid/articles/2.jpg',
		'3' 		=>'public/images/cases/madrid/articles/3.png',
		'4' 		=>'public/images/cases/madrid/articles/4.png',
		'5' 		=>'public/images/cases/madrid/articles/5.png',
		'6' 		=>'public/images/cases/madrid/articles/6.png',
		'7' 		=>'public/images/cases/madrid/articles/7.png',
		'8' 		=>'public/images/cases/madrid/articles/8.jpg',
	],
	'thumbs' 				=> [
		'1' 		=>'public/images/cases/madrid/thumbs/1.jpg',
		'2' 		=>'public/images/cases/madrid/thumbs/2.jpg',
		'3' 		=>'public/images/cases/madrid/thumbs/3.png',
		'4' 		=>'public/images/cases/madrid/thumbs/4.png',
		'5' 		=>'public/images/cases/madrid/thumbs/5.png',
		'6' 		=>'public/images/cases/madrid/thumbs/6.png',
		'7' 		=>'public/images/cases/madrid/thumbs/7.png',
		'8' 		=>'public/images/cases/madrid/thumbs/8.jpg',
	],
	'no_products' 			=> 'Nos produits seront bientôt disponibles.',


	'cta-contact_1' 	=>'Vous avez un projet ? N’hésitez pas à',
	'cta-contact_2' 	=>'nous contacter',

	'cta-contact_3a' 	=>'Intéressés ?',
	'cta-contact_3b' 	=>'Contactez-nous',
	'cta-contact_3c' 	=>'pour un devis sur mesure !',

	/*Next*/
	'next_title' 		=> 'PKF Center',
	'next_label' 		=> 'success story suivante',
	'next_img' 			=> 'public/images/cases/pkf/other/header.jpg',
	'next_url' 			=> 'fr/pkf-center',

];
