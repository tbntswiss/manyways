<?php

return [

	/*
	|--------------------------------------------------------------------------
	| URLS
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'home' 					=> 'fr',

	'products' 				=> 'fr/social-media',
	'success_stories' 		=> 'fr/success-stories',

	'contact' 				=> 'fr/contact',
	'confirm' 				=> 'fr/merci',

	'success_stories_id' 	=> '[0-9a-z\-]+',

	// -- A MODIFIER/SUPPRIMER -> @foreach ($app_langs)

	'en'					=> 'en',
	'fr'					=> 'fr',
	'de'					=> 'de',

	// -- A SUPPRIMER

	'madrid' 				=> 'fr/madrid-open',
	'pkf' 					=> 'fr/pkf-center',
	'nendaz' 				=> 'fr/gaby-sport-nendaz',
	'fer' 					=> 'fr/commerce-de-fer',
];
