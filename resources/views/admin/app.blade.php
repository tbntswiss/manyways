<!DOCTYPE html>
<html>
<head>
	<base href="{{ $app_url }}/" />

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="HandheldFriendly" content="true"><meta content="telephone=no" name="format-detection" />

	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-16x16.png') }}" sizes="16x16">

	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
	<link rel="stylesheet" type="text/css" href="public/admin/styles/vendor.css" />

	<title>{{ $app_name }} Admin | {{ $title or '' }}</title>

	@include('admin.includes.js')

	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
	<script src="public/admin/scripts/vendor.js"></script>
</head>
<body class="uk-background-light">
	<!--[if lte IE 10]>
		@include('admin.modules.browsehappy')
	<![endif]-->

	@include('admin.includes.menu')

	<main id="app" class="uk-padding uk-padding-remove-horizontal">
		@yield('content')

		<script>
			window.Laravel.js = "{{ $js ?? '' }}";
			window.Laravel.menu = "{{ $menu ?? '' }}";
		</script>
	</main>
</body>
</html>
