<script>
	window.Laravel = @define(
		$filename = pathinfo(resource_path().'/lang/'.$app_lang->code.'/urls')['filename'];
		$trans = [$filename => trans($filename)];

		echo json_encode([
			'app' => ['name' => $app_name, 'url' => $app_url, 'base' => $app_base],
			'token' => csrf_token(), 'lang' => $app_lang, 'langs' => $app_langs, 'trans' => $trans,
		]);
	);
</script>
