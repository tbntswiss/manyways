@if ($is_logged_in ?? false)
	<div id="navbar" class="uk-navbar-container" uk-sticky="media:960">
		<div class="uk-container">
			<nav uk-navbar>
				<div class="uk-navbar-left">
					<div class="uk-navbar-item">
						<a href="admin" class="uk-link-reset">Manyways Adminstration</a>
					</div>
					<ul class="uk-visible@m uk-navbar-nav">
						<li><a href="admin/success-stories">Success stories</a></li>
					</ul>
				</div>
				<div class="uk-navbar-right">
					<div class="uk-navbar-item">
						{{-- <a href="admin/config" class="uk-link-reset">Configurations</a> --}}
					</div>
					<div class="uk-navbar-item">
						<a href="admin/logout" class="uk-link-reset">Logout</a>
					</div>
				</div>
			</nav>
		</div>
	</div>
@endif