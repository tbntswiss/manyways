@extends('admin.app', [
	'title' => 'Login',
])

@section('content')
<div id="admin-page-login" class="uk-container uk-container-small">
	<div class="uk-section uk-width-large@s uk-margin-auto">
		<h1 class="uk-margin-medium">Welcome back admin!</h1>

		<form method="post">
			{{ csrf_field() }}

			@if ($errors->any())
				<div class="uk-alert-danger" uk-alert>
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif

			<div class="uk-margin">
				<input class="uk-input" type="text" name="username" placeholder="Enter username">
			</div>
			<div class="uk-margin">
				<input class="uk-input" type="password" name="password" placeholder="Enter password">
			</div>

			<div class="uk-margin uk-flex uk-flex-middle uk-flex-right">
				<button type="submit" class="uk-button uk-button-primary">Sign in</button>
			</div>
		</form>
	</div>
</div>
@endsection
