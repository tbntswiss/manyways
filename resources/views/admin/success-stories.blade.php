@extends('admin.app', [
	'title' => 'Success stories',
	'js' => 'admin.success_stories',
	'menu' => 'success_stories',
])

@section('content')
<div id="admin-page-success-stories" class="uk-container">
	<div class="uk-flex uk-flex-between uk-flex-middle uk-margin-large">
		<a pjax href="admin/success-story/create" class="uk-button uk-button-primary">Add success story</a>
		<input class="uk-input uk-form-width-xmedium uk-border-none uk-border-bottom" type="text" placeholder="Search" data-search>
	</div>

	<div class="uk-grid-small uk-margin-small-bottom" uk-grid>
		<div class="uk-width-2-5">Name</div>
		<div class="uk-width-expand">Creation date</div>
	</div>

	<div uk-margin>
		@forelse ($success_stories as $success_story)
			<div class="uk-card uk-card-default uk-padding-small @if ($success_story->is_active == 0) tbnt-opacity-50 @endif" data-success-story-id="{{ $success_story->id }}">
				<div class="uk-grid-small uk-flex uk-flex-middle" uk-grid>
					<div class="uk-width-2-5">
						<span class="uk-text-bold">{{ $success_story->client }}</span>
					</div>
					<div class="uk-width-expand">
						{{ format_date($success_story->created_at, 'd.m.Y') }}
					</div>
					<div class="uk-width-1-1 uk-width-auto@s uk-flex uk-flex-right">
						<a pjax href="admin/success-story/update/{{ $success_story->id }}"><i class="ionicon uk-icon-link uk-padding-small-horizontal uk-padding-tiny-vertical ion-edit"></i></a>
					</div>
				</div>
			</div>
		@empty
			<div class="uk-card uk-card-default uk-padding-small">
				<div class="uk-flex uk-flex-middle">
					<span>There is no success story.</span>
				</div>
			</div>
		@endforelse
	</div>
</div>

@script($success_stories, 'SUCCESS_STORIES')
@endsection
