@extends('admin.app', [
	'title' => 'Edit success story',
	'js' => 'admin.success_story.update',
])

@section('content')
<div id="admin-page-success-story-update" class="uk-container">
	<form class="uk-form-stacked" method="post" enctype="multipart/form-data" data-form>
		{{ csrf_field() }}

		<h1 class="uk-margin">Edit success story</h1>

		<div class="uk-flex uk-flex-right">
			<div class="uk-margin-right">
				<button type="button" class="uk-button uk-button-primary" uk-toggle="target:[data-gallery]">Gallery</button>
			</div>
			<div class="uk-button-group">
				@foreach ($app_langs as $lang)
					<button type="button" class="uk-button uk-button-primary" uk-toggle="target:[data-lang-{{ $lang->id }}]">{{ $lang->code }}</button>
				@endforeach
			</div>
		</div>
		<div class="uk-margin-bottom uk-text-right">
			<div name="lang"></div>
		</div>

		<div class="uk-grid-small uk-margin" uk-grid>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Client</label>
				<div class="uk-form-controls">
					@include('fields.card-input', [
						'input' => [
							'name' => 'client',
							'value' => $success_story->client,
						],
					])
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Status</label>
				<div class="uk-form-controls">
					@include('fields.card-checkbox', [
						'input' => [
							'name' => 'is_active',
							'value' => 1,
							'checked' => $success_story->is_active == 1,
						],
						'label' => [
							'active' => 'Active',
							'inactive' => 'Inactive',
						],
					])
				</div>
			</div>
		</div>
		<div class="uk-grid-small uk-margin" uk-grid>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Background image</label>
				<div class="uk-form-controls">
					@include('fields.input-image', [
						'name' => 'background',
						'data' => 'background',
						'image' => $success_story->background,
					])
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Is product</label>
				<div class="uk-form-controls uk-margin-bottom">
					@include('fields.card-checkbox', [
						'input' => [
							'name' => 'is_product',
							'value' => 1,
							'checked' => $success_story->is_product == 1,
						],
						'label' => [
							'active' => 'Yes',
						],
					])
				</div>
				<label class="uk-form-label">Visible at</label>
				<div class="uk-form-controls">
					@include('fields.card-input', [
						'input' => [
							'name' => 'visible_at',
							'value' => date('Y-m-d', strtotime($success_story->visible_at)),
							'type' => 'date',
						],
					])
				</div>
			</div>
		</div>
		<div class="uk-grid-small uk-margin" uk-grid>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Image 1</label>
				<div class="uk-form-controls">
					@include('fields.input-image', [
						'name' => 'image_1',
						'data' => $success_story->image_1->id ?? 'image_1',
						'image' => $success_story->image_1,
					])
				</div>
			</div>
			<div class="uk-width-1-1 uk-width-1-2@m">
				<label class="uk-form-label">Image 2</label>
				<div class="uk-form-controls">
					@include('fields.input-image', [
						'name' => 'image_2',
						'data' => $success_story->image_2->id ?? 'image_2',
						'image' => $success_story->image_2,
					])
				</div>
			</div>
		</div>

		<div uk-modal="esc-close:false; bg-close:false; center:true; container:[data-form]" data-gallery>
			<div class="uk-form-stacked uk-modal-dialog uk-background-light">
				<div class="uk-modal-header">
					<h2 class="uk-modal-title">Gallery</h2>
				</div>
				<div class="uk-modal-body" uk-overflow-auto uk-margin="margin:uk-margin">
					<div hidden data-gallery-template>
						<label class="uk-form-label">Image</label>
						<div class="uk-form-controls">
							@include('fields.input-image', [
								'name' => 'gallery[]',
								'data' => '__GALLERY__',
							])
						</div>
					</div>

					@foreach ($success_story->gallery as $image)
						<div data-gallery-item>
							<label class="uk-form-label">Image</label>
							<div class="uk-form-controls">
								@include('fields.input-image', [
									'name' => 'gallery[]',
									'data' => $image->id,
									'image' => $image,
								])
							</div>
						</div>
					@endforeach
				</div>
				<div class="uk-modal-footer uk-text-right">
					<button type="button" class="uk-button uk-button-default uk-modal-close">Close</button>
				</div>
			</div>
		</div>

		@foreach ($app_langs as $lang)
			@define(
				$post_lang = object_merge([
					'url' => '',
					'title' => '',
					'description' => '',
					'step_1_title' => '',
					'step_1' => '',
					'step_2_title' => '',
					'step_2' => '',
					'step_3_title' => '',
					'step_3' => '',
					'step_4_title' => '',
					'step_4' => '',
					'step_5_title' => '',
					'step_5' => '',
				], $success_story->langs[$lang->code] ?? [])
			)

			<div uk-modal="esc-close:false; bg-close:false; center:true; container:[data-form]" data-lang-{{ $lang->id }}>
				<div class="uk-form-stacked uk-modal-dialog uk-background-light">
					<div class="uk-modal-header">
						<h2 class="uk-modal-title">{{ $lang->name }}</h2>
					</div>
					<div class="uk-modal-body" uk-overflow-auto uk-margin="margin:uk-margin">
						<div>
							<label class="uk-form-label">URL</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][url]',
										'value' => $post_lang->url,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Title</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][title]',
										'value' => $post_lang->title,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Description</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][description]',
										'textarea' => $post_lang->description,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Step 1</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_1_title]',
										'value' => $post_lang->step_1_title,
									],
								])
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_1]',
										'textarea' => $post_lang->step_1,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Step 2</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_2_title]',
										'value' => $post_lang->step_2_title,
									],
								])
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_2]',
										'textarea' => $post_lang->step_2,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Step 3</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_3_title]',
										'value' => $post_lang->step_3_title,
									],
								])
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_3]',
										'textarea' => $post_lang->step_3,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Step 4</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_4_title]',
										'value' => $post_lang->step_4_title,
									],
								])
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_4]',
										'textarea' => $post_lang->step_4,
									],
								])
							</div>
						</div>
						<div>
							<label class="uk-form-label">Step 5</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_5_title]',
										'value' => $post_lang->step_5_title,
									],
								])
								@include('fields.card-input', [
									'input' => [
										'name' => 'langs['.$lang->code.'][step_5]',
										'textarea' => $post_lang->step_5,
									],
								])
							</div>
						</div>
					</div>
					<div class="uk-modal-footer uk-text-right">
						<button type="button" class="uk-button uk-button-default uk-modal-close">Close</button>
					</div>
				</div>
			</div>
		@endforeach

		<div class="uk-clearfix">
			<div class="uk-float-right">
				<button type="button" class="uk-button uk-button-danger" data-delete>Delete</button>
				<a pjax href="admin/success-stories" type="button" class="uk-button uk-button-default">Cancel</a>
				<button type="button" class="uk-button uk-button-primary" data-submit>Update <div class="uk-margin-small-left" hidden uk-spinner></div></button>
			</div>
		</div>
	</form>
</div>

@script($success_story, 'SUCCESS_STORY')
@endsection
