{{ trans('emails/contact.title') }}

{{ $form['surname'] }}

{{ $form['firstname'] }}

{{ $form['email'] }}

{{ $form['phone'] or '' }}

{{ $form['message'] }}

{{ trans('emails/contact.copyright') }}
