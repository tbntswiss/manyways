@extends('public.app', [
'js' => '404',
'page_langs' => [
	'url' => 'urls.home'
],
'social' => [
	'title' => trans('pages/404.meta_title'),
	'description' => trans('pages/404.meta_desc'),
],
])

@section('content')
<div id="top" class="wrapper--fluid hero--header hero--header--smaller confirm--header" data-hero="{{ trans('pages/404.data_hero') }}">
	<h1>{!! trans('pages/404.confirm_title') !!}</h1>
	@if(Lang::has('pages/404.hero_infos', 'en'))
		<div class="header--front">
			<img class="fo" src="{{ trans('pages/404.hero_infos.fo_url') }}" alt="" data-width="{{ trans('pages/404.hero_infos.fo_width') }}" data-x="{{ trans('pages/404.hero_infos.fo_x') }}" data-y="{{ trans('pages/404.hero_infos.fo_y') }}">
			<img class="fs" src="{{ trans('pages/404.hero_infos.fs_url') }}" alt="" data-width="{{ trans('pages/404.hero_infos.fs_width') }}" data-x="{{ trans('pages/404.hero_infos.fs_x') }}" data-y="{{ trans('pages/404.hero_infos.fs_y') }}">
		</div>
		<div class="header--back">
			<img class="bo" src="{{ trans('pages/404.hero_infos.bo_url') }}" alt="" data-width="{{ trans('pages/404.hero_infos.bo_width') }}" data-x="{{ trans('pages/404.hero_infos.bo_x') }}" data-y="{{ trans('pages/404.hero_infos.bo_y') }}">
			<img class="bs" src="{{ trans('pages/404.hero_infos.bs_url') }}" alt="" data-width="{{ trans('pages/404.hero_infos.bs_width') }}" data-x="{{ trans('pages/404.hero_infos.bs_x') }}" data-y="{{ trans('pages/404.hero_infos.bs_y') }}">
		</div>
	@endif
	<div class="bottom-stuff">
		<div class="grid col-xs-10 col-s-6 part border">
			<p>{!! trans('pages/404.confirm_txt') !!}</p>
		</div>
		<div class="grid col-xs-10 col-s-4 part">
			@define(
				$last_url = \App\Project\SuccessStory::lastUrl($app_lang->id);
			)

			@if ($last_url !== '')
				<a pjax href="{{ trans('urls.success_stories').'/'.$last_url }}">{!! trans('pages/contact.confirm_story') !!}</a>
			@else
				<a pjax href="{{ trans('urls.home') }}">{!! trans('modules/navigation.home') !!}</a>
			@endif
		</div>
	</div>
</div>

@endsection

