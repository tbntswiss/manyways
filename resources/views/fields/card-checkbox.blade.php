@define(
	$card = isset($card) && is_array($card) ? $card : [];
	$label = isset($label) && is_array($label) ? $label : [];
	$input = isset($input) && is_array($input) ? $input : [];

	$id = $input['id'] ?? str_replace(['[', ']'], '', ($input['name'] ?? '').'-'.($input['value'] ?? ''));

	$label_active = $label['active'] ?? 'Active';
	$label_inactive = $label['inactive'] ?? $label_active;
)

{{--

card => [
	class => 'tag class',
	data => 'tag data',
	editable => 'tag data',
],
label => [
	active => '<label> active text || ""',
	inactive => '<label> inactive text || <label> active text',
	class => '<label> class',
],
input => [
	id => '<input> id',
	value => '<input> value',
	name => '<input> name',
	class => '<input> class',
	data => '<input> data',
	checked => '<input> is checked',
	disabled => '<input> is disabled',
]
tooltip => [
	text => 'tooltip text',
	class => 'tooltip class',
],

--}}

<div class="card-module uk-card uk-flex uk-flex-stretch {{ isset($card['editable']) && !$card['editable'] ? '' : 'uk-card-default' }} {{ $card['class'] or '' }}" {{ $card['data'] or '' }}>
	<div class="uk-grid-small uk-flex uk-flex-middle uk-flex-1" uk-grid>
		@if (isset($tooltip))
			<div class="">
				<i class="ion-information-circled uk-text-primary" title="{{ $tooltip['text'] }}" uk-tooltip="pos:left;"></i>
			</div>
		@endif

		<label class="uk-width-expand {{ $label['class'] or '' }}" for="checkbox-target-{{ $id }}" data-checkbox-target-{{ $id }} @if (isset($input['checked']) === false || (boolean) $input['checked'] === false) hidden @endif>{!! $label_active !!}</label>
		<label class="uk-width-expand {{ $label['class'] or '' }}" for="checkbox-target-{{ $id }}" data-checkbox-target-{{ $id }} @if (isset($input['checked']) === true && (boolean) $input['checked'] === true) hidden @endif>{!! $label_inactive !!}</label>
		<input class="uk-checkbox uk-margin-small-left {{ $input['class'] or '' }}" uk-toggle="target:[data-checkbox-target-{{ $id }}]" id="checkbox-target-{{ $id }}" type="checkbox" name="{{ $input['name'] or '' }}" value="{{ $input['value'] or '' }}" {{ isset($input['disabled']) && $input['disabled'] ? 'disabled' : '' }} {{ isset($input['checked']) && $input['checked'] ? 'checked' : '' }} {{ $input['data'] or '' }}>
	</div>
</div>
