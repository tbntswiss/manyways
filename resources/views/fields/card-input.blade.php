@define(
	$card = isset($card) && is_array($card) ? $card : [];
)

{{--

card => [
	class => 'tag class',
	data => 'tag data',
	editable => 'tag data',
	checkbox => [
		id => '<input> id',
		value => '<input> value',
		name => '<input> name',
		class => '<input> class',
		data => '<input> data',
		checked => '<input> is checked',
		disabled => '<input> is disabled',
	],
]

--}}

<div class="card-module uk-card uk-flex uk-flex-stretch {{ isset($card['editable']) && !$card['editable'] ? '' : 'uk-card-default' }} {{ $card['class'] or '' }}" {{ $card['data'] or '' }}>
	<div class="uk-grid-small uk-flex uk-flex-middle uk-flex-1" uk-grid>
		<div class="uk-width-expand">
			<div class="uk-grid-small uk-flex uk-flex-middle" uk-grid>
				@if (isset($card['checkbox']))
					@define(
						$card_check = is_array($card['checkbox']) ? $card['checkbox'] : [];
					)

					<div class="card-input-card-check uk-width-auto">
						<input class="uk-checkbox uk-margin-tiny-vertical" type="checkbox" name="{{ $card_check['name'] or '' }}" value="{{ $card_check['value'] or '' }}" {{ isset($card_check['disabled']) && $card_check['disabled'] ? 'disabled' : '' }} {{ isset($card_check['checked']) && $card_check['checked'] ? 'checked' : '' }} {{ $card_check['data'] or '' }}>
					</div>
				@endif

				@include('fields.card-input.inputs', ['input' => $input ?? [], 'inputs' => $inputs ?? []])
				@include('fields.card-input.checkboxes', ['checkbox' => $checkbox ?? [], 'checkboxes' => $checkboxes ?? []])
				@include('fields.card-input.labels', ['label' => $label ?? [], 'labels' => $labels ?? []])
			</div>
		</div>

		@if (isset($button) || isset($buttons))
			<div class="uk-width-1-1 uk-width-auto@s uk-flex uk-flex-right uk-flex-last uk-flex-start@s">
				<div class="uk-grid-small uk-flex uk-flex-middle" uk-grid>
					@include('fields.card-input.buttons', ['button' => $button ?? [], 'buttons' => $buttons ?? []])
				</div>
			</div>
		@endif

		@if (isset($icon) || isset($icons))
			<div class="uk-width-auto uk-flex uk-flex-right uk-flex-start uk-flex-last@s">
				<div class="uk-grid-small uk-flex uk-flex-middle" uk-grid>
					@include('fields.card-input.icons', ['icon' => $icon ?? [], 'icons' => $icons ?? []])
				</div>
			</div>
		@endif
	</div>
</div>
