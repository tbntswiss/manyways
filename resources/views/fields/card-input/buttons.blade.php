@define(
	$button = isset($button) && is_array($button) ? $button : [];
	$buttons = isset($buttons) && is_array($buttons) ? $buttons : [];

	array_unshift($buttons, $button);
)

{{--

button => [
	text => '<button> text',
	class => '<button> class',
	data => '<button> data',
]

buttons => [
	button,
	...,
]

--}}

@if (count(array_collapse($buttons)))
	<div class="card-input-buttons uk-width-1-1 uk-width-auto@s uk-flex uk-flex-right uk-flex-last uk-flex-first@s">
		@foreach ($buttons as $button)
			@continue (count($button) === 0)

			<button type="button" class="uk-button uk-text-nowrap {{ $button['class'] or '' }}" {{ $button['data'] or '' }}>
				{{ $button['text'] or '' }}
			</button>
		@endforeach
	</div>
@endif
