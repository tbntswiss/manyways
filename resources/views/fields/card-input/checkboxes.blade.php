@define(
	$checkbox = isset($checkbox) && is_array($checkbox) ? $checkbox : [];
	$checkboxes = isset($checkboxes) && is_array($checkboxes) ? $checkboxes : [];

	array_unshift($checkboxes, $checkbox);
)

{{--

checkbox => [
	label => [
		active => '<label> active text || ""',
		inactive => '<label> inactive text || <label> active text',
		class => '<label> class',
	],
	input => [
		id => '<input> id',
		value => '<input> value',
		name => '<input> name',
		class => '<input> class',
		data => '<input> data',
		checked => '<input> is checked',
		disabled => '<input> is disabled',
	],
]

checkboxes => [
	checkbox,
	...,
]

--}}

@if (count(array_collapse($checkboxes)))
	<div class="card-input-checkboxes uk-width-1-1 uk-width-auto@s">
		@foreach ($checkboxes as $checkbox)
			@continue (count($checkbox) === 0)

			@define(
				$label = isset($checkbox['label']) && is_array($checkbox['label']) ? $checkbox['label'] : [];
				$checkbox = isset($checkbox['input']) && is_array($checkbox['input']) ? $checkbox['input'] : [];

				$checkbox['value'] = $checkbox['value'] ?? 1;
				$id = $checkbox['id'] ?? str_replace(['[', ']'], '', ($checkbox['name'] ?? '').'-'.($checkbox['value'] ?? ''));

				$label_active = $label['active'] ?? '';
				$label_inactive = $label['inactive'] ?? $label_active;
			)

			<div class="uk-margin-tiny-vertical uk-flex uk-flex-middle">
				<label class="uk-padding-tiny-vertical {{ $label['class'] or '' }}" for="checkbox-target-{{ $id }}" data-checkbox-target-{{ $id }} @if (isset($checkbox['checked']) === false || $checkbox['checked'] === false) hidden @endif>{!! $label_active !!}</label>
				<label class="uk-padding-tiny-vertical {{ $label['class'] or '' }}" for="checkbox-target-{{ $id }}" data-checkbox-target-{{ $id }} @if (isset($checkbox['checked']) === true && $checkbox['checked'] === true) hidden @endif>{!! $label_inactive !!}</label>
				<input class="uk-checkbox uk-margin-small-left {{ $checkbox['class'] or '' }}" uk-toggle="target:[data-checkbox-target-{{ $id }}]" id="checkbox-target-{{ $id }}" type="checkbox" name="{{ $checkbox['name'] or '' }}" value="{{ $checkbox['value'] or '' }}" {{ isset($checkbox['disabled']) && $checkbox['disabled'] ? 'disabled' : '' }} {{ isset($checkbox['checked']) && $checkbox['checked'] ? 'checked' : '' }} {{ $checkbox['data'] or '' }}>
			</div>
		@endforeach
	</div>
@endif
