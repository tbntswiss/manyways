@define(
	$icon = isset($icon) && is_array($icon) ? $icon : [];
	$icons = isset($icons) && is_array($icons) ? $icons : [];

	array_unshift($icons, $icon);
)

{{--

icon => [
	icon => 'ionicon class',
	text => '<span> text',
	class => '<i> class',
	data => '<i> data',
	link => '<i> is link',
	count => '<i> count',
]

icons => [
	icon,
	...,
]

--}}

@if (count(array_collapse($icons)))
	<div class="card-input-icons uk-width-1-1 uk-width-auto@s uk-height-1-1 uk-flex uk-flex-middle uk-flex-right uk-flex-first uk-flex-last@s">
		@foreach ($icons as $icon)
			@continue (count($icon) === 0)

			@if (isset($icon['text']))
				<span class="uk-padding-small-left uk-text-small {{ $icon['class'] or '' }}">{{ $icon['text'] or '' }}</span>
			@endif

			<i class="ionicon uk-padding-small-right uk-padding-tiny-left uk-padding-tiny-vertical uk-position-relative {{ $icon['icon'] or '' }} {{ $icon['class'] or '' }} {{ isset($icon['link']) && !$icon['link'] ? '' : 'uk-icon-link' }}" @if (isset($icon['icon'])) @endif {{ $icon['data'] or '' }}>
				@if (isset($icon['count']))
					<span class="uk-position-cover uk-text-small uk-flex uk-flex-middle uk-flex-center uk-text-bold">{{ $icon['count'] }}</span>
				@endif
			</i>
		@endforeach
	</div>
@endif
