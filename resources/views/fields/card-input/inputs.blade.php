@define(
	$input = isset($input) && is_array($input) ? $input : [];
	$inputs = isset($inputs) && is_array($inputs) ? $inputs : [];

	array_unshift($inputs, $input);
)

{{--

input => [
	text => '<span> or <a> text || display <input> or <textarea>',
	link => '<a> link and display <a> instead of <span>',
	target => '<a> target',
	pjax => '<a> is pjax',
	textarea => '<textarea> value || display <input>',
	max => '<textarea> maxlength',
	value => '<input> value',
	type => '<input> type || text',
	name => 'tag name',
	placeholder => '<input> or <textarea> placeholder',
	readonly => '<input> or <textarea> readonly',
	class => 'tag class',
	data => 'tag data',
	width => 'tag parent width',
]

inputs => [
	input,
	...,
]

--}}

@if (count(array_collapse($inputs)))
	@foreach ($inputs as $input)
		@continue (count($input) === 0)

		<div class="card-input-text uk-position-relative uk-width-1-1 uk-width-{{ $input['width'] or 'expand' }}@s">
			@if (isset($input['text']))
				@if (isset($input['link']))
					<a {{ isset($input['pjax']) && $input['pjax'] ? 'pjax' : '' }} href="{{ $input['link'] }}" class="uk-inline-block uk-margin-tiny-vertical {{ $input['class'] or '' }}"  name="{{ $input['name'] or '' }}" {{ $input['data'] or '' }} target="{{ $input['target'] or '' }}">{!! $input['text'] or '' !!}</a>
				@else
					<span class="uk-inline-block uk-margin-tiny-vertical {{ $input['class'] or '' }}"  name="{{ $input['name'] or '' }}" {{ $input['data'] or '' }}>{!! $input['text'] or '' !!}</span>
				@endif
			@else
				@if (isset($input['readonly']) && $input['readonly'])
					<div class="uk-position-cover"></div>
				@endif

				@if (isset($input['textarea']))
					<textarea class="uk-textarea uk-padding-remove-horizontal uk-border-none uk-resize-vertical {{ $input['class'] or '' }}" name="{{ $input['name'] or '' }}" placeholder="{{ $input['placeholder'] or '' }}" {{ $input['data'] or '' }} maxlength="{{ $input['max'] or '' }}">{!! $input['textarea'] or '' !!}</textarea>
				@elseif (isset($input['select']))
					<select class="uk-select uk-padding-remove uk-border-none {{ $input['class'] or '' }}" name="{{ $input['name'] or '' }}" {{ $input['data'] or '' }}>
						@foreach ($input['select'] as $value => $option)
							<option value="{{ $value }}" {{ ($input['value'] ?? 0) == $value ? 'selected' : '' }}>{{ $option }}</option>
						@endforeach
					</select>
				@else
					<input class="uk-input uk-padding-remove uk-border-none {{ $input['class'] or '' }}" type="{{ $input['type'] or 'text' }}" name="{{ $input['name'] or '' }}" value="{{ $input['value'] or '' }}" placeholder="{{ $input['placeholder'] or '' }}" {{ $input['data'] or '' }}>
				@endif
			@endif
		</div>
	@endforeach
@endif
