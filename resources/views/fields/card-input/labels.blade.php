@define(
	$label = isset($label) && is_array($label) ? $label : [];
	$labels = isset($labels) && is_array($labels) ? $labels : [];

	array_unshift($labels, $label);
)

{{--

label => [
	text => '<span> text',
	class => '<span> class',
	data => '<span> data',
]

labels => [
	label,
	...,
]

--}}

@if (count(array_collapse($labels)))
	<div class="card-input-labels uk-width-1-1 uk-width-auto@s uk-text-right">
		@foreach ($labels as $label)
			@continue (count($label) === 0)

			<span class="uk-label {{ $label['class'] or '' }}" {{ $label['data'] or '' }}>
				{{ $label['text'] or '' }}
			</span>
		@endforeach
	</div>
@endif
