@define(
	$item = isset($item) && is_array($item) ? $item : [];
	$items = isset($items) && is_array($items) ? $items : [];
	$dropdown = isset($dropdown) && is_array($dropdown) ? $dropdown : [];

	array_unshift($items, $item);
)

{{--

dropdown => [
	position => 'dropdown position',
	data => 'dropdown data',
]

item => [
	label => [
		text => '<label> text',
		class => '<label> class',
		data => '<label> data',
	],
	input => [
		id => '<input> id',
		value => '<input> value',
		name => '<input> name',
		class => '<input> class',
		data => '<input> data',
		checked => '<input> is checked',
		disabled => '<input> is disabled',
	],
]

items => [
	item,
	...,
]

--}}

<div uk-dropdown="mode:click; {{ isset($dropdown['position']) ? 'pos:'.$dropdown['position'].';' : '' }}" {{ $dropdown['data'] or '' }}>
	<ul class="uk-nav uk-dropdown-nav">
		@foreach ($items as $item)
			@continue (count($item) === 0)

			@define(
				$label = isset($item['label']) && is_array($item['label']) ? $item['label'] : [];
				$checkbox = isset($item['input']) && is_array($item['input']) ? $item['input'] : [];

				$id = $checkbox['id'] ?? str_replace(['[', ']'], '', ($checkbox['name'] ?? '').'-'.($checkbox['value'] ?? ''));
			)

			<li>
				<div class="uk-flex uk-flex-middle">
					@if (count($checkbox) > 0)
						<div class="uk-flex uk-flex-middle uk-margin-small-right">
							<input class="uk-checkbox {{ $checkbox['class'] or '' }}" id="checkbox-filter-target-{{ $id }}" type="checkbox" name="{{ $checkbox['name'] or '' }}" value="{{ $checkbox['value'] or '' }}" {{ isset($checkbox['disabled']) && $checkbox['disabled'] ? 'disabled' : '' }} {{ isset($checkbox['checked']) && $checkbox['checked'] ? 'checked' : '' }} {{ $checkbox['data'] or '' }}>
						</div>
					@endif

					<label class="uk-width-1-1 uk-padding-tiny-vertical {{ $label['class'] or '' }}" for="checkbox-filter-target-{{ $id }}" {{ $label['data'] or '' }}>{{ $label['text'] or 'Active' }}</label>
				</div>
			</li>
		@endforeach
	</ul>
</div>
