@define(
	$image = $image ?? null;
	$name = $name ?? 'NO_NAME';
	$data = $data ?? '';
	$remove = $remove ?? true;
)

<div class="uk-placeholder uk-padding-small uk-margin-bottom" data-error-after @if ($image !== null) hidden @endif @if ($data !== '') data-file="{{ $data }}" @endif>
	<div class="uk-flex uk-flex-middle uk-flex-center" uk-form-custom>
		<input type="file" name="{{ $name }}">
		<i class="ionicon ion-upload uk-margin-tiny-right uk-text-primary"></i>
		<span class="uk-link">Upload</span>
	</div>
</div>
<div class="uk-inline uk-transition-toggle" @if ($image === null) hidden @endif data-file-target="{{ $data }}">
	@if ($image !== null)
		<img src="{{ $dir_thumbs($image->filename) }}">
	@endif

	@if ($remove === true)
		<div class="uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle uk-transition-fade">
			<button type="button" class="uk-button uk-button-danger" data-file-remove>Remove</button>
		</div>
	@endif
</div>
