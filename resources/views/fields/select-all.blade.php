@define(
	$box = isset($box) && is_array($box) ? $box : [];
	$label = isset($label) && is_array($label) ? $label : [];
	$input = isset($input) && is_array($input) ? $input : [];
)

{{--

box => [
	class => '<div> class',
	data => '<div> data',
],
label => [
	text => '<label> text',
	class => '<label> class',
	data => '<label> data',
],
input => [
	name => 'select all name',
	class => '<input> class',
	data => '<input> data',
	checked => '<input> is checked',
	disabled => '<input> is disabled',
],

--}}

<div class="uk-flex uk-flex-middle {{ $box['class'] or '' }}" {{ $box['data'] or '' }}>
	<input class="uk-checkbox {{ $input['class'] or '' }}" id="checkbox-filter-target-{{ $input['name'] or '' }}" type="checkbox" data-select-all="{{ $input['name'] or '' }}" {{ isset($input['disabled']) && $input['disabled'] ? 'disabled' : '' }} {{ isset($input['checked']) && $input['checked'] ? 'checked' : '' }} {{ $input['data'] or '' }}>
	<label class="uk-margin-small-left {{ $label['class'] or '' }}" for="checkbox-filter-target-{{ $input['name'] or '' }}" {{ $label['data'] or '' }}>{{ $label['text'] or '' }}</label>
</div>
