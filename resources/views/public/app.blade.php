<!DOCTYPE html>
<html class="noscroll">
<head>
	<base href="{{ $app_url }}/" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="HandheldFriendly" content="true">
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="en">

	{{-- FAVICON --}}
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{{ url('public/images/favicon/manifest.json') }}">
	<meta name="theme-color" content="#ffffff">

	{{-- META --}}
	<title>{{ $social['title'] ?? trans('app.meta_title') }}</title>
	<meta name="description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />

	{{-- CSS --}}
	<link rel="stylesheet" href="public/app/styles/vendor.css?_={{ date('U') }}" />
	<link rel="stylesheet" href="https://assets.juicer.io/embed.css" media="all" type="text/css" />

	{{-- TWITTER --}}
	<meta name="twitter:title" content="{{ $social['title'] ?? trans('app.meta_title') }}" />
	<meta name="twitter:description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />
	<meta name="twitter:image:src" content="{{ $social['image']['twitter'] ?? $social['image']['app'] ?? url('public/images/share/twitter-share.png') }}" />
	<meta name="twitter:card" content="summary_large_image" />
	{{-- <meta name="twitter:site" content="{{ env('TWITTER_ACCOUNT') }}" /> --}}
	{{-- <meta name="twitter:creator" content="{{ env('TWITTER_ACCOUNT') }}" /> --}}

	{{-- FACEBOOK --}}
	<meta property="og:locale" content="{{ $app_lang->locale }}" />
	<meta property="og:title" content="{{ $social['title'] ?? trans('app.meta_title') }}" />
	<meta property="og:description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />
	<meta property="og:image:width" content="880" />
	<meta property="og:image:height" content="280" />
	<meta property="og:image" itemprop="image" content="{{ $social['image']['facebook'] ?? $social['image']['app'] ?? url('public/images/share/facebook-share.png') }}" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="{{ env('APP_NAME') }}" />

	{{-- SCRIPTS --}}
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-791102522"></script>
	<script>
		// GA
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-34456424-5', 'auto');
		ga('send', 'pageview');

		// Global site tag
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'AW-791102522');

		// Facebook Pixel Code
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1773620589626742'); // Insert your pixel ID here.
		fbq('track', 'PageView');

		@include('public.includes.js')
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1773620589626742&ev=PageView&noscript=1"/>
	</noscript>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYjniKo9jhXftJp0NPn3jHJ2wJF7aulHY"></script>
	{{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
	<script src="https://assets.juicer.io/embed.js" type="text/javascript"></script>
</head>
<body>
	<!-- [if lte IE 10]>
		@include('public.modules.browsehappy')
	<![endif] -->

	<div class="loader">
		<h2>{!! trans('pages/home.slider-tagline') !!}</h2>
		<div class="bar">
			<span class="label">{!! trans('pages/home.loader') !!}</span>
		</div>
	</div>

	@include('public.includes.menu')
	@include('public.includes.banner')
	<div id="top"></div>

	<main id="app">
		@yield('content')

		<script>
			window.Laravel.js = "{{ $js ?? '' }}";
			window.Laravel.menu = "{{ $menu ?? '' }}";
		</script>
	</main>

	<a class='gotop'>
		<img src="public/images/icons/blue/up.svg" alt="go top">
	</a>

 	<script src="public/app/scripts/vendor.js?_={{ date('U') }}"></script>
</body>
</html>
