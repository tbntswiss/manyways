@extends('public.app', [
'js' => 'confirm',
'page_langs' => [
	'url' => 'urls.confirm'
],
'social' => [
	'title' => trans('pages/contact.c_meta_title'),
	'description' => trans('pages/contact.c_meta_desc'),
],
])

@section('content')
<div id="top" class="wrapper--fluid hero--header hero--header--smaller confirm--header" data-hero="{{ trans('pages/contact.data_hero_confirm') }}">
	<h1>{!! trans('pages/contact.confirm_title') !!}</h1>
	@if(Lang::has('pages/contact.hero_infos_confirm', 'en'))
		<div class="header--front">
			<img class="fo" src="{{ trans('pages/contact.hero_infos_confirm.fo_url') }}" alt="" data-width="{{ trans('pages/contact.hero_infos_confirm.fo_width') }}" data-x="{{ trans('pages/contact.hero_infos_confirm.fo_x') }}" data-y="{{ trans('pages/contact.hero_infos_confirm.fo_y') }}">
			<img class="fs" src="{{ trans('pages/contact.hero_infos_confirm.fs_url') }}" alt="" data-width="{{ trans('pages/contact.hero_infos_confirm.fs_width') }}" data-x="{{ trans('pages/contact.hero_infos_confirm.fs_x') }}" data-y="{{ trans('pages/contact.hero_infos_confirm.fs_y') }}">
		</div>
		<div class="header--back">
			<img class="bo" src="{{ trans('pages/contact.hero_infos_confirm.bo_url') }}" alt="" data-width="{{ trans('pages/contact.hero_infos_confirm.bo_width') }}" data-x="{{ trans('pages/contact.hero_infos_confirm.bo_x') }}" data-y="{{ trans('pages/contact.hero_infos_confirm.bo_y') }}">
			<img class="bs" src="{{ trans('pages/contact.hero_infos_confirm.bs_url') }}" alt="" data-width="{{ trans('pages/contact.hero_infos_confirm.bs_width') }}" data-x="{{ trans('pages/contact.hero_infos_confirm.bs_x') }}" data-y="{{ trans('pages/contact.hero_infos_confirm.bs_y') }}">
		</div>
	@endif
	<div class="bottom-stuff">
		<div class="grid col-s-6 part border">
			<p>{!! trans('pages/contact.confirm_txt') !!}</p>
		</div>
		<div class="grid col-s-4 part">
			@define(
				$last_url = \App\Project\SuccessStory::lastUrl($app_lang->id);
			)

			@if ($last_url !== '')
				<a pjax href="{{ trans('urls.success_stories').'/'.$last_url }}">{!! trans('pages/contact.confirm_story') !!}</a>
			@else
				<a pjax href="{{ trans('urls.home') }}">{!! trans('modules/navigation.home') !!}</a>
			@endif
		</div>
	</div>
</div>

@endsection
