@extends('public.app', [
'js' => 'contact',
'page_langs' => [
	'url' => 'urls.contact'
],
'social' => [
	'title' => trans('pages/contact.meta_title'),
	'description' => trans('pages/contact.meta_desc'),
],
])

@section('content')
<div id="top" class="wrapper--fluid hero--header contact--header" data-hero="{{trans('pages/contact.data_hero')}}">
	<h1>{!! trans('pages/contact.title') !!}</h1>
	@if(Lang::has('pages/contact.hero_infos', 'en'))
		<div class="header--front">
			<img class="fo" src="{{trans('pages/contact.hero_infos.fo_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.fo_width')}}" data-x="{{trans('pages/contact.hero_infos.fo_x')}}" data-y="{{trans('pages/contact.hero_infos.fo_y')}}">
			<img class="fs" src="{{trans('pages/contact.hero_infos.fs_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.fs_width')}}" data-x="{{trans('pages/contact.hero_infos.fs_x')}}" data-y="{{trans('pages/contact.hero_infos.fs_y')}}">
		</div>
		<div class="header--back">
			<img class="bo" src="{{trans('pages/contact.hero_infos.bo_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.bo_width')}}" data-x="{{trans('pages/contact.hero_infos.bo_x')}}" data-y="{{trans('pages/contact.hero_infos.bo_y')}}">
			<img class="bs" src="{{trans('pages/contact.hero_infos.bs_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.bs_width')}}" data-x="{{trans('pages/contact.hero_infos.bs_x')}}" data-y="{{trans('pages/contact.hero_infos.bs_y')}}">
		</div>
	@endif
</div>

<div id="contact--about" class="block--superposition contact--about">
	<h2>{!! trans('pages/contact.contact_about') !!}</h2>
</div>

<div class="infos-error-form">

</div>

@include('public.modules.form')
@include('public.modules.contactinfo')
@include('public.modules.footer')


@endsection
