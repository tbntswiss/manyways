@extends('public.app', [
'page_langs' => [
	'url' => 'urls.forgotten_password'
],
'social' => [
	'title' => trans('pages/forgotten-password.meta_title'),
	'description' => trans('pages/forgotten-password.meta_desc'),
],
])

@section('content')
<div id="page-forgotten-password" class="uk-container uk-container-small">
	<div class="uk-section uk-width-xlarge@s uk-margin-auto">
		<h1 class="uk-margin-medium">{{ trans('pages/forgotten-password.title') }}</h1>

		<form method="post">
			{{ csrf_field() }}

			@if ($errors->any())
				<div class="uk-alert-danger" uk-alert>
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif

			@if (session('success') !== null)
				<div class="uk-alert-success" uk-alert>
					<p>{!! trans('pages/forgotten-password.success', ['email' => session('success')]) !!}</p>
				</div>
			@endif

			<div class="uk-margin">
				<input class="uk-input" type="text" name="email" placeholder="{{ trans('pages/forgotten-password.placeh_email') }}">
			</div>

			<div class="uk-margin uk-flex uk-flex-middle uk-flex-between">
				<a href="{{ trans('urls.login') }}" class="uk-button uk-button-text">{{ trans('pages/forgotten-password.btn_login') }}</a>
				<button type="submit" class="uk-button uk-button-primary">{{ trans('pages/forgotten-password.btn_send') }}</button>
			</div>
		</form>
	</div>
</div>
@endsection
