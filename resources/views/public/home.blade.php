@extends('public.app', [
'js' => 'home',
'page_langs' => [
	'url' => 'urls.home'
],
'social' => [
	'title' => trans('pages/home.meta_title'),
	'description' => trans('pages/home.meta_desc'),
],
])

@section('content')
<div class="home-controls">
	<span class="prev unselect"><img src="public/images/icons/blue/left.svg" alt="previous"></span>
	<span class="next"><img src="public/images/icons/blue/right.svg" alt="next"></span>
</div>
<div class="forceScroll"></div>
<div id="home--slider" class="wrapper--fluid home--slider" data-bg="#ffffff">
	<div class="section s-0">
		<h1>{!! trans('pages/home.slider-tagline') !!}</h1>
	</div>
	<div class="section hero--header s-1" data-hero=true data-bg="{{trans('pages/home.microfibre.bg')}}">
		<h1 class="slider">{!! trans('pages/home.microfibre.title') !!}</h1>
		<div class="header--front" data-slider='microfibre'>
			<img class="fo" src="{{trans('pages/home.microfibre.fo_url')}}" alt="" data-width="{{trans('pages/home.microfibre.fo_width')}}" data-x="{{trans('pages/home.microfibre.fo_x')}}" data-y="{{trans('pages/home.microfibre.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.microfibre.fs_url')}}" alt="" data-width="{{trans('pages/home.microfibre.fs_width')}}" data-x="{{trans('pages/home.microfibre.fs_x')}}" data-y="{{trans('pages/home.microfibre.fs_y')}}">
		</div>
		<div class="header--back" data-slider='microfibre'>
			<img class="bo" src="{{trans('pages/home.microfibre.bo_url')}}" alt="" data-width="{{trans('pages/home.microfibre.bo_width')}}" data-x="{{trans('pages/home.microfibre.bo_x')}}" data-y="{{trans('pages/home.microfibre.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.microfibre.bs_url')}}" alt="" data-width="{{trans('pages/home.microfibre.bs_width')}}" data-x="{{trans('pages/home.microfibre.bs_x')}}" data-y="{{trans('pages/home.microfibre.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-2" data-hero=true data-bg="{{trans('pages/home.polos.bg')}}">
		<h1 class="slider">{!! trans('pages/home.polos.title') !!}</h1>
		<div class="header--front" data-slider='polos'>
			<img class="fo" src="{{trans('pages/home.polos.fo_url')}}" alt="" data-width="{{trans('pages/home.polos.fo_width')}}" data-x="{{trans('pages/home.polos.fo_x')}}" data-y="{{trans('pages/home.polos.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.polos.fs_url')}}" alt="" data-width="{{trans('pages/home.polos.fs_width')}}" data-x="{{trans('pages/home.polos.fs_x')}}" data-y="{{trans('pages/home.polos.fs_y')}}">
		</div>
		<div class="header--back" data-slider='polos'>
			<img class="bo" src="{{trans('pages/home.polos.bo_url')}}" alt="" data-width="{{trans('pages/home.polos.bo_width')}}" data-x="{{trans('pages/home.polos.bo_x')}}" data-y="{{trans('pages/home.polos.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.polos.bs_url')}}" alt="" data-width="{{trans('pages/home.polos.bs_width')}}" data-x="{{trans('pages/home.polos.bs_x')}}" data-y="{{trans('pages/home.polos.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-3" data-hero=true data-bg="{{trans('pages/home.tee.bg')}}">
		<h1 class="slider">{!! trans('pages/home.tee.title') !!}</h1>
		<div class="header--front" data-slider='tee'>
			<img class="fo" src="{{trans('pages/home.tee.fo_url')}}" alt="" data-width="{{trans('pages/home.tee.fo_width')}}" data-x="{{trans('pages/home.tee.fo_x')}}" data-y="{{trans('pages/home.tee.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.tee.fs_url')}}" alt="" data-width="{{trans('pages/home.tee.fs_width')}}" data-x="{{trans('pages/home.tee.fs_x')}}" data-y="{{trans('pages/home.tee.fs_y')}}">
		</div>
		<div class="header--back" data-slider='tee'>
			<img class="bo" src="{{trans('pages/home.tee.bo_url')}}" alt="" data-width="{{trans('pages/home.tee.bo_width')}}" data-x="{{trans('pages/home.tee.bo_x')}}" data-y="{{trans('pages/home.tee.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.tee.bs_url')}}" alt="" data-width="{{trans('pages/home.tee.bs_width')}}" data-x="{{trans('pages/home.tee.bs_x')}}" data-y="{{trans('pages/home.tee.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-4" data-hero=true data-bg="{{trans('pages/home.parapluies.bg')}}">
		<h1 class="slider">{!! trans('pages/home.parapluies.title') !!}</h1>
		<div class="header--front" data-slider='parapluies'>
			<img class="fo" src="{{trans('pages/home.parapluies.fo_url')}}" alt="" data-width="{{trans('pages/home.parapluies.fo_width')}}" data-x="{{trans('pages/home.parapluies.fo_x')}}" data-y="{{trans('pages/home.parapluies.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.parapluies.fs_url')}}" alt="" data-width="{{trans('pages/home.parapluies.fs_width')}}" data-x="{{trans('pages/home.parapluies.fs_x')}}" data-y="{{trans('pages/home.parapluies.fs_y')}}">
		</div>
		<div class="header--back" data-slider='parapluies'>
			<img class="bo" src="{{trans('pages/home.parapluies.bo_url')}}" alt="" data-width="{{trans('pages/home.parapluies.bo_width')}}" data-x="{{trans('pages/home.parapluies.bo_x')}}" data-y="{{trans('pages/home.parapluies.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.parapluies.bs_url')}}" alt="" data-width="{{trans('pages/home.parapluies.bs_width')}}" data-x="{{trans('pages/home.parapluies.bs_x')}}" data-y="{{trans('pages/home.parapluies.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-5" data-hero=true data-bg="{{trans('pages/home.doudounes.bg')}}">
		<h1 class="slider">{!! trans('pages/home.doudounes.title') !!}</h1>
		<div class="header--front" data-slider='doudounes'>
			<img class="fo" src="{{trans('pages/home.doudounes.fo_url')}}" alt="" data-width="{{trans('pages/home.doudounes.fo_width')}}" data-x="{{trans('pages/home.doudounes.fo_x')}}" data-y="{{trans('pages/home.doudounes.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.doudounes.fs_url')}}" alt="" data-width="{{trans('pages/home.doudounes.fs_width')}}" data-x="{{trans('pages/home.doudounes.fs_x')}}" data-y="{{trans('pages/home.doudounes.fs_y')}}">
		</div>
		<div class="header--back" data-slider='doudounes'>
			<img class="bo" src="{{trans('pages/home.doudounes.bo_url')}}" alt="" data-width="{{trans('pages/home.doudounes.bo_width')}}" data-x="{{trans('pages/home.doudounes.bo_x')}}" data-y="{{trans('pages/home.doudounes.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.doudounes.bs_url')}}" alt="" data-width="{{trans('pages/home.doudounes.bs_width')}}" data-x="{{trans('pages/home.doudounes.bs_x')}}" data-y="{{trans('pages/home.doudounes.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-6" data-hero=true data-bg="{{trans('pages/home.softshells.bg')}}">
		<h1 class="slider">{!! trans('pages/home.softshells.title') !!}</h1>
		<div class="header--front" data-slider='softshells'>
			<img class="fo" src="{{trans('pages/home.softshells.fo_url')}}" alt="" data-width="{{trans('pages/home.softshells.fo_width')}}" data-x="{{trans('pages/home.softshells.fo_x')}}" data-y="{{trans('pages/home.softshells.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.softshells.fs_url')}}" alt="" data-width="{{trans('pages/home.softshells.fs_width')}}" data-x="{{trans('pages/home.softshells.fs_x')}}" data-y="{{trans('pages/home.softshells.fs_y')}}">
		</div>
		<div class="header--back" data-slider='softshells'>
			<img class="bo" src="{{trans('pages/home.softshells.bo_url')}}" alt="" data-width="{{trans('pages/home.softshells.bo_width')}}" data-x="{{trans('pages/home.softshells.bo_x')}}" data-y="{{trans('pages/home.softshells.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.softshells.bs_url')}}" alt="" data-width="{{trans('pages/home.softshells.bs_width')}}" data-x="{{trans('pages/home.softshells.bs_x')}}" data-y="{{trans('pages/home.softshells.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-7" data-hero=true data-bg="{{trans('pages/home.bonnets.bg')}}">
		<h1 class="slider">{!! trans('pages/home.bonnets.title') !!}</h1>
		<div class="header--front" data-slider='bonnets'>
			<img class="fo" src="{{trans('pages/home.bonnets.fo_url')}}" alt="" data-width="{{trans('pages/home.bonnets.fo_width')}}" data-x="{{trans('pages/home.bonnets.fo_x')}}" data-y="{{trans('pages/home.bonnets.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.bonnets.fs_url')}}" alt="" data-width="{{trans('pages/home.bonnets.fs_width')}}" data-x="{{trans('pages/home.bonnets.fs_x')}}" data-y="{{trans('pages/home.bonnets.fs_y')}}">
		</div>
		<div class="header--back" data-slider='bonnets'>
			<img class="bo" src="{{trans('pages/home.bonnets.bo_url')}}" alt="" data-width="{{trans('pages/home.bonnets.bo_width')}}" data-x="{{trans('pages/home.bonnets.bo_x')}}" data-y="{{trans('pages/home.bonnets.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.bonnets.bs_url')}}" alt="" data-width="{{trans('pages/home.bonnets.bs_width')}}" data-x="{{trans('pages/home.bonnets.bs_x')}}" data-y="{{trans('pages/home.bonnets.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-8" data-hero=true data-bg="{{trans('pages/home.cap.bg')}}">
		<h1 class="slider">{!! trans('pages/home.cap.title') !!}</h1>
		<div class="header--front" data-slider='cap'>
			<img class="fo" src="{{trans('pages/home.cap.fo_url')}}" alt="" data-width="{{trans('pages/home.cap.fo_width')}}" data-x="{{trans('pages/home.cap.fo_x')}}" data-y="{{trans('pages/home.cap.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.cap.fs_url')}}" alt="" data-width="{{trans('pages/home.cap.fs_width')}}" data-x="{{trans('pages/home.cap.fs_x')}}" data-y="{{trans('pages/home.cap.fs_y')}}">
		</div>
		<div class="header--back" data-slider='cap'>
			<img class="bo" src="{{trans('pages/home.cap.bo_url')}}" alt="" data-width="{{trans('pages/home.cap.bo_width')}}" data-x="{{trans('pages/home.cap.bo_x')}}" data-y="{{trans('pages/home.cap.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.cap.bs_url')}}" alt="" data-width="{{trans('pages/home.cap.bs_width')}}" data-x="{{trans('pages/home.cap.bs_x')}}" data-y="{{trans('pages/home.cap.bs_y')}}">
		</div>
	</div>
	<div class="section hero--header s-9" data-hero=true data-bg="{{trans('pages/home.sweatshirt.bg')}}">
		<h1 class="slider">{!! trans('pages/home.sweatshirt.title') !!}</h1>
		<div class="header--front" data-slider='sweatshirt'>
			<img class="fo" src="{{trans('pages/home.sweatshirt.fo_url')}}" alt="" data-width="{{trans('pages/home.sweatshirt.fo_width')}}" data-x="{{trans('pages/home.sweatshirt.fo_x')}}" data-y="{{trans('pages/home.sweatshirt.fo_y')}}">
			<img class="fs" src="{{trans('pages/home.sweatshirt.fs_url')}}" alt="" data-width="{{trans('pages/home.sweatshirt.fs_width')}}" data-x="{{trans('pages/home.sweatshirt.fs_x')}}" data-y="{{trans('pages/home.sweatshirt.fs_y')}}">
		</div>
		<div class="header--back" data-slider='sweatshirt'>
			<img class="bo" src="{{trans('pages/home.sweatshirt.bo_url')}}" alt="" data-width="{{trans('pages/home.sweatshirt.bo_width')}}" data-x="{{trans('pages/home.sweatshirt.bo_x')}}" data-y="{{trans('pages/home.sweatshirt.bo_y')}}">
			<img class="bs" src="{{trans('pages/home.sweatshirt.bs_url')}}" alt="" data-width="{{trans('pages/home.sweatshirt.bs_width')}}" data-x="{{trans('pages/home.sweatshirt.bs_x')}}" data-y="{{trans('pages/home.sweatshirt.bs_y')}}">
		</div>
	</div>
	<div class="section s-10">
	</div>
</div>

<div class="slider-bg"></div>

<div class="bottom-slider-bounds">
	<div class="bottom-slider">
		<li class='s-0' data-slider='1'></li>
		<li class='s-1' data-slider='2'>{!! trans('pages/home.microfibre.title') !!}</li>
		<li class='s-2' data-slider='3'>{!! trans('pages/home.polos.title') !!}</li>
		<li class='s-3' data-slider='4'>{!! trans('pages/home.tee.title') !!}</li>
		<li class='s-4' data-slider='5'>{!! trans('pages/home.parapluies.title') !!}</li>
		<li class='s-5' data-slider='6'>{!! trans('pages/home.doudounes.title') !!}</li>
		<li class='s-6' data-slider='7'>{!! trans('pages/home.softshells.title') !!}</li>
		<li class='s-7' data-slider='8'>{!! trans('pages/home.bonnets.title') !!}</li>
		<li class='s-8' data-slider='9'>{!! trans('pages/home.cap.title') !!}</li>
		<li class='s-9' data-slider='10'>{!! trans('pages/home.sweatshirt.title') !!}</li>
	</div>
</div>
<div class="bottom-infos">
	<span class="label">{!! trans('pages/home.slider_nav') !!}</span>
</div>

<div class="mobile-scroll"></div>

<img src="public/images/icons/nav.gif" alt="" class="bottom-img-infos">

<div id="home--about" class="wrapper--fluid home--about wow fadeIn">
	<div class="indic" data-start='top:-20px; opacity:1;' data-150-start='top:0px; opacity:0;'></div>
	<div class="wrapper">
		<h3>{!! trans('pages/home.about') !!}</h3>
	</div>
</div>

@include('public.modules.process')
@include('public.modules.stories')
@include('public.modules.references')
@include('public.modules.contact')
@include('public.modules.footer')

@endsection
