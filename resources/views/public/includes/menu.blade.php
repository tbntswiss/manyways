<nav class="wrapper navbar" uk-navbar data-start="top:15vh;" data-100-start="top:0;">

	<div class="is-mobile menu--toggle">

	</div>

	<div class="navbar--item navbar--logo">
		<a pjax href="{{ trans('urls.home') }}" class="navbar--item__link">
			<img src='public/images/logo/logo.svg'>
		</a>
	</div>

	<!--Nav items-->
	<div class="navbar--item margin-top">
		<a pjax href="{{ trans('urls.home') }}" class="navbar--item__link navhover">{!! trans('modules/navigation.home') !!}</a>
	</div>
	<div class="navbar--item">
		<a pjax href="{{ trans('urls.home') }}#success-stories" class="navbar--item__link navhover">{!! trans('modules/navigation.stories') !!}</a>
	</div>
	<div class="navbar--item">
		<a pjax href="{{ trans('urls.products') }}" class="navbar--item__link navhover">{!! trans('modules/navigation.products') !!}</a>
	</div>
	<div class="navbar--item blue">
		<a pjax href="{{ trans('urls.contact') }}" class="navbar--item__link navhover">{!! trans('modules/navigation.contact') !!}</a>
	</div>


	<div class="is-mobile menu--bottom--infos">
		<div class="contact">
			<a href="mailto:{{trans('modules/footer.mail')}}" class="text-link contact--item__cta">{!! trans('modules/footer.mail') !!}</a>
			<a href="tel:{{trans('modules/footer.phone')}}" class="text-link contact--item__cta">{!! trans('modules/footer.phone') !!}</a>
		</div>

				<div class="lang">
			@define(
				$page_langs = object_merge(['url' => 'urls.home'], $page_langs ?? [])
			)

			@foreach ($app_langs as $lang)
				@define(
					$is_active = $app_lang->id == $lang->id ? 'active' : ''
				)

				@define(
					$page_lang = Lang::get($page_langs->url, [], $lang->code)
				)

				@if (isset($page_langs->urls) === true)
					@define(
						$page_lang = $page_lang.'/'.$page_langs->urls->{$lang->code}
					)
				@endif

				<a class="label-lang {{ $is_active }}" href="{{ $page_lang }}" data-lang-disabled>{{ strtoupper($lang->code) }}</a>
			@endforeach
		</div>
	</div>


</nav>
