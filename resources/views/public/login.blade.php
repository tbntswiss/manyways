@extends('public.app', [
'page_langs' => [
	'url' => 'urls.login'
],
'social' => [
	'title' => trans('pages/login.meta_title'),
	'description' => trans('pages/login.meta_desc'),
],
])

@section('content')
<div id="page-login" class="uk-container uk-container-small">
	<div class="uk-section uk-width-large@s uk-margin-auto">
		<h1 class="uk-margin-medium">{{ trans('pages/login.title') }}</h1>

		<form method="post">
			{{ csrf_field() }}

			@if ($errors->any())
				<div class="uk-alert-danger" uk-alert>
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif

			<div class="uk-margin">
				<input class="uk-input" type="text" name="email" placeholder="{{ trans('pages/login.placeh_email') }}">
			</div>
			<div class="uk-margin">
				<input class="uk-input" type="password" name="password" placeholder="{{ trans('pages/login.placeh_password') }}">
			</div>

			<div class="uk-margin uk-flex uk-flex-middle uk-flex-between">
				<a href="{{ trans('urls.forgotten_password') }}" class="uk-button uk-button-text">{{ trans('pages/login.btn_forgotten') }}</a>
				<button type="submit" class="uk-button uk-button-primary">{{ trans('pages/login.btn_send') }}</button>
			</div>
		</form>
	</div>
</div>
@endsection
