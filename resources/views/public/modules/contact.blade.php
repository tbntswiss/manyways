<div class="wrapper home--contact">
	<h1 class="wow fadeIn">{!! trans('pages/home.contact_title') !!}</h1>

	<div class="block-inverted contact">
		<div class="block contact--item wow fadeIn">
			<img src="public/images/contact/online.svg" alt="icon" class="contact--item__img">
			<a pjax href="{{ trans('urls.contact') }}" class="button link-cta contact--item__cta">
				<div class="txt">{!! trans('pages/home.online') !!}</div>
				<span class="before"></span>
				<span class="after"></span>
			</a>
		</div>
		<div class="block contact--item wow fadeIn" data-900-top="transform: translate(-0px, -120px);" data--600-top="transform: translate(-0px, -40px);">
			<img src="public/images/contact/mail.svg" alt="icon" class="contact--item__img">
			<a href="mailto:{{trans('pages/home.mail')}}" class="text-link contact--item__cta">{!! trans('pages/home.mail') !!}</a>
			<a href="tel:{{trans('pages/home.phone')}}" class="text-link contact--item__cta">{!! trans('pages/home.phone') !!}</a>
		</div>
	</div>
</div>
