<div class="wrapper--fluid contact--module home--process">
	<div class="block-default process">

		<div class="block process--item contact--infos wow fadeIn">
			<h2>{!! trans('pages/contact.agency_name') !!}</h2>
			<ul>
				<li>
					<img src="public/images/icons/grey/marker.svg" alt="icons">
					<a href="https://goo.gl/maps/VKUx9XE6K2n" target="_blank">{!! trans('pages/contact.street') !!}</a>
				</li>
				<li>
					<img src="public/images/icons/grey/tel.svg" alt="icons">
					<a href="tel:{{trans('pages/contact.phone')}}">{!! trans('pages/contact.phone') !!}</a>
				</li>
				<li>
					<img src="public/images/icons/grey/mail.svg" alt="icons">
					<a href="mailto:{{trans('pages/contact.mail')}}">{!! trans('pages/contact.mail') !!}</a>
				</li>
			</ul>
		</div>

		<div class="block process--item wow fadeIn">
			<a href="https://goo.gl/maps/VKUx9XE6K2n" target="_blank" class="tomaps">
				<div class="map" id="map"></div>
			</a>
		</div>

	</div>
</div>
