<footer class="wrapper-fluid footer">
	<div class="wrapper">
		<div class="grid col-xs-12 footer-column wow fadeIn">
			<a pjax href="{{ trans('urls.home') }}" class="footer--logo">
				<img src='public/images/logo/logo.svg'>
			</a>
			<div class="socials">
				<a href="mailto:{{trans('modules/footer.mail')}}"><img src="public/images/icons/blue/mail.svg" alt="socials"></a>
				<a target='_blank' href="https://www.instagram.com/manyways.sa/"><img src="public/images/icons/blue/insta.svg" alt="socials"></a>
				<a target='_blank' href="https://www.facebook.com/manyways.sa"><img src="public/images/icons/blue/facebook.svg" alt="socials"></a>
			</div>
		</div>

		<div class="grid col-m-3 footer--column wow fadeIn">
			<div class="footer--column__content">
				<span class="label">{!! trans('modules/footer.contact_title') !!}</span>
				<a href="mailto:{{trans('modules/footer.mail')}}" class="text-link contact--item__cta">{!! trans('modules/footer.mail') !!}</a>
				<a href="tel:{{trans('modules/footer.phone')}}" class="text-link contact--item__cta">{!! trans('modules/footer.phone') !!}</a>
			</div>
		</div>


		<div class="grid col-m-4 footer--column wow fadeIn">
			<div class="footer--column__content">
				<span class="label">{!! trans('modules/footer.eshop_title') !!}</span>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.11') }}">{!! trans('modules/footer.eshops.11') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.1') }}">{!! trans('modules/footer.eshops.1') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.2') }}">{!! trans('modules/footer.eshops.2') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.3') }}">{!! trans('modules/footer.eshops.3') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.4') }}">{!! trans('modules/footer.eshops.4') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.5') }}">{!! trans('modules/footer.eshops.5') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.6') }}">{!! trans('modules/footer.eshops.6') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.7') }}">{!! trans('modules/footer.eshops.7') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.8') }}">{!! trans('modules/footer.eshops.8') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.9') }}">{!! trans('modules/footer.eshops.9') !!}</a>
				<a class='eshop-link' target='_blank' href="{{ trans('modules/footer.links.10') }}">{!! trans('modules/footer.eshops.10') !!}</a>
			</div>
		</div>


		<div class="grid col-m-3 footer--column wow fadeIn">
			<div class="footer--column__content">
				<span class="label">{!! trans('modules/footer.newsletter_title') !!}</span>

				<div id="mc_embed_signup">
					{{-- <form action="//many-ways.us4.list-manage.com/subscribe/post?u=21309dabcafb00979ea751749&amp;id=da3629787b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> --}}
					<form action="//many-ways.us4.list-manage.com/subscribe/post?u=21309dabcafb00979ea751749&amp;id=fb7205d8b4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div id="mc_embed_signup_scroll">


							<div class="mc-field-group">
								<label for="mce-EMAIL">E-mail</label>
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
							</div>



							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>

							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_21309dabcafb00979ea751749_da3629787b" tabindex="-1" value=""></div>

							<div class="send"><input type="submit" value="{{trans('modules/footer.newsletter_cta')}}" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
						</div>
					</form>
				</div>

				<div class="lang">
					@define(
						$page_langs = object_merge(['url' => 'urls.home'], $page_langs ?? [])
					)

					@foreach ($app_langs as $lang)
						@define(
							$is_active = $app_lang->id == $lang->id ? 'active' : ''
						)

						@define(
							$page_lang = Lang::get($page_langs->url, [], $lang->code)
						)

						@if (isset($page_langs->urls) === true)
							@define(
								$page_lang = $page_lang.'/'.$page_langs->urls->{$lang->code}
							)
						@endif

						<a class="label-lang {{ $is_active }}" href="{{ $page_lang }}" data-lang-disabled>{{ strtoupper($lang->code) }}</a>
					@endforeach
				</div>
			</div>
		</div>

	</div>
</footer>
