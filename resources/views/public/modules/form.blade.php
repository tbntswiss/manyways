<div class="wrapper contact--form">
	<div id="mw_message">
		<div class="block-default block--flex">
			<div class="block wow fadeIn">
				<div class="input-container">
					<input type="text" class="input-container_input" name="surname" id="surname" required>
					<label for="surname" class="input-container_label">{!! trans('pages/contact.surname.txt') !!}</label>
					<span class="input-container_required">{!! trans('pages/contact.surname.on_error') !!}</span>
				</div>
				<div class="input-container">
					<input type="text" class="input-container_input" name="firstname" id="firstname" required>
					<label for="firstname" class="input-container_label">{!! trans('pages/contact.firstname.txt') !!}</label>
					<span class="input-container_required">{!! trans('pages/contact.firstname.on_error') !!}</span>
				</div>
				<div class="input-container">
					<input type="email" class="input-container_input" name="email" id="email" required>
					<label for="email" class="input-container_label">{!! trans('pages/contact.email.txt') !!}</label>
					<span class="input-container_required">{!! trans('pages/contact.email.on_error') !!}</span>
				</div>
				<div class="input-container">
					<input type="tel" class="input-container_input" name="tel" id="tel">
					<label for="tel" class="input-container_label">{!! trans('pages/contact.tel.txt') !!}</label>
					<span class="input-container_required">{!! trans('pages/contact.tel.on_error') !!}</span>
				</div>
			</div>

			<div class="block wow fadeIn">
				<div class="area-container">
					<textarea name="message" id="message" class="area-container_area" required></textarea>
					<label for="message" class="area-container_label">{!! trans('pages/contact.message.txt') !!}</label>
					<span class="area-container_required">{!! trans('pages/contact.message.on_error') !!}</span>
				</div>
			</div>
 			<div class="block wow fadeIn">
				{{-- <div id="g-recaptcha"></div> --}}
				<button class="button link-cta submit" data-url="{{ trans('urls.confirm') }}">
					<div class="txt">{!! trans('pages/contact.submit') !!}</div>
					<span class="before"></span>
					<span class="after"></span>
				</button>
			</div>
		</div>
	</div>
</div>
