<div class="header--front">
	<img class="fo" src="{{trans('pages/contact.hero_infos.fo_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.fo_width')}}" data-x="{{trans('pages/contact.hero_infos.fo_x')}}" data-y="{{trans('pages/contact.hero_infos.fo_y')}}">
	<img class="fs" src="{{trans('pages/contact.hero_infos.fs_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.fs_width')}}" data-x="{{trans('pages/contact.hero_infos.fs_x')}}" data-y="{{trans('pages/contact.hero_infos.fs_y')}}">
</div>
<div class="header--back">
	<img class="bo" src="{{trans('pages/contact.hero_infos.bo_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.bo_width')}}" data-x="{{trans('pages/contact.hero_infos.bo_x')}}" data-y="{{trans('pages/contact.hero_infos.bo_y')}}">
	<img class="bs" src="{{trans('pages/contact.hero_infos.bs_url')}}" alt="" data-width="{{trans('pages/contact.hero_infos.bs_width')}}" data-x="{{trans('pages/contact.hero_infos.bs_x')}}" data-y="{{trans('pages/contact.hero_infos.bs_y')}}">
</div>

