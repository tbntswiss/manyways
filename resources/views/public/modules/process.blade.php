<div class="wrapper--fluid home--process">
	<h1 class="wow fadeIn">{!! trans('pages/home.process_title') !!}</h1>
	<div class="block-default process skrollr-plugin-default">
		<div class="block process--item wow fadeIn">
			<span class="process--item__number">I.</span>
			<img src="public/images/process/1.svg" alt="icon" class="process--item__img">
			<p class="text-default process--item__txt">{!! trans('pages/home.process_step_1') !!}</p>
		</div>
		<div class="block process--item wow fadeIn">
			<span class="process--item__number">II.</span>
			<img src="public/images/process/2.svg" alt="icon" class="process--item__img">
			<p class="text-default process--item__txt">{!! trans('pages/home.process_step_2') !!}</p>
		</div>
		<div class="block process--item wow fadeIn">
			<span class="process--item__number">III.</span>
			<img src="public/images/process/3.svg" alt="icon" class="process--item__img">
			<p class="text-default process--item__txt">{!! trans('pages/home.process_step_3') !!}</p>
		</div>
		<div class="block process--item wow fadeIn">
			<span class="process--item__number">IV.</span>
			<img src="public/images/process/4.svg" alt="icon" class="process--item__img">
			<p class="text-default process--item__txt">{!! trans('pages/home.process_step_4') !!}</p>
		</div>
		<div class="block process--item wow fadeIn">
			<span class="process--item__number">V.</span>
			<img src="public/images/process/5.svg" alt="icon" class="process--item__img">
			<p class="text-default process--item__txt">{!! trans('pages/home.process_step_5') !!}</p>
		</div>
	</div>
</div>
