<div class="wrapper home--references">
	<h1 class="wow fadeIn">{!! trans('pages/home.references_title') !!}</h1>
	<p class="wow fadeIn">{!! trans('pages/home.references_sub') !!}</p>
	<ul>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_1') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_2') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_3') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_4') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_5') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_6') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_7') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_8') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_9') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_10') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_11') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_12') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_13') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_14') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_15') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_16') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_17') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_18') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_19') }}" alt="clientName"></li>
		<li><img class="wow fadeIn" src="{{ trans('pages/home.references_20') }}" alt="clientName"></li>
	</ul>
</div>
