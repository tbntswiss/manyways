<div class="wrapper--fluid home--stories" id='success-stories'>
	<h1 class="wow fadeIn">{!! trans('pages/home.stories_title') !!}</h1>

	<div class="block-inverted story--item wow fadeIn skrollr-plugin-inverted">
		@foreach ($success_stories as $success_story)
			<div class="block story--item__infos">
				<a pjax href="{{ trans('urls.success_stories').'/'.$success_story->langs->url }}">
					@if ($success_story->is_product == 0)
						<h2>{{ $success_story->client }}</h2>
					@else
						<h2>{{ $success_story->langs->title }}</h2>
					@endif

					<span class="read-more">{!! trans('pages/home.story_more') !!}</span>
					<img src="{{ $dir_thumbs($success_story->background->filename) }}" alt="case preview" class="story--item__img objectfit">
				</a>
			</div>
		@endforeach
	</div>

	<a pjax href="{{ trans('urls.products') }}" class="button link-cta contact--item__cta wow fadeIn">
		<div class="txt">{!! trans('pages/home.story_more_product') !!}</div>
		<span class="before"></span>
		<span class="after"></span>
	</a>
</div>
