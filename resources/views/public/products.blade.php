@extends('public.app', [
'js' => 'products',
'page_langs' => [
	'url' => 'urls.products'
],
'social' => [
	'title' => trans('pages/products.meta_title'),
	'description' => trans('pages/products.meta_desc'),
],
])

@section('content')
{{-- HERO PRODUCTS --}}
<div id="top" class="wrapper--fluid hero--header confirm--header" data-hero="{{trans('pages/products.data_hero')}}">
	<h1>{!! trans('pages/products.title') !!}</h1>
	@if(Lang::has('pages/products.hero_infos', 'en'))
		<div class="header--front">
			<img class="fo" src="{{trans('pages/products.hero_infos.fo_url')}}" alt="" data-width="{{trans('pages/products.hero_infos.fo_width')}}" data-x="{{trans('pages/products.hero_infos.fo_x')}}" data-y="{{trans('pages/products.hero_infos.fo_y')}}">
			<img class="fs" src="{{trans('pages/products.hero_infos.fs_url')}}" alt="" data-width="{{trans('pages/products.hero_infos.fs_width')}}" data-x="{{trans('pages/products.hero_infos.fs_x')}}" data-y="{{trans('pages/products.hero_infos.fs_y')}}">
		</div>
		<div class="header--back">
			<img class="bo" src="{{trans('pages/products.hero_infos.bo_url')}}" alt="" data-width="{{trans('pages/products.hero_infos.bo_width')}}" data-x="{{trans('pages/products.hero_infos.bo_x')}}" data-y="{{trans('pages/products.hero_infos.bo_y')}}">
			<img class="bs" src="{{trans('pages/products.hero_infos.bs_url')}}" alt="" data-width="{{trans('pages/products.hero_infos.bs_width')}}" data-x="{{trans('pages/products.hero_infos.bs_x')}}" data-y="{{trans('pages/products.hero_infos.bs_y')}}">
		</div>
	@endif
</div>

{{-- JUICER.IO --}}
<ul class="juicer-feed" data-feed-id="manyways-sa-f3db0f38-dbea-49b0-96ac-63363a237aaf" data-after="updateFilters()"></ul>
<script>function updateFilters() { $('.juicer-feed .feed-item').addClass('wow fadeIn'); }</script>

{{-- CONTACT --}}
<div class="cta--contact">
	<div class="content">
		<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_1') !!}</h2>
		<a pjax href="{{ trans('urls.contact') }}">
			<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_2') !!}</h2>
		</a>
		<h2 class="wow fadeIn">.</h2>
	</div>
</div>

@include('public.modules.footer')
@endsection
