@extends('public.app', [
'page_langs' => [
	'url' => 'urls.recovery_password'
],
'social' => [
	'title' => trans('pages/recovery-password.meta_title'),
	'description' => trans('pages/recovery-password.meta_desc'),
],
])

@section('content')
<div id="page-recovery-password" class="uk-container uk-container-small">
	<div class="uk-section uk-width-xlarge@s uk-margin-auto">
		<h1 class="uk-margin-medium">{{ trans('pages/recovery-password.title') }}</h1>

		<form method="post">
			{{ csrf_field() }}

			@if ($errors->any())
				<div class="uk-alert-danger" uk-alert>
					@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
					@endforeach
				</div>
			@endif

			@if (session('reset_failed') !== null)
				<div class="uk-alert-danger" uk-alert>
					<p>{{ trans('pages/recovery-password.error') }}</p>
				</div>
			@endif

			@if (session('success') !== null)
				<div class="uk-alert-success" uk-alert>
					<p>{{ trans('pages/recovery-password.success') }}</p>
				</div>
			@else
				@if ($token_valid !== true)
					<div class="uk-alert-danger" uk-alert>
						<p>{{ trans('pages/recovery-password.token_invalid') }}</p>
					</div>
				@else
					<div class="uk-margin">
						<input class="uk-input" type="text" name="password" placeholder="{{ trans('pages/recovery-password.placeh_password') }}">
					</div>

					<div class="uk-margin">
						<input class="uk-input" type="text" name="password_confirmation" placeholder="{{ trans('pages/recovery-password.placeh_password_r') }}">
					</div>
				@endif
			@endif

			<div class="uk-margin uk-flex uk-flex-middle uk-flex-between">
				<a href="{{ trans('urls.login') }}" class="uk-button uk-button-text">{{ trans('pages/recovery-password.btn_login') }}</a>

				@if ($token_valid === true)
					<button type="submit" class="uk-button uk-button-primary">{{ trans('pages/recovery-password.btn_send') }}</button>
				@endif
			</div>
		</form>
	</div>
</div>
@endsection
