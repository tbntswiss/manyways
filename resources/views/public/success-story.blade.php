@extends('public.app', [
'js' => 'case',
'page_langs' => [
	'url' => 'urls.home'
],
'social' => [
	'title' => $success_story->langs->title.' | '.env('APP_NAME'),
	'description' => $success_story->langs->description,
],
])

@section('content')
<div id="story--header" class="wrapper--fluid full--header story--header" style="background-image:url('{{ $dir_images($success_story->background->filename) }}');"></div>

<div id="case-intro" class="block--superposition case-intro">
	<div class="grid col-s-5">
		<span class="label">{!! trans('pages/success-story.client_label') !!}</span>

		@if ($success_story->is_product == 0)
			<h2>{{ $success_story->client }}</h2>
		@else
			<h2>{{ $success_story->langs->title }}</h2>
		@endif
	</div>
	<div class="grid col-s-5 right">
		<div class="case-intro-data">
			<span class="label">{!! trans('pages/success-story.descr_label') !!}</span>
			<p class="mobile-margin">{!! nl2br($success_story->langs->description) !!}</p>
		</div>
	</div>
</div>

@if ($success_story->image_1 !== null && $success_story->image_2 !== null)
	<div class="block-inverted story--ambiant">
		<div class="block wow fadeIn" data-900-top="transform: translate(-0px, -0px);" data--600-top="transform: translate(-0px, -80px);">
			<img src="{{ $dir_thumbs($success_story->image_1->filename) }}" alt="case preview" class="story--item__img objectfit">
		</div>
		<div class="block wow fadeIn" data-900-top="transform: translate(-0px, -80px);" data--600-top="transform: translate(-0px, -0px);">
			<img src="{{ $dir_thumbs($success_story->image_2->filename) }}" alt="case preview" class="story--item__img objectfit">
		</div>
	</div>
@endif

<div id="box-{{ str_slug($success_story->client) }}" class="story--process">
	@if ($success_story->is_product == 0)
		<div class="controls wow fadeIn">
			<span class="prev unselect"><img src="public/images/icons/white/left.svg" alt="previous"></span>
			<span class="next"><img src="public/images/icons/white/right.svg" alt="next"></span>
		</div>
		<div class="forceScrollSuccess"></div>
	@endif

	<div class="block-slider left wow fadeIn">
		<div class="slider-left step-1"><h2>{{ $success_story->langs->step_1_title or trans('pages/success-story.1_title') }}</h2></div>

		@if ($success_story->is_product == 0)
			<div class="slider-left step-2"><h2>{{ $success_story->langs->step_2_title or trans('pages/success-story.2_title') }}</h2></div>
			<div class="slider-left step-3"><h2>{{ $success_story->langs->step_3_title or trans('pages/success-story.3_title') }}</h2></div>
			<div class="slider-left step-4"><h2>{{ $success_story->langs->step_4_title or trans('pages/success-story.4_title') }}</h2></div>
			<div class="slider-left step-5"><h2>{{ $success_story->langs->step_5_title or trans('pages/success-story.5_title') }}</h2></div>
		@endif
	</div>
	<div class="block-slider right wow fadeIn">
		<div class="slider-right step-1"><p>{!! nl2br($success_story->langs->step_1) !!}</p></div>

		@if ($success_story->is_product == 0)
			<div class="slider-right step-2"><p>{!! nl2br($success_story->langs->step_2) !!}</p></div>
			<div class="slider-right step-3"><p>{!! nl2br($success_story->langs->step_3) !!}</p></div>
			<div class="slider-right step-4"><p>{!! nl2br($success_story->langs->step_4) !!}</p></div>
			<div class="slider-right step-5"><p>{!! nl2br($success_story->langs->step_5) !!}</p></div>
		@endif
	</div>
</div>

@if ($success_story->is_product == 0)
	<div class="wrapper bar-controls">
		<div class="numbers wow fadeIn">
			<span class="current">1</span>
			<span class="total">5</span>
		</div>
		<div class="bar wow fadeIn">
			<span class="current"></span>
			<span class="step first border"></span>
			<span class="step border"></span>
			<span class="step border"></span>
			<span class="step border"></span>
			<span class="step last"></span>
		</div>
	</div>
@endif

<div class="after-grey">
	<div class="story--products">
		@if (count($success_story->gallery ?? []) !== 0)
			<h1 class="wow fadeIn">{!! trans('pages/success-story.products_title') !!}</h1>
			<div class="block-default skrollr-plugin-default">
				@foreach ($success_story->gallery ?? [$success_story->background] as $image)
					<div class="block modal-preview wow fadeIn">
						<img src="{{ $dir_thumbs($image->filename) }}" alt="product preview" class="story--product" data-full="{{ $dir_images($image->filename) }}">
					</div>
				@endforeach
			</div>
		@else
			<div class="story--products__coming">
				<h2 class="wow fadeIn">{!! trans('pages/success-story.no_products') !!}</h2>
			</div>
		@endif
	</div>
</div>

<div class="modal">
	<div class="controls">
		<span class="prev"><img src="public/images/icons/blue/left.svg" alt="previous"></span>
		<span class="next"><img src="public/images/icons/blue/right.svg" alt="next"></span>
	</div>
	<div class="closeModal"><img src="public/images/icons/blue/closs.svg" alt="close"></div>
</div>

@if ($next_success_story->id !== $success_story->id)
	<div class="wrapper story--next">
		<div class="block-default wow fadeIn">
			<a pjax href="{{ trans('urls.success_stories').'/'.$next_success_story->langs->url }}">
				<div class="block content" data-900-top="transform: translate(-0px, -60px);" data--600-top="transform: translate(-0px, 0px);">
					<span class="label">{!! trans('pages/success-story.next_label') !!}</span>

					@if ($next_success_story->is_product == 0)
						<h2>{{ $next_success_story->client }}</h2>
					@else
						<h2>{{ $next_success_story->langs->title }}</h2>
					@endif
				</div>
				<div class="block img">
					<img src="{{ $dir_thumbs($next_success_story->background->filename) }}" alt="" class="objectfit">
				</div>
			</a>
		</div>
	</div>
@endif

<div class="cta--contact">
	<div class="content">
		@if ($success_story->is_product == 0)
			<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_1') !!}</h2>
			<a pjax href="{{ trans('urls.contact') }}">
				<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_2') !!}</h2>
			</a>
			<h2 class="wow fadeIn">.</h2>
		@else
			<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_3a') !!}</h2>
			<a pjax href="{{ trans('urls.contact') }}">
				<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_3b') !!}</h2>
			</a>
			<h2 class="wow fadeIn">{!! trans('pages/success-story.cta-contact_3c') !!}</h2>
		@endif
	</div>
</div>

@include('public.modules.footer')
@endsection
