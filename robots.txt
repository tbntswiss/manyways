User-agent: *

# Bloquer dossiers
Disallow: /app
Disallow: /assets
Disallow: /bootstrap
Disallow: /config
Disallow: /database
Disallow: /storage
Disallow: /tests
Disallow: /vendor

# Bloquer extensions
Disallow: /*.cgi$
Disallow: /*.css$
Disallow: /*.gz$
Disallow: /*.inc$
Disallow: /*.js$
Disallow: /*.swf$
Disallow: /*.xhtml$
Disallow: /*.wmv$

# Autoriser Google Image
User-agent: Googlebot-Image
Disallow:
Allow: /*

# Autoriser Google AdSense
User-agent: Mediapartners-Google*
Disallow:
Allow: /*