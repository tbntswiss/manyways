<?php

/*
|--------------------------------------------------------------------------
| UPDATE
|--------------------------------------------------------------------------
|
*/

Route::get('/update-site', 'UpdateController@updateSite');



/*
|--------------------------------------------------------------------------
| AUTH
|--------------------------------------------------------------------------
|
*/

Route::get('/login', 'AuthController@getLogin')->name('admin.login');
Route::get('/logout', 'AuthController@getLogout')->name('admin.logout');

Route::post('/login', 'AuthController@postLogin');



/*
|--------------------------------------------------------------------------
| HOME
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'HomeController@getHome')->name('admin.home');



/*
|--------------------------------------------------------------------------
| SUCCESS STORIES
|--------------------------------------------------------------------------
|
*/

Route::get('/success-stories', 'SuccessStoryController@getSuccessStories')->name('admin.success_stories');
Route::get('/success-story/create', 'SuccessStoryController@getCreateSuccessStory')->name('admin.success_story.create');
Route::get('/success-story/update/{success_story_id}', 'SuccessStoryController@getUpdateSuccessStory')->where('success_story_id', '[0-9]+')->name('admin.success_story.update');
Route::get('/success-story/delete/{success_story_id}', 'SuccessStoryController@getDeleteSuccessStory')->where('success_story_id', '[0-9]+')->name('admin.success_story.delete');

Route::post('/success-story/create', 'SuccessStoryController@postCreateSuccessStory');
Route::post('/success-story/update/{success_story_id}', 'SuccessStoryController@postUpdateSuccessStory')->where('success_story_id', '[0-9]+');
