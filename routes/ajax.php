<?php

/*
|--------------------------------------------------------------------------
| CONFIG
|--------------------------------------------------------------------------
|
*/

Route::get('/app-config', 'HomeController@getConfig');
Route::get('/csrf-token', 'HomeController@getCSRFToken');
