<?php

/*
|--------------------------------------------------------------------------
| HOME
|--------------------------------------------------------------------------
|
*/

Route::get('/', 'HomeController@getHome');
Route::get('{home}', 'HomeController@getHome');

/*
|--------------------------------------------------------------------------
| SUCCESS STORIES
|--------------------------------------------------------------------------
|
*/

Route::get('{success_stories}/{success_stories_id}', 'HomeController@getSuccessStory');

/*
|--------------------------------------------------------------------------
| PRODUCTS
|--------------------------------------------------------------------------
|
*/

Route::get('{products}', 'HomeController@getProducts');

/*
|--------------------------------------------------------------------------
| CONTACT
|--------------------------------------------------------------------------
|
*/

Route::get('{contact}', 'HomeController@getContact');
Route::get('{confirm}', 'HomeController@getConfirmation');

Route::post('/ajax/contact', 'HomeController@postContact');
Route::post('/ajax/captcha', 'HomeController@postCaptcha');
