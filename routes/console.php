<?php

use App\Project\Admin;
use App\Project\User;

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Example command
|--------------------------------------------------------------------------
|
| ex: php artisan inspire
|
*/

Artisan::command('inspire', function ()
{
    $this->comment('Inspiring::quote()');
})
->describe('Display an inspiring quote');



/*
|--------------------------------------------------------------------------
| Hash password
|--------------------------------------------------------------------------
|
| ex: php artisan hash_password {password}
|
*/

Artisan::command('hash_password {password}', function ($password)
{
	$this->comment(Hash::make($password));
})
->describe('Hash password');



/*
|--------------------------------------------------------------------------
| Create admin user
|--------------------------------------------------------------------------
|
| ex: php artisan create_user_admin {username} {password}
|
*/

Artisan::command('create_user_admin {username} {password}', function ($username, $password)
{
	$data = [
		'username' => $username,
		'password' => $password,
	];

	$validate = Admin::validateInsert($data);

	if ($validate->any())
		return $this->comment($validate->__toString());

	$admin_id = Admin::insert($data);

	if (is_numeric($admin_id) === true)
		$this->comment('Admin '.$username.' was successfully created.');
	else
		$this->comment('An error occured whie creating admin '.$username.'.');
})
->describe('Create admin user');
